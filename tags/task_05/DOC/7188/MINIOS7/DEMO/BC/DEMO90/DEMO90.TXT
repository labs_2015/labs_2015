*******************************************************
* gotc.bat    : batch file for use TC 2.0
* gomsc.bat   : batch file for use MSC 5.0/6.0 
* demo90.prj  : project file for TC++3.0 or BC++ 3.1
*******************************************************
demo90
 Demo program for use I-7188 Timer.
    Timer function used :
	  TimerOpen--> begin to use I-7188 timer
	  TimerClose-->Stop to use timer
	  TimerReadValue--> get current timer value.(unit 1ms)
	  TimerResetValue--> reset timer value to 0.
    This program show the time interval from call TimerOpen or
 TimerResetValue to now.

 press '0' to reset timer.
 press 'q' to quit program.
