/*
  program: ECHOCOM.C
  use    : echocom [/pn] [/bm] [/to] [/ep]
	   where n is comport,may be 1 or 2 or 3
		 m is baudrate(2400-115200)
		 o is 0(command mode) or 1(Bypass mode)
		 p is 0(echo off) or 1(echo on)
	   (1) must run on 7188
	   (2) default baudrate is 9600 , default port is COM3
	   (3) In bypass mode, any data key in will be direct send to comport
	   (4) echo ON/OFF is used on bypass mode.
  function description:
	   (1) echo any data on the 7188's COM1/2/3 to COM4.
	   (2) data come from COM4(end with CR(0x0d)) will be send to
	       COM1/2/3(data in will be buffered,until receive 0x0d,
	       7188 will send out the buffered data.(if checksum enable,
	       will append checksum(2 bytes) after the data,before 0x0d.)
	   (3) press 'q'(not 'Q') to quit.
	   (4) press 's'(not 'S') to toggle checksum mode.
	       so 'q' and 's' will not be send to comport.
	   (5) default mode is checksum disable.
	   (6) protocal is 1 start bit,8 data bit,none parity,1 stop bit.
	   (7) when use COM2, echocom is the same as echo485
	   (8) press CTRL_A/B/C change to use COM1/2/3
           (9) input "*bxxxx" to change baudrate,where the xxxx is baudrate.
               for example: *b115200 change baudrate to 115200
                            *b9600   change baudrate to 9600 
[10/11/1999]
  Add show COM port,checksum,baudrate on 5-DigitLED
  position 1: 1/2/3 for COM1/2/3
  position 2: 0/1 for checksum OFF/ON
  position 3-5: 115--> baudrate=115200
		576--> baudrate=57600
		384--> baudrate=38400
		192--> baudrate=19200
		096--> baudrate=9600
		048--> baudrate=4800
		024--> baudrate=2400
		012--> baudrate=1200
		006--> baudrate=600
		003--> baudrate=300
[10/12/1999] Ver. 1.02
  Add bypass mode, in bypass mode can set Echo ON/OFF
  *p0 --> end of bypass mode
  *p1 --> to bypass mode
  *e0 --> echo off
  *e1 --> echo on
  Only on [Command mode] can use *p,*e,*b command.
  Press CTRL_E to toggle between COMMAND mode and BYPASS mode
[10/19/1999]
  use SetBaudrate1(),SetBaudrate2(),SetBaudrate3() to change baudrate.
*/
#include<stdlib.h>
#include<stdio.h>
#include"..\lib\i7188.h"

int read_touch7(int *TouchType, int *TouchAddr, int *TouchValue);
int write_to_easyview_1(int addr, unsigned int value);
int write_to_easyview_2(int addr, int onoff);
int ComPort=1;	/* this value can be 1/3/4 --> COM 1/3/4 */

unsigned no;
unsigned char buf[80];
unsigned char OutBuf[80];
int idx=0;
int outidx=0;
int Bypass=0;
int Echo=1;

void ShowPort(int port)
{
  Show5DigitLedWithDot(1,port);
}
void ShowChecksum(int checksum)
{
  Show5DigitLedWithDot(2,checksum);
}
void ShowBaudrate(long baud)
{ int data;

  if(baud>100000) data=baud/1000;
  else data=baud/100;

  Show5DigitLed(5,data%10);
  data/=10;
  Show5DigitLed(4,data%10);
  data/=10;
  Show5DigitLed(3,data%10);
}

main(int argc,char *argv[])
{ long baud=9600;
  int quit=0;
  int data;
  int i;
  int checksum=0;
  int port=3;
  int val;
  unsigned mtdata=1234;

  Puts("\n\rStart echocom.exe\n\r");
  if(!Is7188()){
     Puts("\nechocom must run on 7188!");
     return;
  }
  for(i=1;i<argc;i++){
     if(argv[i][0]=='/'){
       switch(argv[i][1]){
	case 'p': case 'P':
	     switch(argv[i][2]){
	       case '1':
	       case '2':
	       case '3':
		    port=argv[i][2]-'0';
		    break;
	     }
	     break;
	case 'b': case 'B':
	     baud=atol(argv[i]+2);
	     if(baud<=0) baud=9600;
	     break;
	case 't': case 'T':
	     if(argv[i][2]=='0') Bypass=0;
	     else if(argv[i][2]=='1') Bypass=1;
	     break;
	case 'e': case 'E':
	     if(argv[i][2]=='0') Echo=0;
	     else if(argv[i][2]=='1') Echo=1;
	     break;
       }
     }
  }
  InstallCom(port,baud,8,0,1);
//  InstallCom(4,57600L,8,0,1);
  Print("EchoCOM.EXE Ver. 1.02\n\r");
  Print("Use COM%d,Baudrate=%lu,checksum=%d\n\r",port,baud,checksum);
  Init5DigitLed();
  ShowPort(port);
  ShowChecksum(checksum);
  ShowBaudrate(baud);
  while(!quit){
     if(Kbhit()){
	data=Getch();
	if(data==1){
	   /* CTRL_A --> COM1 */
	   if(port != 1){
	      RestoreCom(port);
	      port=1;
	      InstallCom(port,baud,8,0,1);
	      Print("\n\rChange to COM1, Baud=%ld checksum=%d\n\r",baud,checksum);
	      ShowPort(port);
	   }
	}
	else if(data==2){
	   /* CTRL_B --> COM2 */
	   if(port != 2){
	      RestoreCom(port);
	      port=2;
	      InstallCom(port,baud,8,0,1);
	      Print("\n\rChange to COM2, Baud=%ld checksum=%d\n\r",baud,checksum);
	      ShowPort(port);
	   }
	}
	else if(data==3){
	   /* CTRL_C --> COM3 */
	   if(port != 3){
	      RestoreCom(port);
	      port=3;
	      InstallCom(port,baud,8,0,1);
	      Print("\n\rChange to COM3, Baud=%ld checksum=%d\n\r",baud,checksum);
	      ShowPort(port);
	   }
	}
	else if(data==4){
		extern unsigned long Com3IntCount;
		/* CTRL_D */
		Print("Com3IntCount=%lu\n\r",Com3IntCount);
	}
	else if(data==5){
		/* CTRL_E */
		Bypass=!Bypass;
		Print("\n\rChange to %s mode\n\r",Bypass?"Bypass":"Command");
	}
	else if(data==6){ /* test MT-250 */
		ComPort=port;
		write_to_easyview_1(4,mtdata);
		mtdata++;
	}
	else if(Bypass){
		ToCom(port,data);
		if(Echo){
		   Putch(data);
		}
	}
	else if(data=='q') quit=1;
	else if(data=='s'){
	   checksum=!checksum;
	   Puts("CheckSum=");
	   Putch('0'+checksum);
	   Puts("\r\n");
	   ShowChecksum(checksum);
	}
	else if(data=='\b'){
	      if(outidx){
		 outidx--;
		 Putch(data);
	      }
	}
	else {
	   OutBuf[outidx++]=data;
	   Putch(data);
	   if(data=='\r'){
	      unsigned char sum=0;
	      Putch('\n');
	      outidx--;
	      if(OutBuf[0]=='*'){
		 switch(OutBuf[1]){
		  case 'b': case 'B':
		       {long b=atol(OutBuf+2);
			if(b>=300 && b<=115200){
			   /*
			   RestoreCom(port);
			   val=InstallCom(port,b,8,0,1);
			   if(val==NoError){
			      baud=b;
			      Print("COM%d baudrate change to %ld\n\r",port,baud);
			   }
			   else {
			     InstallCom(port,baud,8,0,1);
			     Print("Error=%d\n\r",val);
			   }
			   */
			   switch(port){
			     case 1:
				  if(NoError==SetBaudrate1(b)) baud=b;
				  break;
			     case 2:
				  if(NoError==SetBaudrate2(b)) baud=b;
				  break;
			     case 3:
				  if(NoError==SetBaudrate3(b)) baud=b;
				  break;
			   }
			   /* or use
			      if(NeError==SetBaudrate(port,b)) baud=b;
			   */
			}
			ShowBaudrate(baud);
			outidx=0;
		       }
		       break;
		  case 'e': case 'E':
		       if(OutBuf[2]=='0' && Echo){
			  Echo=0;
			  Print("Change to Echo OFF\n\r");
		       }
		       else if(OutBuf[2]=='1' && !Echo){
			       Echo=1;
			       Print("Change to Echo ON\n\r");
		       }
		       break;
		  case 'p': case 'P':
		       if(OutBuf[2]=='0' && Bypass){
			  Bypass=0;
			  Print("Change to Command mode\n\r");
		       }
		       else if(OutBuf[2]=='1' && !Bypass){
			       Bypass=1;
			       Print("Change to Bypass mode\n\r");
		       }
		       break;
		  }
	      }
	      else {
//		 Set485DirToTransmit(port);
		 for(i=0;i<outidx;i++){
		   ToCom(port,OutBuf[i]);
		   if(checksum) sum+=OutBuf[i];
		 }
		 if(checksum){
		   ToCom(port,hex_to_ascii[sum>>4]);
		   ToCom(port,hex_to_ascii[sum&0xf]);
		 }
		 ToCom(port,'\r');
//		 WaitTransmitOver(port);
//		 Set485DirToReceive(port);
	      }
	      outidx=0;
	   }
	}
     }
     if(IsCom(port)){
	data=ReadCom(port);
/*	printf("[%02X]",data);   */
	Putch(data);
	if(data=='\r'&& !Bypass) Putch('\n');
	buf[idx++]=data;
	if(data=='\r'){
	   idx=0;
	}
	else {
	   if(idx>=79) idx=78;
	}
     }
  }
  RestoreCom(port);
//  RestoreCom(4);
}

/*******************************************/
#define    DLE      0x10
#define    STX      0x02
#define    ACK      0x06
#define    NAK      0x15
#define    ETX      0x03
#define  DATA_SIZE  30
unsigned Data7[512];
unsigned NewData[DATA_SIZE];
int ShowMessage=0;
char szCmd1[80];

int ascii_to_hex(char c);
/* ------------------------------------------------------------------- */


/* ------------------------------------------------------------------- */
int read_touch7(int *TouchType, int *TouchAddr, int *TouchValue)
{ int i,rep,t;
  char c,str[80];
  int retval[20],maxi;
  char type,onoff;
  int addr;
  unsigned int value;

  if (ComPort!=4 && ShowMessage) Print(" -> is touch7");

  ToCom(ComPort,ACK); /* send ACK to EASYVIEW */

  i=0; rep=0;
  for (;;) {
    t=0;
    while (IsCom(ComPort)==0) {
	t++;
	if (t>32760) return(1);	/* time out */
    }
    c=ReadCom(ComPort);

    if(i==0) {
       if (c==1) {type=1; maxi=9;}
       else if (c==2) {type=2; maxi=7;}
       else return(2);			/* error cmd */
    }
    else if (type==1) {			/* DLE will repeat */
       if ((i>=1) && (i<=5) && (c==DLE)) {
	  if (rep==0) rep=1;
	  else rep=0;
       }
    }
    else if (type==2) {			/* DLE will repeat */
       if ((i>=1) && (i<=3) && (c==DLE)) {
	  if (rep==0) rep=1;
	  else rep=0;
       }
    }

    if (rep==0) {
       szCmd1[i]=c;
       i++; if (i>=maxi) break;
    }
  }
rec_ok:
    ToCom(ComPort,ACK); /* send ACK to EASYVIEW */
    if (type==1) {
       addr=*(int *)(szCmd1+1);
       value=*(unsigned int*)(szCmd1+4);
       *TouchType=1; *TouchAddr=addr; *TouchValue=value;
       if (ComPort!=4 && ShowMessage) Print(" * type=1, addr=%d, value=%d",addr,value);
       write_to_easyview_1(addr,value);
    }
    else if (type==2) {
	   addr=*(int *)(szCmd1+1);
	   value=szCmd1[3];
	   *TouchType=2; *TouchAddr=addr; *TouchValue=value;
	   if (ComPort!=4 && ShowMessage) Print(" * type=2, addr=%d, value=%d",addr,value);
	   /* write_to_easyview_2(addr,value); */
    }
    return(NoError);	/* receive OK */
}

/* ------------------------------------------------------------------- */
char cCmd1[9]={
  1,      /* 01     */
  0,      /* addr_l */
  0,      /* addr_h */
  2,      /* #	    */
  0,      /* data_l */
  0,      /* data_h */
  DLE,   /* DLE    */
  ETX,   /* ETX    */
  0,    /* checksum */
};

int write_to_easyview_1(int addr, unsigned int value)
{
   int iRet,i,j,retval[20],hh,ll,val_l,val_h,t;
   unsigned char xor;
   char c;
   int SendTime=0;

SendAgain:
   ToCom(ComPort,STX); /* send STX to EASYVIEW */
   t=0;
   while (IsCom(ComPort)==0){
     t++; if (t>32760) return(1);      /* time out */
   }
   c=ReadCom(ComPort);
   if (c==ACK) goto rec_ack; /* receive a ACK from easyview */
   return(2);

rec_ack:
   ll=addr&0xff;
   hh=(addr>>8)&0xff;

   val_l=value&0xff;
   val_h=(value>>8)&0xff;

/* cCmd1[0]=1;*/	   /* 01     */
   cCmd1[1]=ll;	   /* addr_l */
   cCmd1[2]=hh;	   /* addr_h */
/* cCmd1[3]=2; */	   /* #	     */
   cCmd1[4]=val_l;  /* data_l */
   cCmd1[5]=val_h;  /* data_h */
/* cCmd1[6]=0x10;*/   /* DLE    */
/* cCmd1[7]=0x03;*/   /* ETX    */

   xor=0;
   for (i=0; i<=5; i++) {
    xor = xor ^ cCmd1[i];
    retval[i]=ToCom(ComPort,cCmd1[i]);
    if(cCmd1[i]==DLE){
       retval[i]=ToCom(ComPort,cCmd1[i]);
       xor = xor ^ cCmd1[i];
    }
   }
   xor = xor ^ DLE ^ ETX;
   retval[6]=ToCom(ComPort,DLE);
   retval[7]=ToCom(ComPort,ETX);
   cCmd1[8]=xor;   /* CheckSum */
   retval[i]=ToCom(ComPort,xor);

   t=0;
   while (IsCom(ComPort)==0) {
     t++; if (t>3000) return(3);      /* time out */
     Delay(1);
   }
   c=ReadCom(ComPort);
   if (c==ACK) goto rec_ack2; /* receive a ACK from easyview */
   else if(c==NAK){
      SendTime++;
      if(SendTime<2){
	 goto SendAgain;
      }
      else return(4);
   }
rec_ack2:
   return(0);
}

/* ------------------------- */
char cCmd2[7]={
  2,	/* 02 */
  0,	/* addr_l */
  0,	/* addr_h */
  0,	/* #1/0   */
  0x10,	/* DLE	  */
  0x03,	/* ETX	  */
  0,    /* checksum */
};

int write_to_easyview_2(int addr, int onoff)
{ int iRet,i,xor,j,retval[20],hh,ll,val_l,val_h,t;
  char c;

  ToCom(ComPort,STX); /* send ACK to EASYVIEW */

  t=0;
  while (IsCom(ComPort)==0) {
     if (++t>32760) return(1);      /* time out */
  }
  c=ReadCom(ComPort);
  if (/*(iRet==0) &&*/ (c==ACK)) goto rec_ack; /* receive a ACK from easyview */
  return(2);
rec_ack:

  ll=addr&0xff;
  hh=(addr>>8)&0xff;

  /*cCmd2[0]=2;*/	/* 02 */
    cCmd2[1]=ll; 	/* addr_l */
    cCmd2[2]=hh;	/* addr_h */
    cCmd2[3]=onoff;	/* #1/0   */
  /*cCmd2[4]=0x10; */	/* DLE	  */
  /*cCmd2[5]=0x03; */	/* ETX	  */

  xor=0;
  for (i=0; i<=3; i++) {
    xor = xor ^ cCmd2[i];
    retval[i]=ToCom(ComPort,cCmd2[i]);
    if(cCmd2[i]==DLE){
       retval[i]=ToCom(ComPort,cCmd2[i]);
       xor = xor ^ cCmd2[i];
    }
  }
  xor = xor ^ DLE ^ ETX;
  retval[4]=ToCom(ComPort,DLE);
  retval[5]=ToCom(ComPort,ETX);
  retval[6]=ToCom(ComPort,xor);

  t=0;
  while (IsCom(ComPort)==0) {
     if (++t>500) return(3);      /* time out */
     Delay(1);
  }

  c=ReadCom(ComPort);
  if (c==ACK) goto rec_ack2; /* receive a ACK from easyview */
  return(4);
  rec_ack2:
  return(0);
}
