#include<stdlib.h>
#include<ctype.h>
#include<dos.h>
#include<math.h>
#include<string.h>
#include"..\lib\i7188.h"
/*
typedef struct  {
  unsigned mark;
  unsigned char fname[12];
  unsigned char year;
  unsigned char month;
  unsigned char day;
  unsigned char hour;
  unsigned char minute;
  unsigned char sec;
  unsigned long size;
  char far *addr;
  unsigned CRC;
  unsigned CRC32;
} FILE_DATA;
*/

#define M_PI        3.14159265358979323846
static FILE_DATA far *fdata;


float far *sin360, far *cos360;
unsigned long far * const SystemClock=(unsigned long far *)0x0040006CL;
void main(void)
{ int fileno=GetFileNo();
  unsigned i;
  char fname[13];
  unsigned long clock;
  float sincos;

/* Do "DIR" */
  Print("Total file number=%d\n\r",fileno);
  fname[12]=0;
  for(i=0;i<fileno;i++){
      fdata=GetFileInfoByNo(i);
      if(fdata) {
	 GetFileName(i,fname);
	 Print("[%02d]:%-12s start at %Fp "
	       "%02d/%02d/%04d %02d:%02d:%02d size=%lu\n\r",
	       i,fname,fdata->addr,
	       fdata->month,fdata->day,(fdata->year)+1980,
	       fdata->hour,fdata->minute,fdata->sec*2,fdata->size);
      }
  }
/* Get sincos.dat */

  sin360=(float far *)GetFilePositionByName("sincos.dat");
  if(sin360){
     cos360=&sin360[360];
     Print("Sin(0)=%f, cos(0)=%f\n\r",sin360[0],cos360[0]);
     Print("Sin(30)=%f, cos(30)=%f\n\r",sin360[30],cos360[30]);
     Print("Sin(45)=%f, cos(45)=%f\n\r",sin360[45],cos360[45]);
     Print("Sin(60)=%f, cos(60)=%f\n\r",sin360[60],cos360[60]);
     Print("Sin(90)=%f, cos(90)=%f\n\r",sin360[90],cos360[90]);
     Print("Sin(120)=%f, cos(120)=%f\n\r",sin360[120],cos360[120]);
     Print("Sin(180)=%f, cos(180)=%f\n\r",sin360[180],cos360[180]);
     Print("Sin(210)=%f, cos(210)=%f\n\r",sin360[210],cos360[210]);
  }

/* TEST SPEED for use table & use emulation */

  clock=*SystemClock;
  for(i=0;i<6000U;i++) sincos=sin(M_PI/180.0*60);
  clock=(*SystemClock)-clock;
  Print("sin(60 degree) 6000 times take %lu ms\n\r",clock);

  clock=*SystemClock;
  for(i=0;i<6000U;i++) sincos=sin360[60];
  clock=(*SystemClock)-clock;
  Print("sin360[60] 6000 times take %lu ms\n\r",clock);

}