#include"..\lib\i7188.h"

void main(void)
{ int type;
  int ver;

  type=Is7188x();
  if(type){
     ver=GetLibVersion();
     Print("Hello 7188x! (Flash memory is %d K)\n\r",type);
     Print("Library version is %d.%02d",ver>>8,ver&0xff);
  }
  else {
     Print("Hello PC!, this program is not run under I-7188.");
  }
}