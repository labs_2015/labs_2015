/*
1. Compiler: TC 2.0
2. Mode: Small
3. Project: Demo90.prj
   	    Demo90.c
	    i7188s.lib
4. Explain: (1). Demonstration for use Timer.
	    (2). To use TimerResetValue & TimerReadValue dunctions.
	    (3). Max TimerValue=0xFFFFFFFF=4294967295(ms)-->4294967.295(S)-->49.7(days)
5. Hordware: 7188
It's compiled by Tony
---------------------------------------------------------------------------------------
*/

#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<dos.h>
#include<math.h>
#include<string.h>
#include"..\LIB\i7188.h"
/*------------------------------------------------------------------------------------*/

void main(void)
{ 
  unsigned long time,sec;
  unsigned sec1;
  int quit=0;
  int type;
  int ver;
  type=Is7188();	/*detect the current operation system*/
   if(type)
     {
     ver=GetLibVersion();	/*if used in minios7*/
     Print("Hello 7188x! (Flash memory is %d K)\n\r",type);
     Print("Library version is %d.%02d",ver>>8,ver&0xff);
     }
   else 	/*if used in dos*/
     {
     Print("Hello PC!, this program is not run under I-7188.");
     return;
     }

  Print("cos(0)=%f sin(45)=%f",cos(0),sin(3.14159/4.0));
  Print("\n\rPress any key to start timer");
  Print("\n\rthen Press '0' to Reset timer,'q' to quit\n\r");
  Getch();
  TimerOpen();
  while(!quit){		/*set the key function*/
     if(Kbhit()){
	switch(Getch()){
	  case '0':
	       TimerResetValue();
	       break;
	  case 'q':
	       quit=1;
	       break;
	}
     }
     time=TimerReadValue();
/*
     sec=time/1000;
     sec1=time%1000;
     printCom3("\rTime=%05ld.%03d sec",sec,sec1);
*/
     Print("\rTime=%8.3f sec",0.001*time);
  }
  TimerClose();
}