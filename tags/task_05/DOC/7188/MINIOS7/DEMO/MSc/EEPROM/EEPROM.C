/*
1. Compiler: TC 2.0
2. Mode: Small
3. Project: Eeprom.prj
   	    Eeprom.c
	    i7188s.lib
4. Explain: To write a value to EEPROM and show it.
5. Hordware: 7188
It's compiled by Tony
----------------------------------------------------------------
*/

#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<dos.h>
#include<string.h>
#include"..\lib\i7188.h"
/*--------------------------------------------------------------*/

unsigned long far *TimeTick=(unsigned long far *)0x0040006CL;
unsigned char buf[2048];

main()
{ 
  int i,j,addr,block;
  unsigned long st,et;
  int type;
  int ver;

  type=Is7188();	/*detect the current operation system*/
   if(type)
     {
     ver=GetLibVersion();	/*if used in minios7*/
     Print("Hello 7188x! (Flash memory is %d K)\n\r",type);
     Print("Library version is %d.%02d",ver>>8,ver&0xff);
     }
   else		/*if used in dos*/
     {
     Print("Hello PC!, this program is not run under I-7188.");
     return;
     }
  st=*TimeTick;
  EE_WriteEnable();
  for(block=0;block<8;block++){
     for(addr=0;addr<256;addr++) buf[addr]=addr+block;
     for(addr=0;addr<256;addr+=16){
	EE_MultiWrite(block,addr,16,buf+addr);
     }
  }
  EE_WriteProtect();
  et=*TimeTick;
  Print("\n\rUse new function: Start=%lu, End=%lu -->Time=%lu(ms)",st,et,et-st);
  Print("\n\rPress any key to continue..."); Getch();
  EE_MultiRead(0,0,2048,buf);
  j=0;
  for(block=0;block<8;block++){
    for(addr=0;addr<256;addr+=16){
      Print("\n\rBlock %d [%03d] ",block,addr);
      for(i=0;i<16;i++){
	Print("%02X ",buf[j++]);
      }
    }
    Print("\n\rPress any key to continue..."); Getch();
  }

}