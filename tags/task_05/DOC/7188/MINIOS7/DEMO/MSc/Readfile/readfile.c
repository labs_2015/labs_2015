/*
1. Compiler: TC 2.0
2. Mode: Large
3. Project: Readfile.prj
   	    Readfile.c
            config_r.c
	    i7188l.lib
4. Explain: (1) This program is useing for reading the meat of the file.
            (2) You can select the file which download in the 7188.
            ***It's used config1.dat,config2.dat and config3.dat as an example here.***
            Note: This program only valid for Large Model.
                  This program will not work for Small Model.
5. Hordware: 7188
It's compiled by Tony
-----------------------------------------------------------------------------------------
*/

#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<dos.h>
#include<string.h>
#include"..\lib\i7188.h"
/*-------------------------------------------------------------------------------------*/

void main(void)
{
 int data,type,ver;
  char c;
  type=Is7188();		/*detect the current operation system*/
   if(type)
     {
     ver=GetLibVersion();
     Print("Hello 7188x! (Flash memory is %d K)\n\r",type);
     Print("Library version is %d.%02d",ver>>8,ver&0xff);
     }
   else
     {
     Print("Hello PC!, this program is not run under I-7188.");
     return;
     }

load_config();
}
