/*
  sincos.c
   make a sin & cos table, save to sincos.dat

   this program must run on PC
*/
#include<math.h>
#include<stdio.h>


float sin360[360];
float cos360[360];
void main(void)
{ int i;
  FILE *file;

  for(i=0;i<360;i++){
      sin360[i]=sin(M_PI/180.0*i);
      cos360[i]=cos(M_PI/180.0*i);
  }
  file=fopen("sincos.dat","wb");
  if(file){
     fwrite(&sin360[0],sizeof(float),360,file);
     fwrite(&cos360[0],sizeof(float),360,file);
     fclose(file);
     printf("save sin,cos table to sincos.dat");
  }
  else printf("cannot open file sincos.dat");
}
