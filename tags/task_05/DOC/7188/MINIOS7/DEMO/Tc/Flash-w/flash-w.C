/*
1. Compiler: TC 2.0
2. Mode: Small
3. Project: Flash-w.prj
   	    Flash-r.c
	    i7188s.lib
4. Explain: (1). To write a value which you input to flash memory at segment 0x9000 which
                 offset is 0 to 65535 and show it on monitor.
            (2). The value you input will write to Flash memory and auto-plus 1.
            * Press 'q' or 'Q' to quit program.
5. Hordware: 7188
It's compiled by Tony
-----------------------------------------------------------------------------------------
*/

#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<dos.h>
#include<string.h>
#include"..\lib\i7188.h"
/*------------------------------------------------------------------------------------*/

void main(void)
{
 int seg,data;
 char c;
 unsigned e=0x9000,i=0;
  int type;
  int ver;
  type=Is7188();	/*detect the current operation system*/
   if(type)		/*if it's used in minios7*/
     {
     ver=GetLibVersion();
     Print("Hello 7188x! (Flash memory is %d K)\n\r",type);
     Print("Library version is %d.%02d",ver>>8,ver&0xff);
     }
   else		/*if it's used in dos*/
     {
     Print("Hello PC!, this program is not run under I-7188.");
     return;
     }
 Print("\r\nPlease Input a value writting to segment 0x9000 of Flash Member:");
 Scanf("%d",&seg);
 FlashErase(e);
   while(i<65535){
    FlashWrite(e,i,seg);
    Print("\r\nThe value %d is writting to offset %d of Flash Memory",seg,i);
    i++;seg++;
    if(i%100==0){
     Print("\r\nPress q to quit or any key to continue...");c=Getch();
     if ((c=='q') || (c=='Q')) return;
    }
   }
}
