/*
1. Compiler: TC 2.0
2. Mode: Small
3. Project: Led.prj
   	    Led.c
	    i7188s.lib
4. Explain: This program is used to power on and power off sustained.
            * Press 'q' or to quit this program.
5. Hordware: 7188
It's compiled by Tony
----------------------------------------------------------------------------------
*/

#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<dos.h>
#include<string.h>
#include"..\lib\i7188.h"
/*-------------------------------------------------------------------------------*/

int quit=0;
int LedMode=0;
int data;
unsigned long time;
void main(void)
{
  int type;
  int ver;

  type=Is7188();	/*detect te current operation system*/
   if(type)
     {
     ver=GetLibVersion();	/*if used in minios7*/
     Print("Hello 7188x! (Flash memory is %d K)\n\r",type);
     Print("Library version is %d.%02d",ver>>8,ver&0xff);
     }
   else		/*if used in dos*/
     {
     Print("Hello PC!, this program is not run under I-7188.");
     return;
     }

  Print("Test functions: LedOn,LedOff,StopWatchStart,StopWatchReadValue\n\r");
  Print("and Kbhit,Getch,Putch\n\r");
  Print("Press 'q' to quit.\n\r");
  TimerOpen();
  StopWatchStart(0);
  LedOff();
  while(!quit){
    StopWatchReadValue(0,&time);
    if(time>=500){
       StopWatchStart(0);
       LedMode=!LedMode;
       if(LedMode) LedOn();
       else LedOff();
    }
    if(Kbhit()){
       data=Getch();
       if(data=='q' || data=='Q') quit=1;
       else {
	  Putch(data);
	  if(data=='\r') Putch('\n');
       }
    }
  }
  TimerClose();
}