/*
1. Compiler: TC 2.0
2. Mode: Small
3. Project: Seg7led.prj
   	    Seg7led.c
	    i7188s.lib
4. Explain: (1) This program will cause the first digit led of left side 
                continue showing 1,2......f .
            (2) The other digitled willshow the same value with keyboard.
                Even is with dot and Odd isn't with dot.
            * Press 'q' or 'Q' to quit.
5. Hordware: 7188
It's compiled by Tony
------------------------------------------------------------------------------
*/

#include<stdlib.h>
#include<stdio.h>
#include<conio.h>
#include<dos.h>
#include<string.h>
#include"..\lib\i7188.h"
/*---------------------------------------------------------------------------*/

int quit=0;
int LedMode=0;
int data;
unsigned long time;
void main(void)
{
  int data,y,i=0;
  int type;
  int ver;
  type=Is7188();		/*detect the current operation system*/
   if(type)
     {
     ver=GetLibVersion();
     Print("Hello 7188x! (Flash memory is %d K)\n\r",type);
     Print("Library version is %d.%02d",ver>>8,ver&0xff);
     }
   else
     {
     Print("Hello PC!, this program is not run under I-7188.");
     return;
     }
  for(;;){
   Show5DigitLed(1,i++);
   DelayMs(500);
   i=i&0x0f;
    if (Kbhit()){
   data=Getch();
    if(data=='q' || data=='Q') return;
    y=data&0x01;
     if (y) /*Even */
      {
      y=data-'0';
      Show5DigitLed(2,y);
      Show5DigitLed(3,y);
      Show5DigitLed(4,y);
      Show5DigitLed(5,y);
      }
     else /*Odd  */
      {
      y=data-'0';
      Show5DigitLedWithDot(2,y);
      Show5DigitLedWithDot(3,y);
      Show5DigitLedWithDot(4,y);
      Show5DigitLedWithDot(5,y);
      }
     }
    }
}
