****************************************************************************
 ~~	MiniOS7 utility   ~~~
 ICPDAS CO., LTD.
 Established by sean	
****************************************************************************
[2004/03/19] V1.0.05
	Add: Show the MiniOS7 utility name and hint on the start toolbar
	Modify:
	    1.Modify the MiniOS7dll for the latest of the minios7 image file.

	Library: UartDLL 2.05  , MiniOS7.dll 1.05
[2003/11/10] V1.0.04
	Add:1. Show the date of MiniOS7 and Flash size on the statusbus
	    2. Show the contents of autoexec.bat while you click the autoexec.bat
	Modify:
	    1.Modify the CRC16 in the MiniOS7dll
	    2.Lowercase the filename downloaded to the module.

	Library: UartDLL 2.04  , MiniOS7.dll 1.04
[2003/02/14] V1.0.03
	Add:1.Add the dialog 'Caution' for guiding
	    2.Add the dialog 'How to use'for guiding		
	Modify:
	    After updating MiniOS7,the success dialog shown.
	Fix:
	    Fix a bug that MiniOS7 utility can't download the read-only files.

[2003/01/10] V1.0.02
	Add:Search the files in Local HD of PC

[2002/11/01] V1.0.01 Release
	Update MiniOS7,
	Load one/multiplefiles to Flash
	basic configuration in MiniOS7 



