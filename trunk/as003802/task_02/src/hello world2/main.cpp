/*!
@file main.cpp
@brief "Hello world"
@author Андросюк А.Н. АС-38
@date 07.12.2015 г.
*/

#include "stdafx.h"
#include <iostream>
using namespace std;
/*!
@brief Выводит в стандартный поток ввода-вывода строку "Hello world"

@return Возвращает 0 при успешном завершении выполнения функции main
*/
int main() {
	cout << "Hello, World!" << endl;
	system("pause");
    return 0;
}

