#pragma once
///@brief дочерний класс, реализующий линейную модель
using namespace std;
class LinearModel :public Object {
public:
	/**
	@brief Происходит вывод таблицы на консоль.
	*/
	void tempShow(int(*ut)(int)) {
	///@brief задает вход и выход системы соответственно
	///@param yt - выходная температура 
	///@param ut - тепло, поступающее в систему
		cout << "Решение линейной модели:" << endl;
		cout.width(4);
		cout << "t" << "  |     " << "y" << "     |  " << "u" << endl;
		cout << "------------------------" << endl;
		cout.width(4);
		cout << "0" << "  |     " << y0 << "     |  " << ut(0) << endl;
		double y = y0;
		for (int i = 1; i <11; i++) {
			cout.width(4);
			cout << i << "  |  " << (y = 0.988*y + 0.232*ut(i - 1)) << "  |  " << ut(i - 1) << endl;
		}
	}
};
