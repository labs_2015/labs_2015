
#pragma once
///@brief дочерний класс, реализующий нелинейную модель
using namespace std;
class NoLinearModel :public Object {
protected:
	double y1;
public:
	NoLinearModel() {
		y1 = 20;
	}

	double getY1() {
		return y1;
	}

	void setY1(double y1) {
		this->y1 = y1;
	}
	/**
	@brief Происходит вывод таблицы на консоль.
	*/
	///@brief задает вход и выход системы соответственно
	///@param y0 - выходная температура 
	///@param ut - тепло, поступающее в систему
		///@brief выводит на консоль входное значение температуры на каждой итерации(Нелинейная модель)
	///@param t - количество итераций
	void tempShow(int(*ut)(int)) {
		cout << "Решение нелинейной модели:" << endl;
		cout.width(4);
		cout << "t" << "  |     " << "y" << "     |  " << "u" << endl;
		cout << "------------------------" << endl;
		cout.width(4);
		cout << "0" << "  |     " << y0 << "     |  " << ut(0) << endl;
		double yLast = y0, yNext = y1;
		for (int i = 1; i <11; i++) {
			double y = yNext;
			cout.width(4);
			cout << i << "  |  " << (yNext = 0.9*yNext - 0.001*yLast*yLast + ut(i - 1) + sin(static_cast<double>(ut(i - 2)))) << "  |  " << ut(i - 1) << endl;
			yLast = y;
		}
	}
};
