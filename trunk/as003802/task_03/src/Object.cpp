/**
@brief Производится создание класса "Object".
*/
class Object {
protected:
	double y0;
public:
	Object() {
		y0 = 0;
	}

	double getY0() {
		return y0;
	}

	void setY0(double y0) {
		this->y0 = y0;
	}

	virtual void tempShow(int(*u)(int)) = 0;
};
