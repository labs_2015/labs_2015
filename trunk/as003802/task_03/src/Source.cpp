/*! \mainpage Project "Lab_1"
* Программа, моделирующая объект управления.
* \n <b> График линейной модели: </b>
*  \image html LinearModel.PNG
* <b> График нелинейной модели: </b>
*  \image html NonLinearModel.PNG
* LinearModel: Данная сис-ма является линейной т.к. нет параметров имеющих нелинейную зависимость входа и выхода.
* \n NonLinearModel: Данная сис-ма является нелинейной т.к. есть параметр имеющий нелинейную зависимость входа и выхода.
*/
#include "stdafx.h"
#include <iostream>
#include <math.h>
#include "Object.h"
#include "LinearModel.h"
#include "NoLinearModel.h"
#include "DeskLinearModel.h"
#include "DeskNoLinearModel.h"

using namespace std;
/**
@brief Производится создание класса "Object".
*/
int u(int t) {
	return 100;
}

/**
@brief Происходит определение точки входа в программу.
*/
int main() {
	setlocale(LC_ALL, "rus");
	LinearModel o1;
	NoLinearModel o2;
	DeskNoLinearModel d1;
	DeskLinearModel d2;
	o1.tempShow(&u);
	cout << endl << endl;
	o2.tempShow(&u);
	cout << endl;
	d1.tempShow(&u);
	cout << endl;
	d2.tempShow(&u);
	system("pause");
}