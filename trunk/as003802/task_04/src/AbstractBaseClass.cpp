class AbstractBaseClass
{
public:
	/// The controller parameters
	double q0, q1, q2;
	/// �onstant of integration
	double V = 0.5;
	double n = 0.5;
	double e1 = 0, e2 = 0, e3 = 0;
	double u = 0;
	double w = 40;
	double y = 0;
	/// The time step
	double V0 = 0.5;
	/// Permanent differentiation
	double Vd = 0.02;
protected:
	virtual void show() = 0;
};