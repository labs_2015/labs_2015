/*! \mainpage Project "Hello world!"
* <b> ���������, ��������� �� ������� "Hello world!". </b>
* \file main.cpp
* \brief �������� ���� � ��������� ������� main().
*/

#include <iostream>
#include <string>
using namespace std;



int main()//! ����� ����� � ���������. � ��� �������������� ������������� � ����� ������. 
{
	/*!
	���������� ������ ��������:
	\code
	string str = "Hello world!";
	\endcode
	*/
	string str = "Hello world!";  
	/*!
	����� ������ �������� �� �������:
	\code
	cout << str << endl; 
	\endcode
	*/
	cout << str << endl; 
	system("pause");
	return 0;
}