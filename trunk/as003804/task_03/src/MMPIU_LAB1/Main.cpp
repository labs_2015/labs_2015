/*! \mainpage Project "Lab rab 1"
* ���������, ������������ ������ ����������.
* ����������� ������������� ���: ��� ������ (���� �� ������� �����������) � ������������.
* \n <b> ������ �������� ������: </b>
*  \image html Linear.jpg
* <b> ������ ���������� ������: </b>
*  \image html Nonlinear.jpg
* \file main.cpp
* \brief �������� ���� � ��������� ������� Model, LinearModel, NonlinearModel, � ����� ������� ShowResult(), SearchAnswer(), PrintResultFile().
*/

# include <iostream>//!������������ ���� ��� ����������� �����-������.
# include <math.h>//!������������ ����, ������������� ��� ���������� ������� �������������� ��������. 
#include <fstream>//!������������ ����, ������� ��������� ���� ��������� � ������;
using namespace std;
const double ut = 7.4321; //!<���������� ����������, ���������� �������� �����, ������������ � �������.

/*!����������� ����� Model.
\brief ����������� ������������ �����,�� ������� ������� ��������� ��������
*/
class Model
{
private:
	//! ������, ���������� �������� �������� ����������� ������. 
	double array_y[31] = {};
public: 
	    //!����������� ������������ ������. 
	Model() :array_y() {};
		//!����������� ����� ��� ���������� ������������ ������
		virtual void ShowResult() = 0; 
		//!����������� ����� ��� ���������� ������������ ������
		virtual void SearchAnswer() = 0;
		//!����������� ����� ��� ���������� ������������ ������
		virtual void PrintResultFile() = 0;

};


/*!����� ��� ���������� �������� ������, �������������� �� ������ Model.
\brief ����� - ������� ������ Model, ����������� ������� ������ �������.
*/
class LinearModel :public Model
{
private:
	//! ������, ���������� �������� �������� ����������� ������. 
	double array_y[31];
public:
	//!����������� ������, ������� �������� ��������� ������������� ������� array_y
	LinearModel() :array_y() {}
	//!�����, ������� ��������� ���������� �����������.
	void SearchAnswer()
	{
		for (int i = 1; i <= 30; i++)
		{
			/**
			���������� i-�� ����� �������:
			@code
			array_y[i] = 0.988*array_y[i - 1] + 0.232*ut;
			@endcode
			*/
			array_y[i] = 0.988*array_y[i - 1] + 0.232*ut;
		}
	}
	//!�����, ������� ��������� ����� �����������.
	void ShowResult()
	{
		cout << "������� ��������� ������: " << endl;
		for (int i = 1; i <= 30; i++)
		{
			cout << "t=" << i << " y=" << array_y[i] << endl;
		}
	}
	//!�����, ������� ��������� ����� ����������� � ��������� ����.
	void PrintResultFile()
	{
		ofstream fout;
		fout.open("F://lab.txt");
		fout << "������� ��������� ������ :" << endl;
		for (int i = 1; i <= 30; i++)
		{
			fout << array_y[i] << endl;
		}
		fout.close();
	}
};

/*!����� ��� ���������� ���������� ������, �������������� �� ������ Model.
* \brief ����� - ������� ������ Model, ����������� ��������� ������ �������.
*/
class NonlinearModel : public Model
{
private: 
	//! ������, ���������� �������� �������� ����������� ������. 
	double array_y[31] = {};
public:
	//!����������� ������, ������� �������� ��������� ������������� ������� array_y.
	NonlinearModel() :array_y() {}
	//!�����, ������� ��������� ���������� �����������.
	void SearchAnswer()
	{
		for (int i = 1; i <= 30; i++)
		{
			if (i > 1)
			{
				/**
				���������� i-�� ����� �������:
				@code
				array_y[i] = 0.9*array_y[i - 1] - 0.001*array_y[i - 2] * array_y[i - 2] + ut + sin(ut);
				@endcode
				*/
				array_y[i] = 0.9*array_y[i - 1] - 0.001*array_y[i - 2] * array_y[i - 2] + ut + sin(ut);
			}
		}
	}
	//!�����, ������� ��������� ����� ����������� � ��������� ����.
	void PrintResultFile()
	{
		ofstream fout;
		fout.open("F://lab.txt", ios_base::app);
		fout << "������� ����������� ������ :" << endl;
		for (int i = 1; i <= 30; i++)
		{
			fout << array_y[i] << endl;
		}
		fout.close();
	}
	//!�����, ������� ��������� ����� �����������.
	void ShowResult()
	{
		cout << "������� ����������� ������: " << endl;
		for (int i = 1; i <= 30; i++)
		{
			cout << "t=" << i << " y=" << array_y[i] << endl;
		}
	}
};

//!�������, � ������� �������� �������� �������� ������, � ����� ���������� ������� ��� ����.
int main()
{
	setlocale(0, "");
	LinearModel *ObjectLinearModel = new LinearModel;
	NonlinearModel *ObjectNonlinearModel= new NonlinearModel;
	ObjectLinearModel->SearchAnswer();
	ObjectLinearModel->ShowResult();
	ObjectLinearModel->PrintResultFile();
	ObjectNonlinearModel->SearchAnswer();
	ObjectNonlinearModel->ShowResult();
	ObjectNonlinearModel->PrintResultFile();
	system("pause");
	return 0;
}


