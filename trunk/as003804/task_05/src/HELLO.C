/*! \mainpage Project "Lab rab 3"
* Программа "Hello world!" для PAC ICPCON 7188D.
* Строка с приветствием должна выводится на семисегментном индикаторе.
* \file HELLO.c
* \brief Файл, содержащий исходный код, который реализует вывод на семисегментном индикаторе.
*/

#include "I7188.h"

int main()
{
		while(1)
	{
		/**
			Пример вывода HELLO:
			@code
			Show5DigitLedSeg(1, 55);	
			Show5DigitLedSeg(2, 79);	
			Show5DigitLedSeg(3, 14);	
			Show5DigitLedSeg(4, 14);	
			Show5DigitLedSeg(5, 126);	
			@endcode
			*/
		Show5DigitLedSeg(1, 55);	
		Show5DigitLedSeg(2, 79);	
		Show5DigitLedSeg(3, 14);	
		Show5DigitLedSeg(4, 14);	
		Show5DigitLedSeg(5, 126);	
		DelayMs(750);
		Show5DigitLedSeg(1, 79);        
		Show5DigitLedSeg(2, 14);        
		Show5DigitLedSeg(3, 14);        
		Show5DigitLedSeg(4, 126);       
		Show5DigitLedSeg(5, 0);        
		DelayMs(750);
		Show5DigitLedSeg(1, 14);        
		Show5DigitLedSeg(2, 14);        
		Show5DigitLedSeg(3, 126);       
		Show5DigitLedSeg(4, 0);         
		Show5DigitLedSeg(5, 30);        
		DelayMs(750);
		Show5DigitLedSeg(1, 14);        
		Show5DigitLedSeg(2, 126);       
		Show5DigitLedSeg(3, 0);         
		Show5DigitLedSeg(4, 30);        
		Show5DigitLedSeg(5, 60);        
		DelayMs(750);
		Show5DigitLedSeg(1, 126);       
		Show5DigitLedSeg(2, 0);         
		Show5DigitLedSeg(3, 30);        
		Show5DigitLedSeg(4, 60);        
		Show5DigitLedSeg(5, 126);       
		DelayMs(750);
		Show5DigitLedSeg(1, 0);	        
		Show5DigitLedSeg(2, 30);        
		Show5DigitLedSeg(3, 60);        
		Show5DigitLedSeg(4, 126);       
		Show5DigitLedSeg(5, 70);	
		DelayMs(750);
		Show5DigitLedSeg(1, 30);        
		Show5DigitLedSeg(2, 60);        
		Show5DigitLedSeg(3, 126);       
		Show5DigitLedSeg(4, 70);       
		Show5DigitLedSeg(5, 14);        
		DelayMs(750);
		Show5DigitLedSeg(1, 60);       
		Show5DigitLedSeg(2, 126);       
		Show5DigitLedSeg(3, 70);        
		Show5DigitLedSeg(4, 14);        
		Show5DigitLedSeg(5, 189);	
		DelayMs(750);
		Show5DigitLedSeg(1, 126);       
		Show5DigitLedSeg(2, 70);        
		Show5DigitLedSeg(3, 14);        
		Show5DigitLedSeg(4, 189);       
		Show5DigitLedSeg(5, 0);         
		DelayMs(750);
		Show5DigitLedSeg(1, 70);        
		Show5DigitLedSeg(2, 14);        
		Show5DigitLedSeg(3, 189);       
		Show5DigitLedSeg(4, 0);        
		Show5DigitLedSeg(5, 0);	        
		DelayMs(750);
	}
 }