/**
@mainpage  A console application that displays the "Hello World!"
@file
@author Vlad Hrytsuk
@date 06.11.15
*/
#include <iostream>
using namespace std;
/**
@brief We define an entry point in the program
*/
int main()
{
	/**
	@brief Display message "Hello,world"
	*/
	cout << "Hello, world." << endl;
	///@brief We detain the console screen
	system("pause");
	return 0;
}
