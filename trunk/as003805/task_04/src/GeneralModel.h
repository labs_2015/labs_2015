/**
@mainpage Model
@image html LinearModel.png
@image html NonlinearModel.png
The graphs show that we adjust the model. The nonlinear model is adjusted faster than linear.
*/

#include "stdafx.h"
#include <iostream>

class GeneralModel
{
public:
	GeneralModel();
	///@brief Abstract method to display the values of the model.
	virtual double Model(double yt, double ut) = 0;
	~GeneralModel();
};


