#include "GeneralModel.h"
///@brief The child class that implements the linear model.
class LinearModel :
	public GeneralModel
{
public:
	LinearModel();
	///@brief Console output to the input temperature at each iteration (linear model).
	///@param yt - initial the input value of the temperature.
	///@param ut - the input intensity value.
	///@return yt 
	double Model(double yt, double ut);
	~LinearModel();
};
