#include "GeneralModel.h"

class NonlinearModel:
	public GeneralModel
{
private:
	double ytPrev;
public:
	NonlinearModel();
	///@brief Sets the input value and the intensity of the initial the input value of the temperature.
	///@param y0 - initial the input value of the temperature.
	NonlinearModel(double y0);
	///@brief Console output to the input value of the temperature at each iteration (nonlinear model)
	///@param yt - initial the input value of the temperature.
	///@param ut - the input intensity value.
	double Model(double yt, double ut);
	~NonlinearModel();
};



