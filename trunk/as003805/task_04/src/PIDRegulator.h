class PIDRegulator
///@brief Class regulating outlet temperature.
{
private:
	double q0, q1, q2;
	double T0, Td, T;
	double K;
	double eFirst, eSec, eThird;
	double u;

public:
	PIDRegulator();
	PIDRegulator(double Td, double T0, double T, double K);
	/// Calculates ut
	///@param y -  Control action.
	///@param w - The algorithm of the system.
	///@return - ut
	double Deviation(double y, double w);
	~PIDRegulator();
};

