/**
@file task_04.cpp
@author Vlad Hrytsuk
@date 28.11.2015
*/
#include "stdafx.h"
#include <iostream>
#include "GeneralModel.h"
#include "LinearModel.h"
#include "NonlinearModel.h"
#include "PIDRegulator.h"
#include <iostream>

using namespace std;
/**
@brief It contains simulation PID. It displays the console output temperature.
*/
int main()
{
	const double w = 75.0;

	LinearModel *linMod = new LinearModel();
	NonlinearModel *nonlinMod = new NonlinearModel(0);
	PIDRegulator *regul = new PIDRegulator(0.01, 0.5, 0.21, 0.5);

	/*!
	Linear model:
	\code
	cout << "Linear model: " << endl;

	double y = linMod->Model(0, 0);
	double u = regul->Deviation(y, w);

	for (int i = 0; i < 50; i++)
	{
		y = linMod->Model(y, u);
		u = regul->Deviation(y, w);
		cout << "yt = " << y << ";  " << "ut = " << u << endl;
	}
	\endcode
	*/
	cout << "Linear model: " << endl;

	double y = linMod->Model(0, 0);
	double u = regul->Deviation(y, w);

	for (int i = 0; i < 50; i++)
	{
		y = linMod->Model(y, u);
		u = regul->Deviation(y, w);
		cout << "yt = " << y << ";  " << "ut = " << u << endl;
	}

	/*!
	Nonlinear model:
	\code
	cout << "Nonlinear Model: " << endl;

	y = nonlinMod->Model(0, 0);
	u = regul->Deviation(y, w);

	for (int i = 0; i < 50; i++)
	{
		y = nonlinMod->Model(y, u);
		u = regul->Deviation(y, w);
		cout << "yt = " << y << ";  " << "ut = " << u << endl;
	}

	\endcode
	*/

	cout << "Nonlinear Model: " << endl;

	y = nonlinMod->Model(0, 0);
	u = regul->Deviation(y, w);

	for (int i = 0; i < 50; i++)
	{
		y = nonlinMod->Model(y, u);
		u = regul->Deviation(y, w);
		cout << "yt = " << y << ";  " << "ut = " << u << endl;
	}
	/*!
	We put a program on pause to watch the selected inscription:
	\code
	system("pause");
	\endcode
	*/
	system("pause");
	/*!
	The successful completion of of the program, the main function returns zero:
	\code
	return 0;
	\endcode
	*/
	return 0;
}