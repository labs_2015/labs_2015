#pragma once
class Abstract
{
public:
	Abstract();
	virtual void print() = 0;
	~Abstract();
};

