#pragma once
#include "Const.h"
#include <iostream>
#include <math.h>
using namespace std;
class BasicObj :
	public Const
{
public:
	BasicObj();
	~BasicObj();
public: void print()
{
	Ylin[0] = 0; Ynlin[0] = 0;
	for (int i = 0;i<21;i++)
	{
		Ylin[i + 1] = 0.988*Ylin[i] + 0.232*add(i);
		if (i >= 1) {
			Ynlin[i + 1] = 0.9*Ynlin[i] - 0.001*Ylin[i - 1] * Ylin[i - 1] + add(i) + sin(add(i - 1));
			cout << "t=" << i << " ylin=" << Ylin[i] << "  ynlin=" << Ynlin[i] << ";" << endl;
		}
	}
}

private:
	double Ylin[20], Ynlin[20];
};

