#pragma once
#include "Abstract.h"
#include<math.h>
class BasicOBJ :
	public Abstract
{
protected:
	double yL = 20;
public:
	///@brief ���������� �������� �������� ����������� �������
	double getY1()
	{
		return yL;
	}

	///@brief ������������� �������� �������� ����������� �������
	void setY1(double yL)
	{
		this->yL = yL;
	}

	double getTemperature(double y, double u)
	{
		double result = 0.9*y - 0.001*yL*yL + u + sin(u);
		yL = y;
		return result;
	}
};

