#pragma once
class PID
{
private:
	double T0, Td, T, k, q0, q1, q2;
	double e1 = 0, e2 = 0, e3 = 0, u = 0;
public:

	///@brief ������� SetT0 ������������� �������� ��� T0
	void SetT0(double t)
	{
		T0 = t;
	}
	double getT0()
	{
		return T0;
	}

	///@brief ������� SetTd ������������� �������� ��� Td
	void SetTd(double t)
	{
		Td = t;
	}
	double getTd()
	{
		return Td;
	}
	///@brief ������� SetT ������������� �������� ��� T
	void SetT(double t)
	{
		T = t;
	}
	double getT()
	{
		return T;
	}
	///@brief ������� SetK ������������� �������� ��� �
	void SetK(double K)
	{
		k = K;
	}
	double getK()
	{
		return k;
	}
	///@brief ������� CalcQ ������������ �������� q
	void CalcQ()
	{
		q0 = k * (1 + (Td / T0));
		q1 = -k *(1 + 2 * (Td / T0) - (T0 / T));
		q2 = k * (Td / T0);
	}

	double Calc(double y, double w)
	{
		e3 = e2;
		e2 = e1;
		e1 = w - y;
		u += (q0 * e1 + q1 * e2 + q2 * e3);
		return u;
	}

};



