/*! \mainpage "Hello, world!"
* ���������, ������� ������� ������� "Hello, World!" �� �������
* \file Hello World comment.cpp
* \brief ���� � ����� �� ����� ���������������� �++, ������� ������� ������� "Hello,world" �� �������
*/
#include "stdafx.h"
#include <iostream>
using namespace std;

int main() //! �������, ������� ������� ������� "Hello,world" �� �������
{
	/*!
	����� ������� "Hello,world" �� �������
	*/
	cout << "Hello, world!" << endl;
	system("pause");
	return 0;
}