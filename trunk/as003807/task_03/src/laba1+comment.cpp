
/*! \mainpage "������������ ������ 1"
* ������ ��������������� �������� ����������� �������� ������:
*  \image html Linear.png
*  ������ ��������������� �������� ����������� ���������� ������:
*  \image html Nolinear.png
* \file laba1+comment.cpp
* \brief ���� � �������� ����� �� ����� C++, ����������� ������������� ������� ����������
*/

#include "stdafx.h"
#include <iostream>
#include <math.h>

using namespace std;
/**
@brief ������������ �������� ������ "lab".
*/
class lab {
protected:
	double y0;
public:
	lab() {
		y0 = 0;
	}

	double getY0() {
		return y0;
	}

	void setY0(double y0) {
		this->y0 = y0;
	}

	virtual void tempShow(int(*u)(int)) = 0;
};
/**
@brief ���������� ������������ ������ "lab1" �� ������ "lab".
*/
class lab1 :public lab {
public:
	/**
	@brief ���������� ����� ������� �� �������.
	*/
	void tempShow(int(*ut)(int)) {
		cout << "������� �������� ������:" << endl;
		cout.width(4);
		cout << "t" << "  |     " << "y" << "     |  " << "u" << endl;
		cout << "------------------------" << endl;
		cout.width(4);
		cout << "0" << "  |     " << y0 << "     |  " << ut(0) << endl;
		double y = y0;
		for (int i = 1; i <11; i++) {
			cout.width(4);
			cout << i << "  |  " << (y = 0.988*y + 0.232*ut(i - 1)) << "  |  " << ut(i - 1) << endl;
		}
	}
};
/**
@brief ���������� ������������ ������ "lab2" �� ������ "lab".
*/
class lab2 :public lab {
protected:
	double y1;
public:
	lab2() {
		y1 = 20;
	}

	double getY1() {
		return y1;
	}

	void setY1(double y1) {
		this->y1 = y1;
	}
	/**
	@brief ���������� ����� ������� �� �������.
	*/
	void tempShow(int(*ut)(int)) {
		cout << "������� ���������� ������:" << endl;
		cout.width(4);
		cout << "t" << "  |     " << "y" << "     |  " << "u" << endl;
		cout << "------------------------" << endl;
		cout.width(4);
		cout << "0" << "  |     " << y0 << "     |  " << ut(0) << endl;
		double yLast = y0, yNext = y1;
		for (int i = 1; i <11; i++) {
			double y = yNext;
			cout.width(4);
			cout << i << "  |  " << (yNext = 0.9*yNext - 0.001*yLast*yLast + ut(i - 1) + sin(static_cast<double>(ut(i - 2)))) << "  |  " << ut(i - 1) << endl;
			yLast = y;
		}
	}
};


int u(int t) {
	return 100;
}
/**
@brief ���������� ����������� ����� ����� � ���������.
*/
int main() {
	setlocale(LC_ALL, "rus");
	lab1 o1;
	lab2 o2;
	o1.tempShow(&u);
	cout << endl << endl;
	o2.tempShow(&u);
	cout << endl;
	system("pause");
}
