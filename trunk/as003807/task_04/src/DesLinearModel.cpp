class DesLinearModel : public Reg_PID
{
public:
	double  y_1 = 0;
	void PID()
	{
		REG_PID();
		y_1 = 0.988*y + 0.232*u;
		y = y_1;
		cout.width(8);
		cout << y_1 << "  |  " << u << endl;
	}
};