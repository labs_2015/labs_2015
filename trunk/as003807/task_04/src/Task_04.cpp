/*! @mainpage Project "Task_04"
* \n <b> <center> Schedule linear model:</center> </b>
*  \image html LinearModel.png
*<br> <b> <center> Schedule Nonlinear model:</center> </b>
*  \image html NonLinearModel.png
*\details Based on the aforementioned diagrams , one can conclude that regulation of a nonlinear model is faster .
* @author Danilevick K.A.
*/
#include "stdafx.h"
#include <iostream>
#include <math.h>
#include "AbstractBaseClass.h"
#include "PIDSimulation.h"
#include "DesLinearModel.h"
#include "DesNonLinearModel.h"
#include "OutputDesLinearModel.h"
#include "OutputDesNonLinearModel.h"
using namespace std;
///@brief The function that creates a class of objects and executes methods on them .
int main(int argc, char* argv[])
{
	setlocale(0, "");
	OutputDesNonLinearModel n;
	OutputDesLinearModel l;
	n.show();
	l.show();
	system("pause");
	return 0;
};