/*! \mainpage ������ "Hello world Kirill!"
@author Kirill Danilevich
@date 23.12.15
* ���������,�����������  ����� �� �������������� ��������� ����������� ������ "Hello world Kirill!"
* \brief ����, ���������� �������� ���, ������� ��������� ����� �� �������������� ����������.
@file kir.C
*/

#include"I7188.h"
void main()
{
while(1)
{
Show5DigitLedSeg(5,55); //h
Show5DigitLedSeg(4,79); //e
Show5DigitLedSeg(3,14); //l 
Show5DigitLedSeg(2,14); //l
Show5DigitLedSeg(1,126);//o
DelayMs(1000);
Show5DigitLedSeg(5,79); //e
Show5DigitLedSeg(4,14); //l
Show5DigitLedSeg(3,14); //l
Show5DigitLedSeg(2,126);//0
Show5DigitLedSeg(1,0);  //_
DelayMs(1000);
Show5DigitLedSeg(5,14); //l
Show5DigitLedSeg(4,14); //l
Show5DigitLedSeg(3,126);//o
Show5DigitLedSeg(2,0);  //_
Show5DigitLedSeg(1,62); //w
DelayMs(1000);
Show5DigitLedSeg(5,14); //l
Show5DigitLedSeg(4,126);//o
Show5DigitLedSeg(3,0);  //_
Show5DigitLedSeg(2,62); //w
Show5DigitLedSeg(1,126);//o
DelayMs(1000);
Show5DigitLedSeg(5,126);//o
Show5DigitLedSeg(4,0);  //_
Show5DigitLedSeg(3,62); //w
Show5DigitLedSeg(2,126);//o
Show5DigitLedSeg(1,70); //r
DelayMs(1000);
Show5DigitLedSeg(5,0);  //_
Show5DigitLedSeg(4,62); //w
Show5DigitLedSeg(3,126);//o
Show5DigitLedSeg(2,70); //r
Show5DigitLedSeg(1,14); //l
DelayMs(1000);
Show5DigitLedSeg(5,62); //w
Show5DigitLedSeg(4,126);//o
Show5DigitLedSeg(3,70); //r
Show5DigitLedSeg(2,14); //l
Show5DigitLedSeg(1,61); //d
DelayMs(1000);
Show5DigitLedSeg(5,126); //o
Show5DigitLedSeg(4,70); //r
Show5DigitLedSeg(3,14); //l
Show5DigitLedSeg(2,61); //d
Show5DigitLedSeg(1,0); //_
DelayMs(1000);
Show5DigitLedSeg(1,70); //r
Show5DigitLedSeg(2,14); //l
Show5DigitLedSeg(3,61); //d
Show5DigitLedSeg(4,0); //_
Show5DigitLedSeg(5, 53); //K 
DelayMs(1000);
Show5DigitLedSeg(1,14); //l
Show5DigitLedSeg(2,61); //d
Show5DigitLedSeg(3,0); //_
Show5DigitLedSeg(4, 53); //K 
Show5DigitLedSeg(5,4); //i
DelayMs(1000);
Show5DigitLedSeg(1,61); //d
Show5DigitLedSeg(2,0); //_
Show5DigitLedSeg(3, 53); //K 
Show5DigitLedSeg(4,4); //i
Show5DigitLedSeg(5,70); //r
DelayMs(1000);
Show5DigitLedSeg(1,0); //_
Show5DigitLedSeg(2, 53); //K 
Show5DigitLedSeg(3,4); //i
Show5DigitLedSeg(4,70); //r
Show5DigitLedSeg(5,4); //i
DelayMs(1000);
Show5DigitLedSeg(1, 53); //K 
Show5DigitLedSeg(2,4); //i
Show5DigitLedSeg(3,70); //r
Show5DigitLedSeg(4,4); //i
Show5DigitLedSeg(5,14); //l
DelayMs(1000);
Show5DigitLedSeg(1,4); //i
Show5DigitLedSeg(2,70); //r
Show5DigitLedSeg(3,4); //i
Show5DigitLedSeg(4,14); //l
Show5DigitLedSeg(5,14); //l
DelayMs(1000);
}
}


