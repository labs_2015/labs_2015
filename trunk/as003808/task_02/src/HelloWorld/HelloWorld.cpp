/*! \mainpage ������ "Hello, World!
* ���������, ��������� �� ������� ������� "Hello, World!"
* \file HelloWorld.cpp
* \brief ���� � �������� ����� �� ����� �++, ��������� �� ������� ������� "Hello, World!"
*/

#include "stdafx.h"
#include <iostream>
using namespace std;

int main() //! ������� ������� ���������, � ������� �������������� ����� �� ������� ������� "Hello, World!". 
{
	/*!
	����� ������� �� �������:
	\code
	cout << "Hello, world!" << endl;
	\endcode
	*/
	cout << "Hello, World!" << endl;
	/*!
	�������, ��������� ������� ����� �������, ������������ ��� ��������� ���������� �������.
	\code
	system("pause");
	\endcode
	*/
	system("pause");
	/*!
	������� main ���������� 0, ���� ��������� ���������.
	\code
	return 0;
	\endcode
	*/
	return 0;
}
