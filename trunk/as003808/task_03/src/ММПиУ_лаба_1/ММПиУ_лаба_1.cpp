/*! \mainpage ������ "������������ ������ �1"
* ���������, ����������� ������������� ������� ����������.
* \n  ������ �������� ������:
*  \image html linearGraphic.png
*  ������ ���������� ������:
*  \image html nonlinearGraphic.png
* \file �����_����_1.cpp
* \brief ���� � �������� ����� �� ����� C++, ����������� ������������� ������� ���������� � ��������� ������� BaseTeapot, LinearTeapot, NotLinearTeapot � ������� IncreasingTemp() � Print().
*/

#include "stdafx.h"
#include "iostream"
#include <math.h>
using namespace std;
//! ����������, ���������� �������� ���������� �����, ������������ � �������.
const float cmt = 8.4; 

 /*!
 \brief ����������� �����, �������� ���������, �������� �������.
 */
class BaseTeapot 
{
public:
	//! ������, ���������� �������� �������� ����������� � ������������ ������� �������.
	float temperature[40]; 
protected:
	//! ����������� ������������ ������, ���������������� ���������� temperature.
	BaseTeapot() : temperature()
	{}
	//! ����������� �������, �������������� �����������.
	virtual void IncreasingTemp() = 0; 
	//! ����������� �������, ��������� �� ������� ���������� ����������.
	virtual void Print() = 0; 
	//! ���������� ������������ ������.
	~BaseTeapot()
	{}
};

/*!
\brief �����, ����������� �������� ������ ������� ����������, ������� ������ BaseTeapot.
*/
class LinearTeapot : public BaseTeapot 
{
private: 
	float temperature[40];
public:
	//! ����������� ������, ���������������� ���������� temperature.
	LinearTeapot() 
	{
		temperature[0] = 0;
	}
	//! �������, �������������� �����������.
	void IncreasingTemp() 
	{
		for (int i = 0; i <= 39; i++)
		{
			temperature[i+1] = 0.988*temperature[i] + 0.232*cmt;
		}
	}
	//! �������, ��������� �� ������� ���������� ����������.
	void Print()
	{
		for (int i = 0; i <= 39; i++)
		{
			cout << "step = " << i + 1 << " \t temperature = " << temperature[i + 1] << endl;
		}
	}
	//! ���������� ������.
	~LinearTeapot()
	{ }
};

/*!
\brief �����, ����������� ���������� ������ ������� ����������, ������� ������ BaseTeapot.
*/
class NotLinearTeapot : public BaseTeapot
{
private:
	float temperature[40];
public:
	//! ����������� ������, ���������������� ���������� temperature.
	NotLinearTeapot() 
	{ 
		temperature[0] = 0;
		temperature[1] = 0;
	}
	//! �������, �������������� �����������.
	void IncreasingTemp()
	{
		for (int i = 1; i <= 39; i++)
		{
			temperature[i+1] = 0.9*temperature[i] - 0.001*temperature[i - 1] * temperature[i - 1] + cmt + sin(cmt);
		}
	}
	//! �������, ��������� �� ������� ���������� ����������.
	void Print()
	{
		for (int i = 0; i <= 39; i++)
		{
			cout << "step = " << i + 1 << " \t temperature = " << temperature[i + 1] << endl;
		}
	}
	//! ���������� ������.
	~NotLinearTeapot()
	{}
};

//! ������� �������, ��������� ������� �������, ����������� ������� �������� ���������� � ������ �������� �� �������
int main() 
{
	LinearTeapot *a = new LinearTeapot;
	NotLinearTeapot *b = new NotLinearTeapot;
	a->IncreasingTemp();
	a->Print();
	b->IncreasingTemp();
	cout << endl << endl;
	b->Print();
	system("pause");
	return 0;
}

