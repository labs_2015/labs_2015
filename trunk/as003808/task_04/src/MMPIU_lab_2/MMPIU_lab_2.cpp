/*! \mainpage ������ "������������ ������ �2"
* ���������, ����������� ������������� PID-����������.
* \n  ������ �������� ������:
*  \image html linearGraphic.png
*  ������ ���������� ������:
*  \image html nonlinearGraphic.png
* \file �����_����_2.cpp
* \brief ���� � �������� ����� �� ����� C++, ����������� ������������� PID-���������� � ��������� ������� BaseTeapot, LinearTeapot, NotLinearTeapot � ������� ChangeTempValue().
*/
#include "stdafx.h"
#include <iostream>
#include <math.h>
//! ����������� ��������� ������������ ������, ���������� ������.
#include "BaseTeapot.h"
#include "LinearTeapot.h"
#include "NotLinearTeapot.h"
using namespace std;

//! ������� �������, ��������� ������� �������, ����������� ������� �������� � ������ �������� �� �������.
int main()
{
	LinearTeapot *linear = new LinearTeapot;
	NotLinearTeapot *not_linear = new NotLinearTeapot;
	linear->ChangeTempValue();
	cout << endl << endl;
	not_linear->ChangeTempValue();
	system("pause");
	return 0;
}