/*!
* \file NotLinearTeapot.h
* \brief ������������ ���� � �������� ����� �� ����� C++, ���������� �������� ������ NotLinearTeapot � ������� ChangeTempValue().
*/
/*!
\brief �����, ������������ ���������� ���������, ������� ������ BaseTeapot.
*/
#pragma once
#include "BaseTeapot.h"
#include <iostream>
#include <iomanip>
#include <math.h>

class NotLinearTeapot : public BaseTeapot
{
private:
	double u, w, y, y1, T_integral_constant, T_differential_constant, k_coefficient, T0, e1, e2, e3, q0, q1, q2;
public:
	//! ����������� ������
	NotLinearTeapot::NotLinearTeapot()
	{
		//! ���������� ������������ �����������
		u = 0;
		//! �������� ��������
		w = 50;
		//! �������� ����������
		y = 0; y1 = 0;
		//! ���������� ��������������.
		T_integral_constant = 0.4;
		//! ���������� �����������������.
		T_differential_constant = 0.1;
		//! ����������� ��������.
		k_coefficient = 0.5;
		//! ��� �� �������.
		T0 = 0.5;
		//! ��������� PID-����������.
		e1 = 0; e2 = 0; e3 = 0;
		q0 = 0; q1 = 0; q2 = 0;
	}
	//! �������, �������������� � ��������� �� ������� �������� ��� ����������� ���������.
	void ChangeTempValue()
	{
		std::cout << "Nonlinear" << std::endl << "#" << std::setw(15) << "Y" << "\t" << "U" << std::endl << "-----------------------------------" << std::endl;
		for (int i = 0; i <= 20; i++)
		{
			q0 = k_coefficient*(1 + T_differential_constant / T0);
			q1 = -k_coefficient*(1 + 2 * T_differential_constant / T0 - T0 / T_integral_constant);
			q2 = k_coefficient*(T_differential_constant / T0);
			e3 = e2;
			e2 = e1;
			e1 = w - y;
			u += q0*e1 + q1*e2 + q2*e3;
			y1 = 0.9*y1 - 0.001*y * y + u + sin(u);
			y = y1;
			std::cout << i << ") " << std::setw(15) << y << "\t" << u << std::endl;
		}
	}
	//! ���������� ������.
	NotLinearTeapot::~NotLinearTeapot()
	{ }
};