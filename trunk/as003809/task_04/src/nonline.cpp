
/** \file
\brief  �������� �������� ������ pid
*/
#include "stdafx.h"
#include "nonline.h"
#include <cmath>
	
	double  NonLine::modeling(double Y, double U)
	{
		double ans = 0.9*Y - 0.001*pow(old_Y, 2) + U + sin(old_U);

		old_U = U;
		old_Y = Y;

		return ans;
	}

	NonLine::NonLine() : ControlObject()
	{
		old_U = 0;
		old_Y = 0;
	}
