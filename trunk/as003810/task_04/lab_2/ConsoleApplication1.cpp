///@file ConsoleApplication1.cpp
///@brief �������� �������� ��� ��������� ��� ������������ ������ �2.
///@author ������� �.�.

///@mainpage
///@image html line.png 
///@image html nonLine.png 
///@brief ���������� �������� , ��� ���������� ������ ������������ �������




#include "stdafx.h"
#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;
///@class ��������� ����������� ������ ����������. �������� ������� ��� �������� �������� ����������.
class ControlObject
{

public:
	virtual double modeling(double, double) = 0;

	ControlObject(){}
};
///@class ��������� �������� ������ ������� ����������. �������� ����������� ������ ControlObject.
class line : public ControlObject
{
public:
	/// ��������� ���������� ������������� �������� �������
	double modeling(double Y, double U)
	{
		return 0.988*Y + 0.232*U;
	}

	line() : ControlObject() {};
	~line() {}

};
///@class ��������� ���������� ������ ������� ����������. �������� ����������� ������ ControlObject.
class NonLine : public ControlObject
{
protected:
	double old_U ;
	double old_Y;
public:
	///@biref ��������� ���������� ������������� ���������� �������
	double modeling(double Y, double U)
	{
		double ans = 0.9*Y - 0.001*pow(old_Y, 2) + U + sin(old_U);

		old_U = U;
		old_Y = Y;
		
		return ans;
	}
			
	NonLine() : ControlObject() 
	{
		old_U = 0;
		old_Y = 0;
	}

	~NonLine() {}
};
///@class ������������� ���-����������
class pid
{
private:
	///@param T0 - ��� �� �������
	///@param Td - ��������� ���������.
	///@param T - ��������� ��������.
	///@param K - ����������� ��������

	double T0, Td, T, K, q0, q1, q2;
	double e1 = 0, e2 = 0, e3 = 0, U = 0;
public:
	///@brief ������� calc_q ������������ �������� q
	void calc_q()
	{
		q0 = K* (1 + (Td / T0));
		q1 = -K *(1 + 2 * (Td / T0) - (T0 / T));
		q2 = K * (Td / T0);
	}
	///@brief�������� ������ ���-����������
	double Calc(double Y, double W)
	{
		e3 = e2;
		e2 = e1;
		e1 = W - Y;
		U += (q0 * e1 + q1 * e2 + q2 * e3);
		return U;
	}
	pid(double T0 = 0.1 , double Td = 0.02,  double T= 0.4, double K = 0.4)
	{
		this->T0 = T0;
		this->Td = Td;
		this->T = T;
		this->K = K;
	}

	~pid(){ }

};

///@brief������ ���������������� ��������
int main()
{
	line lineObj;

	NonLine nonlineObj;
	
	pid PID;
	PID.calc_q();

	double Y = 30;
	double U = 30;

	double W = 50;


	for (int i = 0; i != 100; i++)
	{
		U = PID.Calc(Y, W);
		Y = lineObj.modeling(Y, U);
		cout << i << "  " << Y << endl;
	}
	cout << "--------------------------------------------" << endl;
	
	
	Y = 20;
	U = 30;
	for (int i = 0; i != 20; i++)
	{
		U = PID.Calc(Y, W);
		Y = nonlineObj.modeling(Y, U);
		cout << i << "  " << Y << endl;
	}

	return 0;
}