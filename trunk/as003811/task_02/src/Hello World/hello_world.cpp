/*! \mainpage ������ "Hello, world!"
* ���������, ����������� ����� � ������� ������ "Hello, World!".
* \file Hello World.cpp
* \brief ���� � ����� �� ����� C++, ������� ������� � ������� ������� "Hello, World!"
*/

#include "stdafx.h" 
#include <iostream> 
using namespace std;
int main() //! �������� ������� ���������. ����� �������������� ����� ������� �� �������. 
{
	/*!
	����� ������� �� �����:
	\code
	cout � "Hello, World!" � endl;
	\endcode
	*/
	cout << "Hello, World!" << endl;
	/*!
	������ ��������� �� ����� ��� ��������� ���������� �������:
	\code
	system("pause");
	\endcode
	*/
	system("pause");
	/*!
	��� �������� ���������� ��������� ������� main ���������� ����:
	\code
	return 0;
	\endcode
	*/
	return 0;
}