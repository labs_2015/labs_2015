/*! \mainpage Project "������������ ������ �2"
*  \image html linear.png 
* * <b><center> ��� �������� ������ </b></center>
*  \image html nonlinear.png
* <b><center>  ��� ���������� ������ </b></center>
* \author ��������� �����
* \file main.cpp
* \brief �������� ������� main.
*/

#include "Model.h"
#include "LinearModel.h""
#include "NonLinearModel.h"		
#include <iostream>
using namespace std;

//!������������ ������ ��������� �������.
int main()
{
	LinearModel *ObjectLinearModel= new LinearModel;
	ObjectLinearModel->ShowResult();
	NonLinearModel *ObjectNonLinearModel= new NonLinearModel;
	ObjectNonLinearModel->ShowResult();
	system("pause");
	return 0;
}

