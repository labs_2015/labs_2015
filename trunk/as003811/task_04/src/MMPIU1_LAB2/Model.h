/*! \file Model.h
* \brief ������������ ����, ���������� � ���� �������� ������ Model
*/
#ifndef _MODEL_H
#define _MODEL_H

/*!����������� ����� Model.
* \brief �����, ���������� ����� ��� ������ ������� ���� � ������
*/
class Model
{
public: 
	
	double Ti = 0.32;
	
	double Td = 0.12;
	
	double K = 0.63;
	
	double T0 = 0.5;
	
	double e1 = 0;
	
	double e2 = 0;
	
	double e3 = 0;
	
	double q0 = 0;
	
	double q1 = 0;
	
	double q2 = 0;
	
	double u = 0;
	 
	double w = 60;
	
	double y = 0;
	
	double y1 = 0;
	
	double y2 = 0;
	//! ������������� ���-���������
	void Model::PidController();
	//! ����������� ����������� �������������
	virtual void ShowResult() = 0;
	Model();
	~Model();
};

#endif