/*! \mainpage Project "Hello world!!!"
* <b> ��������� ������� � ������� "Hello world!". </b>
* \file HelloWorld.cpp
* \brief �������� ���� � ��������� ������� main().
*/

#include <iostream>
#include <conio.h>
using namespace std;



int main()//! ����� ����� � ���������. � ��� �������������� ������������� � ����� ������. 
{
	/*!
	����� �������� �� �������:
	\code
	cout <<"HelloWorld!!!"<<endl;
	\endcode
	*/
	cout << "HelloWorld!!!" << endl;
	_getch();
	return 0;
}