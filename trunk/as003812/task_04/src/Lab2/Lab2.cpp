/*! \mainpage ������������ ������ �2
* @author ������ �������
* \n ���������, ����������� ������������� PID-����������.
*  \n ������ �������� ������: 
*  \image html LinearModel.PNG
*  ������ ���������� ������:
*  \image html NonLinearModel.PNG
* \file Lab2.cpp
* \brief �������� ���� � ����� �� C++, ������� ��������� ������������� PID-����������.
*/
#include <iostream.h>

#include <math.h>
//! ���������� ��������� ������
#include "basepot.h"
#include "LinearTeapot.h"
#include "NotLinearTeapot.h"
using namespace std;
int main(int argc, char* argv[]) //! ������� main, � ������� �� ������ ������� ����� �������, ������������ �������� ��� ��� � ������� ��������� �� �������.
{
LineTeapot a;
NotLineTeapot b;
a.RasValue();
b.RasValue();
system("pause");
        return 0;
};

//---------------------------------------------------------------------------
 