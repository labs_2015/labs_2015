/**
@mainpage  a string with a greeting is displayed on the seven-segment display
@file
@author Eugene Korol
@date 12.12.15
*/
#include "i7188.h"

int main()
{
	while(1) 
	{
		Show5DigitLedSeg(1, 118);	
		Show5DigitLedSeg(2, 103);	
		Show5DigitLedSeg(3, 28);	
		Show5DigitLedSeg(4, 127);	
		Show5DigitLedSeg(5, 79);	
		DelayMs(1000);
		Show5DigitLedSeg(1, 103);
		Show5DigitLedSeg(2, 28);
		Show5DigitLedSeg(3, 127);
		Show5DigitLedSeg(4, 79);
		Show5DigitLedSeg(5, 112);    
		DelayMs(1000);
		Show5DigitLedSeg(1, 28);
		Show5DigitLedSeg(2, 127);
		Show5DigitLedSeg(3, 79);
		Show5DigitLedSeg(4, 112);
		Show5DigitLedSeg(5, 64);    
		DelayMs(1000);
		Show5DigitLedSeg(1, 127);	
		Show5DigitLedSeg(2, 79);
		Show5DigitLedSeg(3, 112);
		Show5DigitLedSeg(4, 64);
		Show5DigitLedSeg(5, 0);	   
		DelayMs(1000);
		Show5DigitLedSeg(1, 79);	
		Show5DigitLedSeg(2, 112);
		Show5DigitLedSeg(3, 64);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 78);  
		DelayMs(1000);
		Show5DigitLedSeg(1, 112);	
		Show5DigitLedSeg(2, 64);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 78);
		Show5DigitLedSeg(5, 119); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 64);	
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 78);
		Show5DigitLedSeg(4, 119);
		Show5DigitLedSeg(5, 127); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 0);	
		Show5DigitLedSeg(2, 78);
		Show5DigitLedSeg(3, 119);
		Show5DigitLedSeg(4, 127);
		Show5DigitLedSeg(5, 28); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 78);	
		Show5DigitLedSeg(2, 119);
		Show5DigitLedSeg(3, 127);
		Show5DigitLedSeg(4, 28);
		Show5DigitLedSeg(5, 55); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 119);	
		Show5DigitLedSeg(2, 127);
		Show5DigitLedSeg(3, 28);
		Show5DigitLedSeg(4, 55);
		Show5DigitLedSeg(5, 79); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 127);	
		Show5DigitLedSeg(2, 28);
		Show5DigitLedSeg(3, 55);
		Show5DigitLedSeg(4, 79);
		Show5DigitLedSeg(5, 35); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 28);	
		Show5DigitLedSeg(2, 55);
		Show5DigitLedSeg(3, 79);
		Show5DigitLedSeg(4, 35);
		Show5DigitLedSeg(5, 17); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 55);	
		Show5DigitLedSeg(2, 79);
		Show5DigitLedSeg(3, 35);
		Show5DigitLedSeg(4, 17);
		Show5DigitLedSeg(5, 0);  
		DelayMs(1000);
		Show5DigitLedSeg(1, 79);	
		Show5DigitLedSeg(2, 35);
		Show5DigitLedSeg(3, 17);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 127); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 35);	
		Show5DigitLedSeg(2, 17);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 127);
		Show5DigitLedSeg(5, 126); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 17);	
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 127);
		Show5DigitLedSeg(4, 126);
		Show5DigitLedSeg(5, 127); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 0);	
		Show5DigitLedSeg(2, 127);
		Show5DigitLedSeg(3, 126);
		Show5DigitLedSeg(4, 127);
		Show5DigitLedSeg(5, 119); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 127);	
		Show5DigitLedSeg(2, 126);
		Show5DigitLedSeg(3, 127);
		Show5DigitLedSeg(4, 119);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
		Show5DigitLedSeg(1, 126);	
		Show5DigitLedSeg(2, 127);
		Show5DigitLedSeg(3, 119);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
		Show5DigitLedSeg(1, 127);	
		Show5DigitLedSeg(2, 119);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
		Show5DigitLedSeg(1, 119);	
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
		Show5DigitLedSeg(1, 0);	
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
	}
}