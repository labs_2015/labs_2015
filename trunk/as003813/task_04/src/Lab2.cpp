///@file
///@author ������� �������
///@03.12.2015

#include <iostream>
#include <math.h>
using namespace std;
/**@class AbstactClass
����������� �����
*/
class AbstactClass {
public:
	/// ��� �� �������
	double T0 = 0.5;
	///���������� �����������������
	double Td = 0.02;
	///���������� ��������������
	double T = 0.5; 
	///����������� ��������
	double k = 0.5;
	/// �������� ����������
	double q0; 
	/// �������� ����������
	double q1;
	/// �������� ����������
	double q2;
	///���������� �������� ���������� y(t) �� ��������� �������� w(t).
	double e1 = 0, e2 = 0, e3 = 0;
	///����������� �����������
	double u = 0;
	///�������� ��������
	double w = 40; 
	///�������� ����������
	double y = 0;
	void pid()
	{
		q0 = k*(1 + (Td / T0));
		q1 = -k *(1 + 2 * (Td / T0) - (T0 / T));
		q2 = k * (Td / T0);
		e3 = e2;
		e2 = e1;
		e1 = w - y; 
		u += (q0 * e1 + q1 * e2 + q2 * e3);
	}
protected:
	virtual void vivod() = 0;
};
/**@class LineinayaModel
��������� ����� �� ��������(������������)
*/
class LineinayaModel : public AbstactClass
{
public:
	void vivod()
	{
		cout << "LineinayaModel\n\n";
		cout.width(8);
		cout << "Y" << "  |  " << "U\n";
		double  y_1 = 0;
		for (int i = 0; i <= 20; i++)
		{
			pid();
			y_1 = 0.988*y + 0.232*u;
			y = y_1;
			cout.width(8);
			cout << y_1 << "  |  " << u << endl;
		}
	}
};
/**@class NeLineinayaModel
��������� ����� �� ��������(������������)
*/
class NeLineinayaModel : public AbstactClass
{
public:
	void vivod()
	{
		double y_2 = 0, y_1 = 0;

		cout << "\nNeLineinayaModel\n\n";
		cout.width(8);
		cout << "Y" << "  |  " << "U\n";
		for (int i = 1; i <= 20; i++)
		{
			pid();
			y_2 = 0.9*y_1 - 0.001*y * y + u + sin(u);
			y_1 = y_2;
			y = y_1;
			cout.width(8);
			cout << y_2 << "  |  " << u << endl;
		}
	}
};
/**@mainpage ���-����������
@image html graph.png
\details �� �������� �����, ��� ���������� ������ ������������ �������, ��� ��������
*/
int main(int argc, char* argv[])
{
	LineinayaModel a;
	NeLineinayaModel b;
	a.vivod();
	b.vivod();
	system("pause");
	return 0;
};
