/**
@mainpage  Seven-segment display
@file
@author Artem Lavrushchyk
@Vyvod HELLO sleva napravo
@date 17.12.15
*/
#include "I7188.h"

int main()
{
	while(1) 
	{
		
		Show5DigitLedSeg(1, 0);	
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
		Show5DigitLedSeg(1, 103);	
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
		Show5DigitLedSeg(1, 25);	
		Show5DigitLedSeg(2, 103);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
		Show5DigitLedSeg(1, 103);	
		Show5DigitLedSeg(2, 25);
		Show5DigitLedSeg(3, 103);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
		Show5DigitLedSeg(1, 119);	
		Show5DigitLedSeg(2, 103);
		Show5DigitLedSeg(3, 25);
		Show5DigitLedSeg(4, 103);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
		Show5DigitLedSeg(1, 114);	
		Show5DigitLedSeg(2, 119);
		Show5DigitLedSeg(3, 103);
		Show5DigitLedSeg(4, 25);
		Show5DigitLedSeg(5, 103);
		DelayMs(1000);
		Show5DigitLedSeg(1, 5);	
		Show5DigitLedSeg(2, 114);
		Show5DigitLedSeg(3, 119);
		Show5DigitLedSeg(4, 103);
		Show5DigitLedSeg(5, 25); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 0);	
		Show5DigitLedSeg(2, 5);
		Show5DigitLedSeg(3, 114);
		Show5DigitLedSeg(4, 119);
		Show5DigitLedSeg(5, 103); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 126);	
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 5);
		Show5DigitLedSeg(4, 114);
		Show5DigitLedSeg(5, 119);  
		DelayMs(1000);
		Show5DigitLedSeg(1, 14);	
		Show5DigitLedSeg(2, 126);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 5);
		Show5DigitLedSeg(5, 114);	   
		DelayMs(1000);
		Show5DigitLedSeg(1, 14);
		Show5DigitLedSeg(2, 14);
		Show5DigitLedSeg(3, 126);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 5);    
		DelayMs(1000);
		Show5DigitLedSeg(1, 79);
		Show5DigitLedSeg(2, 14);
		Show5DigitLedSeg(3, 14);
		Show5DigitLedSeg(4, 126);
		Show5DigitLedSeg(5, 0);    
		DelayMs(1000);
		Show5DigitLedSeg(1, 55);	
		Show5DigitLedSeg(2, 79);	
		Show5DigitLedSeg(3, 14);	
		Show5DigitLedSeg(4, 14);	
		Show5DigitLedSeg(5, 126);	
		DelayMs(1000);
	}
}