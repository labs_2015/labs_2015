///@file
///@author ������ �������
#include "stdafx.h"
#include <iostream>
#include <math.h>
/**@mainpage ������������ ������ �1
*\image html linear_graph.png
*\details ������ �������� �����������.
*\image html nonlinear_graph.png
*\details ������ ���������� �����������.
*/
using namespace std;
///@brief ������� ����������� �����.
class Base_Class {
public:
	float Y[20];
	float U = 10;
protected:
	///@brief ����������� ������� ������, ������� ����� ���������� ���������� ������������.
	virtual void Temp() = 0;
};
///@brief �����, ����������� �������� ������. �������� ����������� �������� ������ Base_Class.
class Lin_Model : public Base_Class
{
public:
	void Temp()
	{
		cout << "Lineinaya\n\n";
		cout.width(8);
		cout << "Y" << "  |  " << "U\n";
		Y[0] = 0;
		for (int i = 0; i <= 20; i++)
		{
			Y[i + 1] = 0.988*Y[i] + 0.232*U;
			cout.width(8);
			cout << Y[i] << "  |  " << i << endl;
		}
	}
};
///@brief �����, ����������� ���������� ������. �������� ����������� �������� ������ Base_Class.
class Nonlin_Model : public Base_Class
{
public:
	void Temp()
	{
		Y[0] = 0;
		Y[1] = 0;
		cout << "\nNeLineinaya\n\n";
		cout.width(8);
		cout << "Y" << "  |  " << "U\n";
		for (int i = 1; i <= 20; i++)
		{
			Y[i + 1] = 0.9*Y[i] - 0.001*Y[i - 1] * Y[i - 1] + U + sin(U);
			cout.width(8);
			cout << Y[i] << "  |  " << i << endl;
		}
	}
};
///@brief �������, ������� ������ ������� �������, � ����������� ������ ��� ����.
int main(int argc, char* argv[])
{
	Lin_Model l;
	Nonlin_Model n;
	l.Temp();
	cout << endl;
	n.Temp();
	system("pause");
	return 0;
};
