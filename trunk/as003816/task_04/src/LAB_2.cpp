/*! @mainpage Project "Lab 2"
* @author ������ �.�.
* \details �� C++  ����������� ���������, ������������ ������������� ���� ���-���������.  � �������� ������� ���������� ������������ �������������� ������, ���������� � ���������� ������. ������������ ���, � ��������� ������ ���� �� ����� 5-� ������� (+������������).
* \n <b> <center>������ �������� ������:</center> </b>
*  \image html LinMod.png
*<br> <b> <center>������ ���������� ������:</center> </b>
*  \image html NonMod.png
*\details ���� �� ��������, ������������� ���������� ������ ���������� �������, ��� ��������.
*/
#include "stdafx.h"
#include <iostream>
#include <math.h>
#include "BaseClass.h"
#include "Reg_PID.h"
#include "LinModel.h"
#include "ShowLinModel.h"
#include "NonLinModel.h"
#include "ShowNonLinModel.h"
using namespace std;
///@brief �������, ������� ������� ������� ������ � ��������� ��� ���� ������.
int main(int argc, char* argv[])
{
	setlocale(0, "");
	ShowNonLinModel n;
	ShowLinModel l;
	n.show();
	l.show();
	system("pause");
	return 0;
};
