#include "stdafx.h"
#include "BaseTeapot.h"

class BaseTeapot
{
protected:
	BaseTeapot::BaseTeapot()
	{ }
	virtual void ChangeTempValue() = 0;
	BaseTeapot::~BaseTeapot()
	{ }
};