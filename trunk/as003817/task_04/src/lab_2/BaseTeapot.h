/*!
* \file BaseTeapot.h
* \brief ������������ ���� � �������� ����� �� ����� C++, ���������� �������� �������� ������������ ������ BaseTeapot.
*/
/*!
\brief ����������� �����, �������� ���������, �������� �������.
*/
#pragma once

class BaseTeapot
{
protected:
	//! ����������� ������������ ������
	BaseTeapot::BaseTeapot()
	{}
	//! ����������� �������, �������������� ��������.
	virtual void ChangeTempValue() = 0;
	//! ���������� ������������ ������.
	BaseTeapot::~BaseTeapot()
	{}
};