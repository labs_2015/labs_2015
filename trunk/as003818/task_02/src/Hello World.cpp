/*!
@mainpage  A console application that displays the "Hello World!"
@file
@author ILiya Polhovsky
*/
#include "stdafx.h"
#include <iostream>
using namespace std;
/*!
@brief Define an entry point for program
*/
int main()
{
	/*!
	@brief Function, that display message "Hello,world!"
	*/
	cout << "Hello, world!" << endl;
	system("pause");
	return 0;
}
