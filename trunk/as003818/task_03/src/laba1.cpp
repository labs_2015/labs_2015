///@file
///@author ���� ����������
/**@mainpage ������������ ������ �1
*\image html Linear.png
*\details ����������� ������� �������� �������� .
*\image html NonLinear.png
*\details ����������� ������� �������� ����������.
*/

#include "stdafx.h"
#include <iostream>
#include <math.h>
using namespace std;
///@brief ����������� �����,������� �������� ������� ��� �����������.
class Class1 {
public:
	float A[20];
	float B = 10;
protected:
	///@brief ����������� ����� ������,������ ���������� ���������� ������������ �� �������.
	virtual void temp() = 0;
};
///@brief ��������� �������� ������. �������� ����������� �������� ������ Class1.
class Model_1 : public Class1
{
public:
	void temp()
	{
		cout << "Lineinaya\n\n";
		cout.width(8);
		cout << "A" << "  |  " << "B\n";
		A[0] = 0;
		for (int i = 0; i <= 20; i++)
		{
			A[i + 1] = 0.988*A[i] + 0.232*B;
			cout.width(8);
			cout << A[i] << "  |  " << i << endl;
		}
	}
};
///@brief ��������� ���������� ������. �������� ����������� �������� ������ Class1.
class Model_2 : public Class1
{
public:
	void temp()
	{
		A[0] = 0;
		A[1] = 0;
		cout << "\nNeLineinaya\n\n";
		cout.width(8);
		cout << "A" << "  |  " << "B\n";
		for (int i = 1; i <= 20; i++)
		{
			A[i + 1] = 0.9*A[i] - 0.001*A[i - 1] * A[i - 1] + B + sin(B);
			cout.width(8);
			cout << A[i] << "  |  " << i << endl;
		}
	}
};
///@brief �������, � ������� ���������� �������� �������� ������, � ���������� ������� ��� ����.
int main(int argc, char* argv[])
{
	Model_1 a;
	Model_2 b;
	a.temp();
	b.temp();
	system("pause");
	return 0;
};
