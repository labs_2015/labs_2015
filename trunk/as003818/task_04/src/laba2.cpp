///@file
///@author ���� ����������
/**@mainpage ������������ ������ �2. ������������� ���-����������.
*\image html linear.png
*\image html nonlinear.png
*\details �� ���������� �������� ����� ������, ��� ������������� ���������� ������ ���������� �������, ��� ��������.
*/
#include "stdafx.h"
#include <iostream>
#include <math.h>
using namespace std;
///@brief ����������� �����,������� �������� ������� ��� �����������.
class Class1 {
public:
	//! s - �������� ����������
	double s0;
	double s1;
	double s2;
	//! ��� �� �������
	double V0 = 0.5;
	//!���������� �����������������
	double Vd = 0.02;
	//!���������� ��������������
	double V = 0.5;
	double n = 0.5;
	double e1 = 0, e2 = 0, e3 = 0;
	double u = 0;
	double w = 40;
	double y = 0;
	///@brief ������������� ���-����������
	void regulyator_PID()
	{
		s0 = n*(1 + (Vd / V0));
		s1 = -n *(1 + 2 * (Vd / V0) - (V0 / V));
		s2 = n * (Vd / V0);
		e3 = e2;
		e2 = e1;
		e1 = w - y;
		u += (s0 * e1 + s1 * e2 + s2 * e3);
	}
protected:
	virtual void pokaz() = 0;
};
///@brief ��������� �������� ������. �������� ����������� �������� ������ Class1.
class Model_1 : public Class1
{
public:
	void pokaz()
	{
		cout << "Lineinaya\n\n";
		cout.width(8);
		cout << "A" << "  |  " << "B\n";
		double  y_1 = 0;
		for (int i = 0; i <= 20; i++)
		{
			regulyator_PID();
			y_1 = 0.988*y + 0.232*u;
			y = y_1;
			cout.width(8);
			cout << y_1 << "  |  " << u << endl;
		}
	}
};
///@brief ��������� ���������� ������. �������� ����������� �������� ������ Class1.
class Model_2 : public Class1
{
public:
	void pokaz()
	{
		double y_2 = 0, y_1 = 0;

		cout << "\nNeLineinaya\n\n";
		cout.width(8);
		cout << "A" << "  |  " << "B\n";
		for (int i = 1; i <= 20; i++)
		{
			regulyator_PID();
			y_2 = 0.9*y_1 - 0.001*y * y + u + sin(u);
			y_1 = y_2;
			y = y_1;
			cout.width(8);
			cout << y_2 << "  |  " << u << endl;
		}
	}
};
///@brief �������, � ������� ���������� �������� �������� ������, � ���������� ������� ��� ����.
int main(int argc, char* argv[])
{
	Model_1 a;
	Model_2 b;
	a.pokaz();
	b.pokaz();
	system("pause");
	return 0;
};
