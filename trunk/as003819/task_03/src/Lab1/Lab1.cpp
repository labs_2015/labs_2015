/*! \mainpage ������ "������������ ������ �1"
* ���������, ����������� ������������� ������� ����������.
*  ������ �������� ������: 
*  \image html Linear.png
*  ������ ���������� ������:
*  \image html Nonlinear.png
* \file Lab1.cpp
* \brief �������� ���� � ����� �� C++, ������� ��������� ������������� ������� ����������.
*/
#include "stdafx.h"
#include <iostream.h>
#include <math.h>
using namespace std;
//! ����������, ������� �������� �������� ���������� �����, ������������ � �������.
float cmt=8.4; 
class BasePot /// ����������� �����, ���������� ��������� ��� ��������� �������. 
{
public:
float T[40];
protected:
BasePot ()
{
}
~BasePot ()
{
}
/// ������� �������� ����� �����������.
virtual void RasTemp()=0;
/// ������� ������ �� ����� ���������� ��������.
virtual void print()=0; 
};
class LineTeapot: public BasePot ///�����, ����������� ��������� �������� ������ ������� ����������.
{public:
LineTeapot()
{
T[0]=0;
}
~LineTeapot()
{
}
 void RasTemp()
	{
		for (int i = 0; i <= 38; i++)
			T[i+1] = 0.988*T[i] + 0.232*cmt; }
void print()
{
for (int i = 0; i <= 39; i++)
cout << T[i] << endl;
}};
class NotLineTeapot: public BasePot ///�����, ����������� ��������� ���������� ������ ������� ����������.
{public:
NotLineTeapot()
{
T[0]=0;
T[1]=0;
}
~NotLineTeapot()
{
}
void RasTemp()
{for (int i = 1; i <= 38; i++)
{T[i+1] = 0.9*T[i] - 0.001*T[i - 1] * T[i - 1] + cmt + sin(cmt);
}
}
void print()
{
for (int i = 1; i <= 39; i++)
cout << T[i] << endl;
}
};
int main(int argc, char* argv[]) //! ������� main, � ������� �� ������ ������� ����� �������, ������������ �������� ����������� ��� ��� � ������� ��������� �� �������.
{
LineTeapot a;
NotLineTeapot b;
a.RasTemp();
a.print();
b.RasTemp();
b.print();
system("pause");
        return 0;
};
//---------------------------------------------------------------------------















