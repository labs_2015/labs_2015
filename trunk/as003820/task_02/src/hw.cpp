/**
@mainpage A console application that displays the "hello world"
@file
@author Aleksandr Popko
@date 07.11.15
*/
#include <iostream>
using namespace std;
/**
@brief We define an entry point in the program
*/
int main()
{
	/**
	@brief Display message "hello world"
	*/
	cout << "hello world" << endl;
	///@brief We detain the console screen
	system("pause");
	return 0;
}

