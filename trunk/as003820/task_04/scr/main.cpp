///@file
///@author Aleksandr Popko	
///@29.11.2015

#include <iostream>
#include <cmath>
#include <fstream>

using namespace std;
/**@class TemperatureChange
Absctarct class
*/
class TemperatureChange
{
public:
	///@param parameters for function PID
	///time step
	double T0 = 0.5;
	///Permanent differentiation
	double Td = 0.01;
	///constant of integration
	double T = 0.85;
	///transfer Ratio
	double k = 0.5;
	///The controller parameters:
	double q0;
	double q1;
	double q2;
	///The deviation of the output variable y (t) from a desired value (t).
	double e1 = 0, e2 = 0, e3 = 0;
	///control action
	double u = 0;
	///The desired value
	double w = 30;
	///The output variable
	double y = 0;
	void PID()
	{
		q0 = k*(1 + (Td / T0));
		q1 = -k *(1 + 2 * (Td / T0) - (T0 / T));
		q2 = k * (Td / T0);
		e3 = e2;
		e2 = e1;
		e1 = w - y;
		u += (q0 * e1 + q1 * e2 + q2 * e3);
	}
protected:
	virtual void show() = 0;///virtual method show
};
/**@class LinearTemperatureChange
Inheriting a class from TemperatureChange
*/
class LinearTemperatureChange : public TemperatureChange///line class
{
public:
	void show()///overload functions
	{
		ofstream outText("D:\\University\\3_course\\1_sem\\MPiY\\lab3\\ITOG.txt", ios::out);
		if (!outText.is_open())///If you can not open
		{
			cout << "Error" << endl;
			system("pause");
			exit(1);///completion of the program
		}
		cout << "Linear change:" << endl;
		cout << "Y         | T" << endl;
		double Y_line = 0.0;
		Y_line = 0;///first element
		cout.width(10);//widtch = 10
		for (int i = 0; i < 100; i++)
		{
			PID();
			cout.width(10);
			Y_line = 0.988*y + 0.232*u;///Equation of Manuals
			y = Y_line;
			cout << left << Y_line << "| " << u << "| " << i << endl;
			outText << left << i << "\t" << Y_line << endl;
		}
		outText.close();
	}
};


/**@class NonlinearTemperatureChange
Inheriting a class from TemperatureChange
*/
class NonlinearTemperatureChange : public TemperatureChange///NoLine class
{
public:
	void show()///overload functions
	{
		cout << "Nonlinear change:" << endl;
		cout << "Y           |  T" << endl;
		ofstream outText("D:\\University\\3_course\\1_sem\\MPiY\\lab3\\ITOG2.txt", ios::out);
		if (!outText.is_open())///If you can not open
		{
			cout << "Error" << endl;
			system("pause");
			exit(1);///completion of the program
		}
		double Y_Noline = 0.0, Y_1 = 0.0;
		for (int i = 0; i < 50; i++)
		{
			PID();
			Y_Noline = 0.9*Y_1 - 0.001*y * y + u + sin(u);
			Y_1 = Y_Noline;
			y = Y_1;
			cout.width(10);
			cout << left << Y_Noline << "  |  " << u << "  |  " << i << endl;
			outText << left << i << "\t" << Y_Noline << endl;
		}
		outText.close();
	}
};
/**@mainpage PID controller
@image html image.png
@brief details Looking at the chart, you will notice that the linear model is adjusted more slowly than non-linear
*/
int main()
{
	LinearTemperatureChange line;
	NonlinearTemperatureChange NoLine;
	line.show();
	NoLine.show();
	system("pause");
	return 0;
}