#pragma once
#include "GeneralModel.h"
class GeneralModel
{
public:
	GeneralModel();
	virtual void PrintModel(int t) = 0;
	~GeneralModel();
};


