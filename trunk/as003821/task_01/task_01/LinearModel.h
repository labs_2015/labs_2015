
#include "GeneralModel.h"
class LinearModel :
	public GeneralModel
{
private:
	double yt;
	double ut;
public:
	LinearModel();
	LinearModel(double yt, double ut);
	void PrintModel(int t);
	~LinearModel();
};
