
#include "GeneralModel.h"
class NonlinearModel :
	public GeneralModel
{
private:
	double yt;
	double ut;
public:
	NonlinearModel();
	NonlinearModel(double y0, double ut);
	void PrintModel(int t);
	~NonlinearModel();
};


