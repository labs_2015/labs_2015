#include "stdafx.h"
#include<iostream>
#include"GeneralModel.h"
#include"LinearModel.h"
#include"NonlinearModel.h"
#include<iostream>

using namespace std;
void main()
{
	LinearModel linMod(3, 50);
	NonlinearModel nonlinMod(5, 67);

	linMod.PrintModel(12);
	cout << endl;
	nonlinMod.PrintModel(10);
	cout << endl;
	system("PAUSE");
}