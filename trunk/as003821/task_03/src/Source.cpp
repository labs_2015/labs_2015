/**
@file Source.cpp
@author ������ ���� ��-38
@date 01.11.2015
*/
#include"stdafx.h"
#include<iostream>
#include"GeneralModel.h"
#include"LinearModel.h"
#include"NonlinearModel.h"


using namespace std;
/**
@brief ������� �� ������� �������� �����������
*/
void main()
{
	LinearModel linMod(5, 50);
	NonlinearModel nonlinMod(5, 100);

	linMod.PrintModel(12);
	cout << endl;
	nonlinMod.PrintModel(10);
	cout << endl;
	system("PAUSE");
}