/**
@mainpage  Seven-segment display
@file
@author Alyona Sipchuk
@date 17.12.15
*/
#include "i7188.h"

int main()
{
	while(1) 
	{
		Show5DigitLedSeg(1, 55);	
		Show5DigitLedSeg(2, 79);	
		Show5DigitLedSeg(3, 14);	
		Show5DigitLedSeg(4, 14);	
		Show5DigitLedSeg(5, 126);	
		DelayMs(1000);
		Show5DigitLedSeg(1, 79);
		Show5DigitLedSeg(2, 14);
		Show5DigitLedSeg(3, 14);
		Show5DigitLedSeg(4, 126);
		Show5DigitLedSeg(5, 0);    
		DelayMs(1000);
		Show5DigitLedSeg(1, 14);
		Show5DigitLedSeg(2, 14);
		Show5DigitLedSeg(3, 126);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 119);    
		DelayMs(1000);
		Show5DigitLedSeg(1, 14);	
		Show5DigitLedSeg(2, 126);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 119);
		Show5DigitLedSeg(5, 14);	   
		DelayMs(1000);
		Show5DigitLedSeg(1, 126);	
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 119);
		Show5DigitLedSeg(4, 14);
		Show5DigitLedSeg(5, 79);  
		DelayMs(1000);
		Show5DigitLedSeg(1, 0);	
		Show5DigitLedSeg(2, 119);
		Show5DigitLedSeg(3, 14);
		Show5DigitLedSeg(4, 79);
		Show5DigitLedSeg(5, 21); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 119);	
		Show5DigitLedSeg(2, 14);
		Show5DigitLedSeg(3, 79);
		Show5DigitLedSeg(4, 21);
		Show5DigitLedSeg(5, 119); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 14);	
		Show5DigitLedSeg(2, 79);
		Show5DigitLedSeg(3, 21);
		Show5DigitLedSeg(4, 119);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
		Show5DigitLedSeg(1, 79);	
		Show5DigitLedSeg(2, 21);
		Show5DigitLedSeg(3, 119);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
		Show5DigitLedSeg(1, 21);	
		Show5DigitLedSeg(2, 119);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
		Show5DigitLedSeg(1, 119);	
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0);
		DelayMs(1000);
		Show5DigitLedSeg(1, 0);	
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0);
	}
}