/*! \mainpage Project "Hello world!"
* ���������, �����������  ����� � ������� ������ "Hello world!".
*\file helloworld.cpp
*\brief �������� ������� ������� ������� � ������� ������� "Hello world!".
*/
#include <iostream>
/*!
\brief ������� �� ������� ����������� "Hello world!".
\param argc
\return 0-�������� ����������
*/
#include <string>
using namespace std;

int main()//! ������� main() - �������, ������� ���������� ���������� ����� ������� ���������. � ��� ���������������� ������.
{
	/*!
	���������� ������ ��������� �� ��������:
	\code
	string str = "Hello world!";
	\endcode
	*/
	string str = "Hello world!";
	/**
	����� ������ �������� �� ����� �������:
	\code
	cout � str � endl;
	\endcode
	*/
	cout << str << endl;
	system("pause");
	return 0;
}