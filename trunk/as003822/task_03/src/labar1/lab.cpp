/*! \mainpage Project "lab rab 1"
* ���������, ������������ ������ ����������. 
*����������� ������������ � ��� ������(���� �� ��� �����������).<br>
* \n <b> ������ �������� ������: </b>
*\image html Linear.png
*\details ����������� �������������� �� ������� �������� �������� �.�. ��� ��������� ������� ���������� �����������.
*<br> <b> ������ ���������� ������: </b>
*\image html NonLinear.png
*\details ����������� �������������� �� ������� �������� ���������� �.�. ������������ ��������, ������� ���������� �����������.
* \file lab.cpp
* \brief �������� ���� � ��������� ������� AbstactClass, LinearSchedule, NonLinearSchedule.
*/
#include <iostream>//!������������ ���� � ��������, ��������� � ����������� ��� ����������� �����-������.
#include <math.h>//!���������� math.h ���������� ����� ������� ��� ���������� ����� �������������� �������� � ��������������.

using namespace std;
/*! ����������� ����� AbstactClass.
\brief ����������� ������������ �����,�� ������� ������� ��������� ��������.
*/
class AbstactClass {
public:
	float Temperature[20];
	float AmountOfHeat = 5;
protected:
	//! ����������� �����, ������� ������� ���������� � �������� �����������.
	virtual void Temp() = 0;
};

/*!����� ��� ���������� �������� ������, �������������� �� ������ AbstactClass.
\brief ��������� ����� �� ��������(������������) ��� ���������� �������� ������.
*/

class LinearSchedule : public AbstactClass
{
public:
	//!�����, � ������� �������� ���������� ���������� �����������.
	void Temp()
	{
		Temperature[0] = 0;
		cout << "������� ��������� ���������\n\n";
		cout.width(11);
		cout << "�����������" << "  |  " << "T\n\n";
		for (int i = 0; i <= 20; i++)
			{

				/**
				������ ��������� ���������:
				\code
				Temperature[i + 1] = 0.988*Temperature[i] + 0.232*AmountOfHeat;
				\endcode
				*/

				Temperature[i + 1] = 0.988*Temperature[i] + 0.232*AmountOfHeat;
				cout.width(11);
				cout << Temperature[i] << "  |  " << i << endl;
			}
	}
	~LinearSchedule()///@brief ���������� ������
	{}
};

/*!����� ��� ���������� ���������� ������, �������������� �� ������ AbstactClass.
\brief ��������� ����� �� ��������(������������) ��� ���������� ���������� ������.
*/

class NonLinearSchedule : public AbstactClass
{
public:
	//!�����, � ������� �������� ���������� ���������� �����������.
	void Temp()
	{
		Temperature[0] = 0;
		Temperature[1] = 0;
		cout << "\n������� ����������� ���������\n\n";
		cout.width(11);
		cout << "�����������" << "  |  " << "T\n\n";
		for (int i = 1; i <= 20; i++)
			{
				/**
				������ ����������� ���������:
				\code
				Temperature[i + 1] = 0.9*Temperature[i] - 0.001*Temperature[i - 1] * Temperature[i - 1] + AmountOfHeat + sin(AmountOfHeat);
				\endcode
				*/

				Temperature[i + 1] = 0.9*Temperature[i] - 0.001*Temperature[i - 1] * Temperature[i - 1] + AmountOfHeat + sin(AmountOfHeat);
				cout.width(11);
				cout << Temperature[i] << "  |  " << i << endl;
			}
	}
	~NonLinearSchedule()///@brief ���������� ������
	{}
};
//!�������, � ������� ���������� �������� �������� ������, � ���������� ������� ��� ����.
int main()
{
	setlocale(0, "");
	LinearSchedule a;
	NonLinearSchedule b;
	a.Temp();
	b.Temp();
	system("pause");
	return 0;
};


