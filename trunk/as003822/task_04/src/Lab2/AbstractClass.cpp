/*! \file AbstractClass.cpp
* \brief ���� � �������� �����, ���������� � ���� ����������� ������� PidRegular(),����������� ���������� q0, q1,q2, e3, e2, e1, u, � ����� �����������  � ���������� ������ AbstractClass.
*/
#include "AbstractClass.h"

/*!�������, ����������� ������ ��� - ����������.
\code
�������� ������ ���-����������:
q0 = k*(1 + (Td / T0));
q1 = -k *(1 + 2 * (Td / T0) - (T0 / T));
q2 = k * (Td / T0);
e3 = e2;
e2 = e1;
e1 = w - Temperature;
u += (q0 * e1 + q1 * e2 + q2 * e3);
\endcode
*/

void AbstractClass::PidRegular()
{
	q0 = k*(1 + (Td / T0));
	q1 = -k *(1 + 2 * (Td / T0) - (T0 / T));
	q2 = k * (Td / T0);
	e3 = e2;
	e2 = e1;
	e1 = w - Temperature;
	u += (q0 * e1 + q1 * e2 + q2 * e3);
}
//! ����������� �������� ������������ ������ AbstractClass.
AbstractClass::AbstractClass() {}
//! ���������� �������� ������������ ������ AbstractClass.
AbstractClass::~AbstractClass() {}