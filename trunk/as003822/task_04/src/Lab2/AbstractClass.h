/*! \file AbstractClass.h
* \brief ������������ ����, ���������� � ���� ��������� ������� PidRegular() � Temp(), �������� ������ AbstractClass, ����������� ���������� ����������.
*/
#ifndef _ABSTRACTCLASS_H
#define _ABSTRACTCLASS_H
/*!����� AbstractClass.
\brief ����������� ������������ �����.
*/
class AbstractClass
{
public:
	///�����������, ���������� �������� ����������
	double q0;
	double q1;
	double q2;
	///����������, ���������� ���������� �������� ���������� Temperature(t) �� ��������� �������� w(t).
	double e1 = 0, e2 = 0, e3 = 0;
	///����������, ���������� ����������� �����������.
	double u = 0;
	///����������, ���������� ��������(������� ��������) ��������.
	double w = 50;
	///����������, ���������� �������� ����������.
	double Temperature = 0;
	///����������, ����������  ��� �� �������.
	double T0 = 0.6;
	///����������, ���������� ����������� ��������.
	double k = 0.6;
	///����������, ���������� ���������� �����������������.
	double Td = 0.05;
	///����������, ���������� ���������� ��������������.
	double T = 0.6;
	AbstractClass();
	~AbstractClass();
	void AbstractClass::PidRegular();
protected:
	//!������ ����������� �������.
	virtual void Temp() = 0;
};

#endif
