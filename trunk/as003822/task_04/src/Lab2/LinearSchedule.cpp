/*! \file LinearSchedule.cpp
* \brief ���� � �������� �����, ���������� � ���� ����������� ������� Temp() ��� ������ LinearSchedule, � ����� ������������ � ����������� ������ LinearSchedule.
*/
#include "LinearSchedule.h"
#include "AbstractClass.h"
//!�������, ������������ ��� ��������� �������� ������.
void LinearSchedule::Temp()
{
	std::cout << "������� ��������� ����������\n\n";
	std::cout.width(11);
	std::cout << "�����������" << "  |  " << "����������� �����������\n\n";
	double  Temperature_1 = 0;
	for (int i = 0; i <= 25; i++)
	{
		PidRegular();

		/**
		\code
		������ ��������� ���������:
		Temperature_1 = 0.988*Temperature + 0.232*u;
		\endcode
		*/

		Temperature_1 = 0.988*Temperature + 0.232*u;
		Temperature = Temperature_1;
		std::cout.width(11);
		std::cout << Temperature_1 << "  |  " << u << std::endl;
	}
}
//! ����������� ������ LinearSchedule.
LinearSchedule::LinearSchedule() {}
//! ���������� ������ LinearSchedule.
LinearSchedule::~LinearSchedule() {}
