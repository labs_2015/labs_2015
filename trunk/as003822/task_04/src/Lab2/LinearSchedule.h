/*! \file LinearSchedule.h
* \brief ������������ ����, ������� �������� � ����  �������� ������ LinearSchedule, ��������� ������� Temp(), ����� �������� ������������ � �����������.
*/
#ifndef _LINEARSCHEDULE_H
#define _LINEARSCHEDULE_H
#include <math.h>
#include <iostream>
#include "AbstractClass.h"

/*!����� LinearSchedule, ��� ���������� �������� ������.
\brief �������������� ����� �� ������������ ������.
*/
class LinearSchedule : public AbstractClass
{
public:
	LinearSchedule();
	~LinearSchedule();
	void LinearSchedule::Temp();
};
#endif 