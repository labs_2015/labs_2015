/*! \file NonLinearSchedule.cpp
* \brief ���� � �������� �����, ���������� � ���� ����������� ������� Temp() ��� ������ NonLinearSchedule, � ����� ������������ � ����������� ������ NonLinearShedule.
*/
#include "NonLinearSchedule.h"
#include "AbstractClass.h"
//!�������, ������������ ��� ��������� ���������� ������.
void NonLinearSchedule::Temp()
{
	double Temperature_2 = 0, Temperature_1 = 0;
	std::cout << "\n������� ����������� ���������\n\n";
	std::cout.width(11);
	std::cout << "�����������" << "  |  " << "����������� �����������\n\n";
	for (int i = 1; i <= 25; i++)
	{
		PidRegular();

		/**
		\code
		������ ����������� ���������:
		Temperature_2 = 0.9*Temperature_1 - 0.001*Temperature * Temperature + u + sin(u);
		\endcode
		*/

		Temperature_2 = 0.9*Temperature_1 - 0.001*Temperature * Temperature + u + sin(u);
		Temperature_1 = Temperature_2;
		Temperature = Temperature_1;
		std::cout.width(11);
		std::cout << Temperature_2 << "  |  " << u << std::endl;
	}
}
//! ����������� ������ NonLinearSchedule.
NonLinearSchedule::NonLinearSchedule() {}
//! ���������� ������ NonLinearSchedule.
NonLinearSchedule::~NonLinearSchedule() {}