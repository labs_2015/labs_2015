/*! \file NonLinearSchedule.h
* \brief ������������ ����, ���������� � ���� ��������� ������� Temp(), �������� ������ NonLinearSchedule, � ����� ������������ � �����������.
*/
#ifndef _NONLINEARSCHEDULE_H
#define _NONLINEARSCHEDULE_H
#include <math.h>
#include <iostream>
#include "AbstractClass.h"
/*!����� NonLinearSchedule, ��� ���������� ���������� ������.
\brief �������������� ����� �� ������������ ������.
*/
class NonLinearSchedule : public AbstractClass
{
public:
	double Temperature_2 = 0, Temperature_1 = 0;
	NonLinearSchedule();
	~NonLinearSchedule();
	void NonLinearSchedule::Temp();
};
#endif

