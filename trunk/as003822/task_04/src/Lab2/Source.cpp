/*! @mainpage Project "Lab rab 2"
* @author ��������� ������
* \details �� C++  ����������� ���������, ������������ ������������� ���� ���-���������.  � �������� ������� ���������� ������������ �������������� ������, ���������� � ���������� ������. ������������ ���, � ��������� ������ ���� �� ����� 3-� ������� (+������������).
* \n <b> <center>������ �������� ������:</center> </b>
*  \image html Linear.png
*<br> <b> <center>������ ���������� ������:</center> </b>
*  \image html NonLinear.png
* @file source.cpp
* \brief ����, � ������� ���������� �������� ������� AbstactClass, LinearSchedule, NonLinearSchedule � ������� Temp().
*/
#include "AbstractClass.h"
#include "LinearSchedule.h"
#include "NonLinearSchedule.h"		
#include <iostream>//!������������ ���� � ��������, ��������� � ����������� ��� ����������� �����-������.
using namespace std;
//!�������, � ������� ���������� �������� �������� ������, � ���������� ������� ��� ����.
int main()
{
	setlocale(0, "");
	LinearSchedule a;
	a.Temp();
	NonLinearSchedule b;
	b.Temp();
	system("pause");
	return 0;
}