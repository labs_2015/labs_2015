/**
@mainpage Project Project "Lab rab 3"
* ���������, �����������  ����� �� �������������� ��������� ����������� ������ "HELLO WOrLd VLAd.".
@file HELLO.C
* \brief ����, ���������� �������� ���, ������� ��������� ����� �� �������������� ����������.
@author Vladislav Fieshko
@date 16.12.15
*/
#include"i7188.h"
/**
@brief ���������� ����� ����� � ���������.
*/
void main()
{
	while(1)
	{
		Show5DigitLedSeg(1,55);   ///H
		Show5DigitLedSeg(2,79);   ///E
		Show5DigitLedSeg(3,14);   ///L
		Show5DigitLedSeg(4,14);   ///L
		Show5DigitLedSeg(5,126);  ///O
		DelayMs(1000);
		Show5DigitLedSeg(1,79);   ///E
		Show5DigitLedSeg(2,14);   ///L
		Show5DigitLedSeg(3,14);   ///L
		Show5DigitLedSeg(4,126);  ///O
		Show5DigitLedSeg(5,0);    ///_
		DelayMs(1000);
		Show5DigitLedSeg(1,14);   ///L
		Show5DigitLedSeg(2,14);   ///L
		Show5DigitLedSeg(3,126);  ///O
		Show5DigitLedSeg(4,0);    ///_
		Show5DigitLedSeg(5,30);   ///W1
		DelayMs(1000);
		Show5DigitLedSeg(1,14);   ///L
		Show5DigitLedSeg(2,126);  ///O
		Show5DigitLedSeg(3,0);    ///_
		Show5DigitLedSeg(4,30);   ///W1
		Show5DigitLedSeg(5,60);   ///W2
		DelayMs(1000);
		Show5DigitLedSeg(1,126);  ///0
		Show5DigitLedSeg(2,0);    ///_
		Show5DigitLedSeg(3,30);   ///W1
		Show5DigitLedSeg(4,60);   ///W2
		Show5DigitLedSeg(5,126);  ///O
		DelayMs(1000);
		Show5DigitLedSeg(1,0);    ///_
		Show5DigitLedSeg(2,30);   ///W1
		Show5DigitLedSeg(3,60);   ///W2
		Show5DigitLedSeg(4,126);  ///O
		Show5DigitLedSeg(5,70);   ///r
		DelayMs(1000);
		Show5DigitLedSeg(1,30);   ///W1
		Show5DigitLedSeg(2,60);   ///W2
		Show5DigitLedSeg(3,126);  ///O
		Show5DigitLedSeg(4,70);   ///r
		Show5DigitLedSeg(5,14);   ///L
		DelayMs(1000);
		Show5DigitLedSeg(1,60);   ///W2
		Show5DigitLedSeg(2,126);  ///0
		Show5DigitLedSeg(3,70);   ///r
		Show5DigitLedSeg(4,14);   ///L
		Show5DigitLedSeg(5,61);   ///d
		DelayMs(1000);
		Show5DigitLedSeg(1, 126); ///o      
		Show5DigitLedSeg(2, 70);  ///r      
		Show5DigitLedSeg(3, 14);  ///L      
		Show5DigitLedSeg(4, 61);  ///d     
		Show5DigitLedSeg(5, 0);   ///_      
		DelayMs(1000);
		Show5DigitLedSeg(1, 70);  ///r  
		Show5DigitLedSeg(2, 14);  ///L     
		Show5DigitLedSeg(3, 61);  ///d      
		Show5DigitLedSeg(4, 0);   ///_         
		Show5DigitLedSeg(5, 62);  ///V    
		DelayMs(1000);
		Show5DigitLedSeg(1,14);   ///L
		Show5DigitLedSeg(2,61);   ///d
		Show5DigitLedSeg(3,0);    ///_
		Show5DigitLedSeg(4,62);   ///V
		Show5DigitLedSeg(5,14);   ///L
		DelayMs(1000);
		Show5DigitLedSeg(1, 61);  ///d      
		Show5DigitLedSeg(2, 0);   ///_      
		Show5DigitLedSeg(3, 62);  ///V     
		Show5DigitLedSeg(4, 14);  ///L    
		Show5DigitLedSeg(5, 119); ///A      
		DelayMs(1000);
		Show5DigitLedSeg(1, 0);   ///_  
		Show5DigitLedSeg(2, 62);  ///V     
		Show5DigitLedSeg(3, 14);  ///L      
		Show5DigitLedSeg(4, 119); ///A        
		Show5DigitLedSeg(5, 189); ///d.    
		DelayMs(1000);

	}
}