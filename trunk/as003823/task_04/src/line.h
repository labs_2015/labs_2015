
/** \file
\brief �������� ����������� ������ line
*/
#pragma once
#include "stdafx.h"
#include "ControlObject.h"

/// ����������� �������� ������ ������� ����������. �������� ����������� ������ ControlObject.
class line : public ControlObject
{
public:
	/// ��������� ���������� ������������� �������� �������
	double modeling(double Y, double U);

	line() : ControlObject() {};

	~line() {}

};
