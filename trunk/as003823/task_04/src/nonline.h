
/** \file
\brief  �������� ����������� ������ nonline
*/
#include "stdafx.h"
#include "ControlObject.h"
#pragma once

/// ����������� ���������� ������ ������� ����������. �������� ����������� ������ ControlObject.
class NonLine : public ControlObject
{
protected:
	double old_U;
	double old_Y;
public:
	/// ��������� ���������� ������������� ���������� �������
	double modeling(double Y, double U);

	NonLine();

	~NonLine() {}
};
