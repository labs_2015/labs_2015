/**
@mainpage Project Project "Lr 3"
* ���������, �����������  ����� �� �������������� ��������� ����������� ������ "HELLO WOrLd dIH".
@file HELLO.C
* \brief ����, ���������� �������� ���, ������� ��������� ����� �� �������������� ����������.
@author Dmitry Hobotenko
@date 16.12.15
*/
#include"i7188.h"
/**
@brief ���������� ����� ����� � ���������.
*/
void main()
{
	while(1)
	{
		Show5DigitLedSeg(1,55);   ///H
		Show5DigitLedSeg(2,79);   ///E
		Show5DigitLedSeg(3,14);   ///L
		Show5DigitLedSeg(4,14);   ///L
		Show5DigitLedSeg(5,126);  ///O
		DelayMs(1000);
		Show5DigitLedSeg(1,79);   ///E
		Show5DigitLedSeg(2,14);   ///L
		Show5DigitLedSeg(3,14);   ///L
		Show5DigitLedSeg(4,126);  ///O
		Show5DigitLedSeg(5,0);    ///_
		DelayMs(1000);
		Show5DigitLedSeg(1,14);   ///L
		Show5DigitLedSeg(2,14);   ///L
		Show5DigitLedSeg(3,126);  ///O
		Show5DigitLedSeg(4,0);    ///_
		Show5DigitLedSeg(5,60);   ///W1
		DelayMs(1000);
		Show5DigitLedSeg(1,14);   ///L
		Show5DigitLedSeg(2,126);  ///O
		Show5DigitLedSeg(3,0);    ///_
		Show5DigitLedSeg(4,60);   ///W1
		Show5DigitLedSeg(5,60);   ///W2
		DelayMs(1000);
		Show5DigitLedSeg(1,126);  ///0
		Show5DigitLedSeg(2,0);    ///_
		Show5DigitLedSeg(3,60);   ///W1
		Show5DigitLedSeg(4,60);   ///W2
		Show5DigitLedSeg(5,126);  ///O
		DelayMs(1000);
		Show5DigitLedSeg(1,0);    ///_
		Show5DigitLedSeg(2,60);   ///W1
		Show5DigitLedSeg(3,60);   ///W2
		Show5DigitLedSeg(4,126);  ///O
		Show5DigitLedSeg(5,70);   ///r
		DelayMs(1000);
		Show5DigitLedSeg(1,60);   ///W1
		Show5DigitLedSeg(2,60);   ///W2
		Show5DigitLedSeg(3,126);  ///O
		Show5DigitLedSeg(4,70);   ///r
		Show5DigitLedSeg(5,14);   ///L
		DelayMs(1000);
		Show5DigitLedSeg(1,60);   ///W2
		Show5DigitLedSeg(2,126);  ///0
		Show5DigitLedSeg(3,70);   ///r
		Show5DigitLedSeg(4,14);   ///L
		Show5DigitLedSeg(5,61);   ///d
		DelayMs(1000);
		Show5DigitLedSeg(1, 126); ///o      
		Show5DigitLedSeg(2, 70);  ///r      
		Show5DigitLedSeg(3, 14);  ///L      
		Show5DigitLedSeg(4, 61);  ///d     
		Show5DigitLedSeg(5, 0);   ///_      
		DelayMs(1000);
		Show5DigitLedSeg(1, 70);  ///r  
		Show5DigitLedSeg(2, 14);  ///L     
		Show5DigitLedSeg(3, 61);  ///d      
		Show5DigitLedSeg(4, 0);   ///_         
		Show5DigitLedSeg(5, 61);  ///d    
		DelayMs(1000);
		Show5DigitLedSeg(1,48);   ///I
		Show5DigitLedSeg(2,55);   ///H
		Show5DigitLedSeg(3, 128);    ///.
		Show5DigitLedSeg(4,0);    ///_
		Show5DigitLedSeg(5,61);   ///d
		DelayMs(1000);
		Show5DigitLedSeg(1, 48);   ///I
		Show5DigitLedSeg(2, 55);  ///H   
		Show5DigitLedSeg(3, 128);    ///.
		Show5DigitLedSeg(4, 0);   ///_   
		Show5DigitLedSeg(5, 0);   ///_           
		DelayMs(1000);
		

	}
}