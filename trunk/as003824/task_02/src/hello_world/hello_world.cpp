/** \file
\brief �������� ��������� Hello_world.

\mainpage 
"������������ � ��������� hello world."
*/

#include "stdafx.h"
#include<iostream>
#include<clocale>

using namespace std;

/**
\brief ���������, ��������� �� ������ ������� "Hello, world".
\return 0 - ��������� ������� ���������.
!0 - ��������� ����������� � �������.
*/

int main(){
	setlocale(0, "");
	cout << "����i����� ����" << endl;
	getchar();
	return 0;
}
