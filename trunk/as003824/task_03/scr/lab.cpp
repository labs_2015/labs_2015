#include"lab.h"

class ControlObject
{
protected:
	const int size = 20;
	double *Temp = new double[size];
public:
	virtual void modeling(double, double) = 0;
	/// ���������� �� ������ ���������� ������������
	void show()
	{
		for (int i = 0; i<size; i++)
		{
			cout << setw(3) << i << ":  " << Temp[i] << endl;
		}
	}
	ControlObject()
	{
		for (int i = 0; i<size; i++)
			Temp[i] = 0;
	}

};
/// ��������� �������� ������ ������� ����������. �������� ����������� ������ ControlObject.
class line : public ControlObject
{
public:
	/// ���������� �������� ������
	void modeling(double StartTemp, double ChangeTemp)
	{
		for (int i = 0; i<size; i++)
		{
			Temp[i] = StartTemp;
			StartTemp = 0.988*StartTemp + 0.232*ChangeTemp;
		}

	}

	line() : ControlObject() {};


	~line() { delete[] Temp; }

};
/// ��������� ���������� ������ ������� ����������. �������� ����������� ������ ControlObject.
class NonLine : public ControlObject
{
public:
    /// ���������� ���������� ������
	void modeling(double StartTemp, double ChangeTemp)
	{
		double OldTemp = StartTemp;

		for (int i = 0; i<size; i++)
		{
			Temp[i] = StartTemp;
			if (i>0)
				OldTemp = Temp[i - 1];
			StartTemp = 0.9*StartTemp - 0.001*OldTemp + ChangeTemp + sin(ChangeTemp);
		}

	}

	NonLine() : ControlObject() {};

	~NonLine() { delete[] Temp; }
};