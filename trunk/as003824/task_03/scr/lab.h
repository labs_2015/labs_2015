
class ControlObject
{
protected:
	const int size = 20;
	double *Temp = new double[size];
public:
	virtual void modeling(double, double) = 0;
	void show();
	ControlObject();
};

class line : public ControlObject
{
public:
	void modeling(double StartTemp, double ChangeTemp);
};

class NonLine : public ControlObject
{
public:
	void modeling(double StartTemp, double ChangeTemp);
};