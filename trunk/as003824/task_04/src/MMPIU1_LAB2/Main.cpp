/*! \mainpage Project "Lab rab 2"
* ���������, ������������� ���-���������.
* ����������� ������������� ���: ��� ������ (���� �� ������� �����������) � ������������.
*  \image html Graphic1.JPG
* * <b><center> ������� 1. ��� �������� ������ </b></center>
*  \image html Graphic2.JPG
* <b><center>  ������� 2.��� ���������� ������ </b></center>
* \file main.cpp
* \brief ���� � �������� �����, � ������� �������������� �������� �������� ������� LinearModel � NonLinearModel, � ���������� ������ ShowResult() ��� ����.
*/

#include "Model.h"
#include "LinearModel.h""
#include "NonLinearModel.h"		
#include <iostream>
using namespace std;

//!�������, � ������� �������� �������� �������� ������� LinearModel � NonLinearMode, � ����� ���������� ������ ShowResult() ��� ����.
int main()
{
	setlocale(0, "");
	LinearModel *ObjectLinearModel= new LinearModel;
	ObjectLinearModel->ShowResult();
	NonLinearModel *ObjectNonLinearModel= new NonLinearModel;
	ObjectNonLinearModel->ShowResult();
	system("pause");
	return 0;
}

