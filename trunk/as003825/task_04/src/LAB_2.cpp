/*! \mainpage Project "������������ ������ �2(������)"
* \author ������ ������ �����������
* \details ������ �� task_04, ������ ���� ������� 5 �������, ���� ������ ���� ����� �����������, ������ ���� ������������ ������������.
* \n <b> <center>������ �������� ������:</center> </b>
*  \image html LInMod.png
*<br> <b> <center>������ ���������� ������:</center> </b>
*  \image html NonLInMod.png
*/
#include "stdafx.h"
#include <iostream>
#include <math.h>
#include "VarClass.h"
#include "Count.h"
#include "LInMod.h"
#include "ExLInMod.h"
#include "NonLInMod.h"
#include "ExNonLInMod.h"
using namespace std;
///@brief �������, ������� ������� ������� ������ � ��������� ��� ���� ������.
int main(int argc, char* argv[])
{
	setlocale(0, "");
	ExNonLInMod n;
	ExLInMod l;
	n.show();
	l.show();
	system("pause");
	return 0;
};
