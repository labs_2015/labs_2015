/*! \mainpage ������ "Hello world Sergey!"
@author Sergey Yakimuk
@date 18.12.15
* ���������,�����������  ����� �� �������������� ��������� ����������� ������ "Hello world Sergey!"
* \brief ����, ���������� �������� ���, ������� ��������� ����� �� �������������� ����������.
@file ser.C
*/

#include"I7188.h"
void main()
{
while(1)
{
Show5DigitLedSeg(5,55); //h
Show5DigitLedSeg(4,79); //e
Show5DigitLedSeg(3,14); //l 
Show5DigitLedSeg(2,14); //l
Show5DigitLedSeg(1,126);//o
DelayMs(1000);
Show5DigitLedSeg(5,79); //e
Show5DigitLedSeg(4,14); //l
Show5DigitLedSeg(3,14); //l
Show5DigitLedSeg(2,126);//0
Show5DigitLedSeg(1,0);  //_
DelayMs(1000);
Show5DigitLedSeg(5,14); //l
Show5DigitLedSeg(4,14); //l
Show5DigitLedSeg(3,126);//o
Show5DigitLedSeg(2,0);  //_
Show5DigitLedSeg(1,62); //w
DelayMs(1000);
Show5DigitLedSeg(5,14); //l
Show5DigitLedSeg(4,126);//o
Show5DigitLedSeg(3,0);  //_
Show5DigitLedSeg(2,62); //w
Show5DigitLedSeg(1,126);//o
DelayMs(1000);
Show5DigitLedSeg(5,126);//o
Show5DigitLedSeg(4,0);  //_
Show5DigitLedSeg(3,62); //w
Show5DigitLedSeg(2,126);//o
Show5DigitLedSeg(1,70); //r
DelayMs(1000);
Show5DigitLedSeg(5,0);  //_
Show5DigitLedSeg(4,62); //w
Show5DigitLedSeg(3,126);//o
Show5DigitLedSeg(2,70); //r
Show5DigitLedSeg(1,14); //l
DelayMs(1000);
Show5DigitLedSeg(5,62); //w
Show5DigitLedSeg(4,126);//o
Show5DigitLedSeg(3,70); //r
Show5DigitLedSeg(2,14); //l
Show5DigitLedSeg(1,61); //d
DelayMs(1000);
Show5DigitLedSeg(5,126); //o
Show5DigitLedSeg(4,70); //r
Show5DigitLedSeg(3,14); //l
Show5DigitLedSeg(2,61); //d
Show5DigitLedSeg(1,0); //_
DelayMs(1000);
Show5DigitLedSeg(5,70); //r
Show5DigitLedSeg(4,14); //l
Show5DigitLedSeg(3,61); //d
Show5DigitLedSeg(2,0); //_
Show5DigitLedSeg(1,91); //s
DelayMs(1000);
Show5DigitLedSeg(5,14); //l
Show5DigitLedSeg(4,61); //d
Show5DigitLedSeg(3,0); //_
Show5DigitLedSeg(2,91); //s
Show5DigitLedSeg(1,79); //e
DelayMs(1000);
Show5DigitLedSeg(5,61); //d
Show5DigitLedSeg(4,0); //_
Show5DigitLedSeg(3,91); //s
Show5DigitLedSeg(2,79); //e
Show5DigitLedSeg(1,70); //r
DelayMs(1000);
Show5DigitLedSeg(5,0); //_
Show5DigitLedSeg(4,91); //s
Show5DigitLedSeg(3,79); //e
Show5DigitLedSeg(2,70); //r
Show5DigitLedSeg(1,123); //g
DelayMs(1000);
Show5DigitLedSeg(5,91); //s
Show5DigitLedSeg(4,79); //e
Show5DigitLedSeg(3,70); //r
Show5DigitLedSeg(2,123); //g
Show5DigitLedSeg(1,79); //e
DelayMs(1000);
Show5DigitLedSeg(5,79); //e
Show5DigitLedSeg(4,70); //r
Show5DigitLedSeg(3,123); //g
Show5DigitLedSeg(2,79); //e
Show5DigitLedSeg(1,59); //y
DelayMs(1000);
}
}