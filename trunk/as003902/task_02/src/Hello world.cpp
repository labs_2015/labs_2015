/*!
	@file
	@brief Hello world prject for visual studio community edition 2015
	@author Berinchik Maksim AS - 39
*/
#include "stdafx.h"
#include <iostream>

/*!
	@brief main function of cpp programm
	
	Prints "Hello world!" string on the console
	Pauses the programm's work to show the result

*/
int main()
{
	std::cout << "Hello world!" << std::endl;

	system("pause");
    return 0;
}

