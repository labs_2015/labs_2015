/*!
	@file
	@brief Realisation of AbstractControllObject class

	Realisation of abstract class's functionality
*/

#include "stdafx.h"
#include "AbstractControllObject.h"


AbstractControllObject::AbstractControllObject(float CurrentT)
{
	CurrentTemperature = CurrentT;
}

AbstractControllObject::~AbstractControllObject(void)
{
}

float* AbstractControllObject::GetModel(float QuantityOFHeat, int time)
{
	float *Table = new float[time];

	Table[0] = CurrentTemperature;

	for (int i = 1; i < time; i++) {
		Table[i] = incrTemp(QuantityOFHeat);
	}

	return Table;
}

void AbstractControllObject::OneStep(float QuantityOfHeat)
{
	if (QuantityOfHeat < 0) QuantityOfHeat = 0;

	incrTemp(QuantityOfHeat);
}
