/*!
	@file
	@brief main function for lab_1
	@author Berinchik Maksim AS - 39

	In the result we have two graphics
	![results](Image.png)
*/
#include "stdafx.h"
#include <iostream>
#include "LinearControllObject.h"
#include "NonLinrControllObject.h"
#include <fstream>
#include <cassert>

using namespace std;

int main()
{
	ofstream fout;

	fout.open("Tablet.txt", ios_base::out);

	assert(fout.is_open());

	LinearControllObject A(25);
	NonLinrControllObject B(25);

	float* TabletA = A.GetModel(10, 21);
	float* TabletB = B.GetModel(10, 21);

	cout << "Linear model:" << endl;
	cout << "time | temp" << endl;
	for (int i = 0; i < 21; i++) {
		cout << i << " : ";
		cout << TabletA[i] << endl;

		fout << i << " : ";
		fout << TabletA[i] << endl;
	}

	fout << endl;

	cout << endl << "Non liner model:";
	cout << endl << "time | temp" << endl;
	for (int i = 0; i < 21; i++) {
		cout << i << " : ";
		cout << TabletB[i] << endl;

		fout << i  << " : ";
		fout << TabletB[i] << endl;
	}

	fout.close();
	system("pause");
    return 0;
}

