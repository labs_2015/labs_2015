/*!
	@file
	@brief Realisation of NonLinrControllObject class
*/

#include "stdafx.h"
#include "NonLinrControllObject.h"
#include <cmath>


NonLinrControllObject::NonLinrControllObject(float CurrentT) : AbstractControllObject(CurrentT)
{
	First = true;
	prevTemp = CurrentTemperature;
}

NonLinrControllObject::~NonLinrControllObject()
{
}

/*!
	Overloads incrTemp function and constructor
	@code
	float NonLinrControllObject::incrTemp(float QuantityOFHeat)
	{
		float curT, prevQ;

		if (!First) {
			prevQ = QuantityOFHeat;
		}
		else {
			prevTemp = 0.0f;
			prevQ = 0.0f;
		}

		curT = CurrentTemperature;

		CurrentTemperature = 0.9f * curT - 0.001f * (prevTemp * prevTemp) + QuantityOFHeat - sin(prevQ);

		prevTemp = curT;
		First = false;

		return CurrentTemperature;
	}
	@encode
*/
float NonLinrControllObject::incrTemp(float QuantityOFHeat)
{
	float curT, prevQ;

	if (!First) {
		prevQ = QuantityOFHeat;
	}
	else {
		prevTemp = 0.0f;
		prevQ = 0.0f;
	}

	curT = CurrentTemperature;

	CurrentTemperature = 0.9f * curT - 0.001f * (prevTemp * prevTemp) + QuantityOFHeat - sin(prevQ);

	prevTemp = curT;
	First = false;

	return CurrentTemperature;
}

