#pragma once
#include "AbstractControllObject.h"

/*!
	@brief class inherited from AbstractControllObject
	Provides a non linear model of our object
*/
class NonLinrControllObject : public AbstractControllObject
{
public:
	NonLinrControllObject(float CurrentT = 25.0);
	~NonLinrControllObject();

private:
	/*!
		Overloaded function of AbstractControllObject, that realises non linear model
	*/
	float incrTemp(float QuantityOFHeat);
	
	float prevTemp;///< Temperature of the previous iteration(required for nonlinear model)
	bool First;///< Supportive variable, whych helps to define if it is first iteration

};

