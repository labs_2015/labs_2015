#include "stdafx.h"
#include "AbstractControllObject.h"

AbstractControllObject::AbstractControllObject(float CurrentT)
{
	CurrentTemperature = CurrentT;
}

AbstractControllObject::~AbstractControllObject(void)
{
}

float* AbstractControllObject::GetModel(float QuantityOFHeat, int time)
{
	float *Table = new float[time];

	Table[0] = CurrentTemperature;

	for (int i = 1; i < time; i++) {
		Table[i] = incrTemp(QuantityOFHeat);
	}

	return Table;
}

void AbstractControllObject::OneStep(float QuantityOfHeat)
{
	incrTemp(QuantityOfHeat);
}

float AbstractControllObject::getTemp()
{
	return CurrentTemperature;
}
