#pragma once
/*!
	@brief Abstract class for controll object
	@author Berinchik Maksim AS - 39

	Defines default functinality for our math model class
*/
class AbstractControllObject
{
public:
	AbstractControllObject(float CurrentT = 25.0); 
	~AbstractControllObject();

	/*!
		Returns dependence Tempreture(time)
		@param[in] QuantityOFHeat defines the quantity of heat, given to our object
		@param[in] time number of iteratins
		@return Two dimensional array of time and temperature in this time
	*/
	virtual float* GetModel(float QuantityOFHeat, int time = 10);
	/*!
		Does one iteration with given quantity of heat
		@param[in] QuantityOFHeat defines the quantity of heat, given to our object
	*/
	virtual void OneStep(float QuantityOfHeat);
	/*!
		Function returns current temperature
	*/
	float getTemp();

protected:
	/*!
		Increases current temperature accordingly to the given quantity of heat
		@param[in] QuantityOFHeat defines the quantity of heat, given to our object
	*/
	virtual float incrTemp(float QuantityOFHeat) = 0;

	float CurrentTemperature;
};



