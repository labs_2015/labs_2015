/*!
	@file
	@brief main function for lab_2
	@author Berinchik Maksim AS - 39
*/
#include "stdafx.h"
#include "LinearControllObject.h"
#include "NonLinrControllObject.h"
#include "PID.h"
#include <iostream>
#include <Windows.h>
#include <ctime>

/*!
	@mainpage Result of the regulation
	@image html Result.png
*/

using namespace std;

/*!
	@brief main function of the program

	Creates 2 objects(Linear and Nonlinear) and sets their temperature to the random vakue from 5 to 20.
	Gets the required temperatures for both objects and regulates them to those temperatures using class PID.
*/
int main()
{
	srand(time(0));
	LinearControllObject LinearModel(rand() % 16 + 5);
	NonLinrControllObject NonlinearModel(rand() % 16 + 5);

	float requairedTemp = 25;
	float E, Q = 0, max = 0;
	int iterations = 1;

	float q1 = 0.58;
	float q2 = 0.08;
	float q3 = -0.6337;

	cout << " Current temperatur is:  " << LinearModel.getTemp() << endl << "Enter required temperature: ";
	cin >> requairedTemp;

	PID forLinear(&LinearModel, requairedTemp, "LinearResults.txt", q1, q2, q3);
	forLinear.Regulate();

	cout << "\n\nNonlinear model's current temperatur is:  " << NonlinearModel.getTemp() << endl << "Enter required temperature: ";
	cin >> requairedTemp;

	q1 = 0.28;
	q2 = 0.016;
	q3 = -0.25;

	PID forNonLinear(&NonlinearModel, requairedTemp, "NonLinearResults.txt", q1, q2, q3);
	forNonLinear.Regulate();

	cout << endl;

	system("pause");
	return 0;
}

