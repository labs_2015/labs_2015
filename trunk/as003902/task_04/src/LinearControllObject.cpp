
#include "stdafx.h"
#include "LinearControllObject.h"

LinearControllObject::LinearControllObject(float CurrentT) : AbstractControllObject(CurrentT)
{

}
/*!
	@brief Overloads incrTemp function

	Function realisation:
	@code
	float LinearControllObject::incrTemp(float QuantityOFHeat) {

		return CurrentTemperature = CurrentTemperature*0.988 + QuantityOFHeat*0.232;
	}
	@encode
*/

float LinearControllObject::incrTemp(float QuantityOFHeat) {

	return CurrentTemperature = CurrentTemperature*0.988F + QuantityOFHeat*0.232F;
}
