#pragma once
#include "AbstractControllObject.h"

/*!
	@brief class inherited from AbstractControllObject

	Provides a linear model of our object
*/
class LinearControllObject : public AbstractControllObject
{
public:
	LinearControllObject(float CurrentT = 25.0);

private:
	/*!
		@brief Overloaded function of AbstractControllObject, that realises linear model.
	*/
	float incrTemp(float QuantityOFHeat);

};

