
#include "stdafx.h"
#include "NonLinrControllObject.h"
#include <cmath>


NonLinrControllObject::NonLinrControllObject(float CurrentT) : AbstractControllObject(CurrentT)
{
	prevTemp = 0;
	prevQ = 0;
}

NonLinrControllObject::~NonLinrControllObject()
{
}
float NonLinrControllObject::incrTemp(float QuantityOFHeat)
{
	float curT = CurrentTemperature;

	CurrentTemperature = 0.9 * curT - 0.001 * (prevTemp * prevTemp) + QuantityOFHeat - sin(prevQ);

	prevTemp = curT;
	prevQ = QuantityOFHeat;

	return CurrentTemperature;
}
