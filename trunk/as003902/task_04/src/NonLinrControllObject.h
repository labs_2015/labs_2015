#pragma once
#include "AbstractControllObject.h"

/*!
	@brief class inherited from AbstractControllObject.

	Provides a non linear model of our object
*/
class NonLinrControllObject : public AbstractControllObject
{
public:
	NonLinrControllObject(float CurrentT = 25.0);
	~NonLinrControllObject();

private:
	/*!
		\brief Overloaded function of AbstractControllObject, that realises non linear model.

		\param QuantityOFHeat heat passed to our object
	*/
	float incrTemp(float QuantityOFHeat);
	
	float prevTemp;///< Temperature of the previous iteration(required for nonlinear model)
	float prevQ;///< Supportive variable, whych helps to define if it is first iteration

};

