#include "stdafx.h"
#include "PID.h"
#include <cmath>
#include <iostream>
#include <fstream>
#include <cassert>
#include <string>

PID::PID()
{
	Object = nullptr;
	E = 0;
	prev_E = 0;
	dobl_prev_E = 0;
	tripl_prev_E = 0;

	QuantityOfHeat = 0;

	number = 1;
}

PID::PID(AbstractControllObject * _Object, float _requiredTemp)
{
	Object = _Object;

	requiredTemp = _requiredTemp;

	max = Object->getTemp();

	E = requiredTemp - Object->getTemp();

	prev_E = 0;
	dobl_prev_E = 0;
	tripl_prev_E = 0;

	number = 1;

	iterations = 0;
}

PID::PID(AbstractControllObject * _Object, float _requiredTemp, std::string _fileName, float _q1, float _q2, float _q3) : PID(_Object, _requiredTemp)
{
	setCoeffs(_q1, _q2, _q3);
	fileName = _fileName;
}


PID::~PID()
{
}

float PID::countQ() {
	
	QuantityOfHeat = QuantityOfHeat + q1*E + q2*prev_E + q3*dobl_prev_E;
	Object->OneStep(QuantityOfHeat);

	tripl_prev_E = dobl_prev_E;
	dobl_prev_E = prev_E;
	prev_E = E;
	E = requiredTemp - Object->getTemp();

	return QuantityOfHeat;
}

float PID::average() {

	float avg = (E + prev_E + dobl_prev_E + tripl_prev_E) / number;

	if (number < 4) {
		++number;
	}

	return avg;
}

void PID::setCoeffs(float _q1, float _q2, float _q3)
{
	q1 = _q1;
	q2 = _q2;
	q3 = _q3;
}

void PID::setObject(AbstractControllObject * _Object)
{
	Object = _Object;
}

void PID::Regulate() {

	using namespace std;

	ofstream ResultFile(fileName, ios::out);

	assert(ResultFile.is_open());

	ResultFile << Object->getTemp() << endl;

	while (fabs(average()) >= 1.0E-3) {
		++iterations;

		countQ();
		cout << "Current temp: " << Object->getTemp() << endl;
		if (iterations % 10 == 0 || iterations <= 10) {
			ResultFile << Object->getTemp() << endl;
		}

		if (Object->getTemp() > max) max = Object->getTemp();

	}

	cout << "\nMaximum temperature = " << max << endl
			  << "Iterations:  " << iterations;

	ResultFile << "\nMaximum temperature = " << max << endl
		<< "Iterations:  " << iterations;
	ResultFile.close();
}