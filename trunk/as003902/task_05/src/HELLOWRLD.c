/*!
@file HELLOWORLD.c
@author Maksim Berinchik AS - 39
*/
#include "i7188.h"

/*!
@mainpage HELLO project by Berinchik M.
*/

/*!
@brief Program prints string HELLO WHAT AM I? on the controller's display

Prints string HELLO WHAT AM I? as an running line in the endless cicle.
int str[19] = {55, 79, 14, 14, 126, 0, 30, 56,
				   55, 119, 112, 64, 0, 119, 102,
				   112, 0, 6, 101}; //Text line as codes of each symbol.
*/
int main()
{
	int str[19] = {55, 79, 14, 14, 126, 0, 30, 56,
				   55, 119, 112, 64, 0, 119, 102,
				   112, 0, 6, 101};

	int lim = 5, j = 0, i;
	int size = 19;
	int offset = 0, position = 0;

	while (1)
	{
		for (i = offset, position = 1; position <= lim; i++, position++) {
			if (i >= size) {
				Show5DigitLedSeg(position, str[i - lim]);
			}
			else {
				Show5DigitLedSeg(position, str[i]);
			}
			cout << " ";
		}
		cout << endl;
		DelayMs(1000);

		offset++;
		if (offset >= size) {
			offset = 0;
		}
	}
}