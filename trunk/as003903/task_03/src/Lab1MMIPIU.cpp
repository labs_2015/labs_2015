///@file
///@Author Bogdan Alexei Viktorovith
///@25.10.2015
#include "stdafx.h"
#include <iostream>
#include <math.h>
using namespace std;

///@param input temperature
int u = 100;
///@brief abstract class
class abstraktnyi
{
public:
///@show method
	virtual void show() = 0;
protected:
	double y[10];
};
///@class lineinayaModel 
///@class  lineinayaModel derived from abstraktnyi
class lineinayaModel : public abstraktnyi
{
public:
	///@brief Method for the linear model
	void show()
	{
		cout << "\t�������� ������" << endl << endl;
		cout.width(10);
		cout << "y" << "\t" << "u" << "\t" << "t" << endl << endl;
		y[0] = 0;
		for (int t = 0; t < 11; t++)
		{
			///@brief the formula for calculating the notlinear model
			y[t + 1] = 0.988*y[t] + 0.232*u;
			cout.width(10);
			cout << y[t] << "\t" << u << "\t" << t << endl << endl;
		}
	}
};
///@class neLineinayaModel 
///@class  neLineinayaModel derived from abstraktnyi
class neLineinayaModel : public abstraktnyi
{
public:
	///@brief Method for the notlinear model
	void show()
	{
		cout << "\t���������� ������" << endl << endl;
		cout.width(10);
		cout << "y" << "\t" << "u" << "\t" << "t" << endl << endl;
		y[0] = 0;
		y[1] = 0;
		for (int t = 1; t < 11; t++)
		{
			///@brief the formula for calculating the notlinear model
			y[t + 1] = 0.9*y[t] - 0.001*y[t - 1] * y[t - 1] + u + sin(u);
			cout.width(10);
			cout << y[t] << "\t" << u << "\t" << t << endl << endl;
		}
	}
};
///@mainpage Model of Temperature 
///@image html linModel.png
///@image html neLinModel.png
///@brief We create objects and display them on the screen
int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(0, "Russian");
	lineinayaModel lin;
	neLineinayaModel nelin;
	lin.show();
	cout << endl << endl;
	nelin.show();
	system("pause");
	return 0;
};