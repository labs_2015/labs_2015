///@file
///@Author Bogdan Alexei Viktorovith
///@10.11.2015
#include "stdafx.h"
#include <iostream>
#include <math.h>
#include <fstream>
using namespace std;
///@class abstract class
class abstraktnyi
{
public:
	///@param parameters for PID-controller
	double T0 = 0.4, Td = 0.01, T = 0.85, K = 0.5, q0, q1, q2, e1 = 0, e2 = 0, e3 = 0, u = 0, w = 20, y = 0;
	///@func PID-controller
	void pid()
	{
		q0 = K*(1 + Td / T0);
		q1 = -K*(1 + 2 * (Td / T0) - T0 / T);
		q2 = K*(Td / T0);
		e3 = e2;
		e2 = e1;
		e1 = w - y;
		u += q0*e1 + q1*e2 + q2*e3;
	}
protected:
	///@show method
	virtual void show() = 0;
};
///@class lineinayaModel 
///@class  lineinayaModel derived from abstraktnyi
class lineinayaModel : public abstraktnyi
{
public:
	///@brief Method for the linear model
	void show()
	{
		cout << "\t�������� ������" << endl << endl;
		cout.width(10);
		cout << "y" << "\t" << "u" << " \t\t" << "t" << endl << endl;
		double Y = 0;
		cout.precision(3);
		ofstream fout("C:/mmipiu3.txt", ios::out);
		fout << "\t�������� ������" << endl << endl;
		fout << "y" << "\t" << "u" << " \t\t" << "t" << endl << endl;
		for (int t = 0; t < 150; t++)
		{
			pid();
			Y = 0.988*y + 0.232*u;
			y = Y;
			cout.width(10);
			cout << Y << "\t" << u << "\t\t" << t << endl << endl;
			fout << Y << "\t" << u << "\t\t" << t << endl << endl;
		}
		fout.close();
	}
};
///@class neLineinayaModel 
///@class  neLineinayaModel derived from abstraktnyi
class neLineinayaModel : public abstraktnyi
{
public:
	///@brief Method for the notlinear model
	void show()
	{
		cout << "\t���������� ������" << endl << endl;
		cout.width(10);
		cout << "y" << "\t" << "u" << " \t\t" << "t" << endl << endl;
		double Y = 0;
		double Y1 = 0;
		cout.precision(5);
		ofstream fout("C:/mmipiu2.txt", ios::out);
		fout << "\t���������� ������" << endl << endl;
		fout << "y" << "\t" << "u" << " \t\t" << "t" << endl << endl;
		for (int t = 1; t < 28; t++)
		{
			pid();
			Y = 0.9*Y1 - 0.001*y * y + u + sin(u);
			Y1 = Y;
			y = Y;
			cout.width(10);
			cout << Y1 << "\t" << u << " \t\t" << t << endl << endl;
			fout << Y1 << "\t" << u << "\t\t" << t << endl << endl;
		}
		fout.close();
	}
};
///@mainpage Model of Temperature 
///@image html PID.png
///@brief We create objects and display them on the screen
int _tmain(int argc, _TCHAR* argv[])
{
	setlocale(0, "Russian");
	lineinayaModel lin;
	neLineinayaModel nelin;
	lin.show();
	cout << endl << endl;
	nelin.show();
	system("pause");
	return 0;
};

