
///@mainpage  Hello World
///@file task_05.c
///@author Bogdan Alexei Viktorovitch
///@date 13.12.15

#include "i7188.h"

///@brief  The program will display on the seven-segment LED the string "HELLO WORLd.LESHA."
int main()
{
        while(1)
	{
            Show5DigitLedSeg(1,55);
            Show5DigitLedSeg(2,79);
            Show5DigitLedSeg(3,14);
            Show5DigitLedSeg(4,14);
            Show5DigitLedSeg(5,126);
            DelayMs(1000);
            Show5DigitLedSeg(1,30);
            Show5DigitLedSeg(2,60);
            Show5DigitLedSeg(3,126);
            Show5DigitLedSeg(4,118);
            Show5DigitLedSeg(5,14);
            DelayMs(1000);
            Show5DigitLedSeg(1,189);
            Show5DigitLedSeg(2,0);
            Show5DigitLedSeg(3,0);
            Show5DigitLedSeg(4,0);
            Show5DigitLedSeg(5,0);
            DelayMs(1000);
///@brief Show on the dispaly of the  controller "LESHA."
            Show5DigitLedSeg(1,14);
            Show5DigitLedSeg(2,79);
            Show5DigitLedSeg(3,91);
            Show5DigitLedSeg(4,55);
            Show5DigitLedSeg(5,247);
            DelayMs(1000);
	}
}
