//
///@file
///@author Maxim Boiko
///@date 06,10,2015
///@mainpage My First Lab
///@image html graph.png  

#include "Stdafx.h"
#include <math.h>
#include<iostream>

using namespace std;

///Parent class
class Model
{
protected:
	///\brief Returns the initial value of the output temperature of the object
	double y0 = 0;

public:
	/*!
	\brief function shows result of computing
	\param u

	This function builds table with results of each iteration
	*/
	virtual void showTemperature(int(*u)(int)) = 0;
};
/*!
\brief Inherited class - linear model

*/

class Model_linear :public Model
{
public:

	void showTemperature(int(*ut)(int))
	{
		cout.width(8);
		cout << "y" << "  |  " << "u" << "    |  " << "t" << endl;
		cout << "------------------------" << endl;
		cout.width(8);
		cout << y0 << "  |  " << ut(0) << "  |  " << "0" << endl;
		double y = y0;
		for (int i = 1; i <11; i++)
		{
			cout.width(8);
			///realization of the function of the linear model
			cout << (y = 0.988*y + 0.232*ut(100)) << "  |  " << ut(100) << "  |  " << i << endl;
		}
	}
};

/*!
\brief Inherited class - nonlinear model
*/
class Model_nonlinear :public Model
{
protected:
	double y1 = 0;
	
public:

	void showTemperature(int(*ut)(int))
	{
		cout.width(8);
		cout << "y" << "  |  " << "u" << "    |  " << "t" << endl;
		cout << "------------------------" << endl;
		cout.width(8);
		cout << y0 << "  |  " << ut(0) << "  |  " << "0" << endl;
		cout.width(8);
		double yLast = y0, yNext = y1;
		for (int i = 1; i <11; i++)
		{
			double y = yNext;
			cout.width(8);
			///realization of the function of the nonlinear model
			cout << (yNext = 0.9*yNext - 0.001*yLast*yLast + ut(100) + sin(ut(100))) << "  |  " << ut(100) << "  |  " << i << endl;
			yLast = y;
		}

	}
};

int u(int t)
{
	return t;
}

int main()
{
	
	Model_linear M1;
	Model_nonlinear M2;
	M1.showTemperature(&u);
	cout << endl << endl;
	M2.showTemperature(&u);
	system("pause");
}



