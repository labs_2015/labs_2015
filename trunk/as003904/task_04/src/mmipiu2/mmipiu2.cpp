///@file
///@author Maxim Boiko
///@date 06,10,2015
///@mainpage My Second Lab
///@image html graph.png  

#include "stdafx.h"
#include <math.h>
#include <iostream>

using namespace std;
///Parent class
class System
{
protected:
                ///\brief Returns the initial value of the output temperature of the object
	double y0 = 0;
                ///\brief Returns the initial value of the wanted temperature of the object
	double wt = 100;
                ///\brief Integration constant
     	double i_T = 0.9; 
                ///\brief Differentiation constant
	double d_T = 1.1; 
                ///\brief Transmission coefficient
	double kof = 0.1; 
                ///\brief Time interval
	double T0 = 1; 
public:
	double yLast = 0;
	double uLast = 80;
	double uPrev = 0;
	double d_u = 0;
	double eLast = 0;
	double ePrev = 0;
	double ePrevPrev = 0;
	int time = 1;
	double q0;
	double q1;
	double q2;
	virtual void showTemperature() = 0;
                /*!
	\brief Regulator of temperature
	

	This function maintain temperature
	*/
	void regulator()
	{
                                ///realization of regulator
		uPrev = uLast;
		q0 = kof * (1 + d_T / T0);
		q1 = kof * (1 + 2 * d_T / T0 - T0 / time);
		q2 = kof * d_T / T0;
		eLast = wt - uLast;
		d_u = q0 * eLast + q1 * ePrev + q2 * ePrevPrev;
		uLast = uPrev + d_u;
		ePrevPrev = ePrev;
		ePrev = eLast;
	}	
};
/*!
\brief Inherited class - linear model

*/
class linear_Sys : public System
{
	
public:
	void showTemperature()
	{
		cout << "System with linear model" << endl;
		cout << "Input temperature: " << uLast << endl;
		cout.width(8);
		cout << "y" << "  |  " << "temp(ut(t))" << "  |  " << "u" << "    |  " << "t" << endl;
		double y = y0;
		for (int time = 1; time < 11; time++)
		{
			cout.width(8);
			regulator();
			cout << (y = 0.988*y + 0.232*uLast) << "  |  ";
			cout.width(11);
			cout << uLast << "  |  " << wt << "  |  " << time << endl;
		}
	}
	
};

/*!
\brief Inherited class - nonlinear model

*/

class nonlinear_Sys : public System
{
public:
	void showTemperature()
	{
		cout << "System with linear model" << endl;
		cout << "Input temperature: " << uLast << endl;
		cout.width(8);
		cout << "y" << "  |  " << "temp(ut(t))" << "  |  " << "u" << "    |  " << "t" << endl;
		double yLast = y0, yNext = y0;
		for (int time = 1; time < 11; time++)
		{
			double y = yNext;
			cout.width(8);
			regulator();
			cout << (yNext = 0.9*yNext - 0.001*yLast*yLast + uLast + sin(uLast)) << "  |  ";
			cout.width(11);
		    cout<< uLast << "  |  " << wt << "  |  " << time << endl;
			yLast = y;
		}
	}
};

                /*!
	\brief Main function
	*/

int main()
{
	linear_Sys S1;
	S1.showTemperature();
	nonlinear_Sys S2;
	cout << endl << endl;
	S2.showTemperature();
	cout << endl << endl;
	system("pause");
	return 0;

}

