#include "stdafx.h"
#include "PID_REG.h"

#include <iostream>
PID_REG::PID_REG()
{
}

///@brief set parameters: step on the time (T0), const diff (Td), const integr (T), transfer coeff (K), expected temperature (wt)
void PID_REG::setParametrs(double T0, double TD, double T, double K, double wt)
{
	this->T0 = T0;
	this->Td = TD;
	this->T = T;
	this->K = K;
	this->wt = wt;
}
///@brief get step on the time
double PID_REG::getT0() {
	return T0;
}

///@brief get const diff
double PID_REG::getTd() {
	return Td;
}

///@brief const integr
double PID_REG::getT() {
	return T;
}

///@brief transfer coeff
double PID_REG::getK() {
	return K;
}

///@brief expected temperature
double PID_REG::getwt() {
	return wt;
}

///@brief calculate q0, q1, q2
void PID_REG::findq012()	{
		q0 = K * (1 + (Td / T0));
		q1 = -K *(1 + 2 *( Td / T0) - (T0 / T));
		q2 = K * (Td / T0);
	}

///@brief return deltaU
double PID_REG:: foundU(double y)
	{
		e3 = e2;
		e2 = e1;
		e1 = wt - y;
		deltaU += (q0 * e1 + q1 * e2 + q2 * e3);
		return deltaU;
	}



PID_REG::~PID_REG()
{
}
