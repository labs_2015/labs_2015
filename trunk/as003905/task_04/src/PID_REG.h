#pragma once
class PID_REG
{

public:
	PID_REG();
	~PID_REG();
	///@param T0 - step on the time
	///@param Td - const diff
	///@param K - transfer coeff
	///@param wt - expected temperature
	void setParametrs(double T0,double TD, double T, double K, double wt);
	double getT0();
	double getTd();
	double getT();
	double getK();
	double getwt();
	///@param y - input temperature
	double foundU(double y);
	void findq012();
private:
	double T0, Td, T, K, wt;
	double q0 = 0, q1 = 0, q2 = 0;
	double e1 = 0, e2 = 0, e3 = 0, deltaU = 0;
};

