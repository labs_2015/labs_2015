#pragma once
#include "first.h"
class notline : public first
{
public:
	notline();
	~notline();
	///@param i - time
	double getY_U(int i);
	///@param U - input heat
	void setY_U(double U);
	///@param y1 - input temperature
	void setY1(double y1);
	///@param y - input temperature
	///@param U - input heat
	double findY_U(double y, double U);
	void show();
private:
	double y1 = 0;
};
