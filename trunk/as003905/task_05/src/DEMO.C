///@file demo.c
///@author Valuevich
///@date 13.12.2015
#include<stdio.h>
#include<stdlib.h>
#include<dos.h>
#include"F:\TC\i7188\lib\i7188.h"

///@mainpage Hello World.
///@details project:  demo.prj  demo.c  i7188s.lib
///@details compiler is TC++ v1.01, model is Small

///@fn void show_Func()
///@brief Show on the dispaly of the  controller "HELLO WORLd."
void show_Func(){
Show5DigitLedSeg(1,55);
Show5DigitLedSeg(2,79);
Show5DigitLedSeg(3,14);
Show5DigitLedSeg(4,14);
Show5DigitLedSeg(5,126);
DelayMs(1000);
Show5DigitLedSeg(1,30);
Show5DigitLedSeg(2,60);
Show5DigitLedSeg(3,126);
Show5DigitLedSeg(4,118);
Show5DigitLedSeg(5,14);
DelayMs(1000);
Show5DigitLedSeg(1,189);
Show5DigitLedSeg(2,0);
Show5DigitLedSeg(3,0);
Show5DigitLedSeg(4,0);
Show5DigitLedSeg(5,0);
DelayMs(500);
}
///@brief  If launched on the MiniOS show "HELLO WORLd." on the dispaly of controller else show in the screen PC  "Hello world(screen)"
void main(){
int i;
if(Is7188()){
	while(1){
	  show_Func();
	}
}
else {
Print("Hello World (screen)");
return;
}
}
