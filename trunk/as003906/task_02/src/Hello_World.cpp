///@file
///@author Grigorenko
///@30,10,2015
#include "stdafx.h"
#include <iostream>
using namespace std;
///@brief We define an entry point in the program
int main()
{
	///@output message "Hello, world!"
	cout << "Hello, world!" << endl;
	system("pause");
	return 0;
}

