///@file
///@author Grigorenko
///@20.11.2015
#include "stdafx.h"
#include <iostream>
#include <math.h>
#include <fstream>
using namespace std;
///@class AbstactClass
class AbstactClass {
public:
	///@param parameters for PID-controller
	double T0 = 0.5, Td = 0.02, T = 0.5, k = 0.5, q0, q1, q2, e1 = 0, e2 = 0, e3 = 0, u = 0, w = 40, y = 0;
	///@func PID-controller
	void pid()
	{
		q0 = k*(1 + (Td / T0));
		q1 = -k *(1 + 2 * (Td / T0) - (T0 / T));
		q2 = k * (Td / T0);
		e3 = e2;
		e2 = e1;
		e1 = w - y;
		u += (q0 * e1 + q1 * e2 + q2 * e3);
	}
protected:
	virtual void show() = 0;
};
/**@class LineinayaModel
Inheriting a class from basic(abstract)
*/
class LineinayaModel : public AbstactClass
{
public:
	void show()
	{
		cout << "LinMod\n\n";
		cout.width(8);
		cout << "Y" << "  |  " << "U\n";
		double  x = 0;
		for (int i = 0; i <= 60; i++)
		{
			pid();
			x = 0.988*y + 0.232*u;
			y = x;
			cout.width(8);
			cout << x << "  |  " << u << endl;
		}
	}
};
/**@class NeLineinayaModel
Inheriting a class from basic(abstract)
*/
class NeLineinayaModel : public AbstactClass
{
public:
	void show()
	{
		double z = 0, x = 0;

		cout << "\nNeLinMod\n\n";
		cout.width(8);
		cout << "Y" << "  |  " << "U\n";
		for (int i = 1; i <= 20; i++)
		{
			pid();
			z = 0.9*x - 0.001*y * y + u + sin(u);
			x = z;
			y = x;
			cout.width(8);
			cout << z << "  |  " << u << endl;
		}
	}
};
/**@mainpage Model of Temperature 
@image html Pid.png
@brief We create objects and display them on the screen
*/
int main(int argc, char* argv[])
{
	LineinayaModel a;
	NeLineinayaModel b;
	a.show();
	b.show();
	system("pause");
	return 0;
};