// MMPiU.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
#include <iostream>
#include <math.h>
#include <fstream>
#include <cassert>


class Enter
{
public:
	Enter(float temp = 25.0)
	{
		default = temp;
	}

	virtual float* GetModel(float Grad, int count)
	{
		float *Table = new float[count];
		Table[0] = default;
		for (int i = 1; i < count; i++)
		{
			Table[i] = incrTemp(Grad);
		}
		return Table;
	}

public:
	virtual float incrTemp(float Grad) = 0;
	float default;
};

class LineModel : public Enter
{

public:
	LineModel(float temp1 = 25.0) : Enter(temp1) {

	}

private:
	float incrTemp(float Grad) {

		return default = default*0.988 + Grad*0.232;
	}
};

class NotLineModel : public Enter
{

public:
	NotLineModel::NotLineModel(float temp2 = 25.0) : Enter(temp2)
	{
		First = true;
		prevTemp = default;
	}

private:
	float incrTemp(float Grad)
	{
		float curT, prevQ;
		if (!First)
		{
			prevQ = Grad;
		}
		else
		{
			prevTemp = 0.0f;
			prevQ = 0.0f;
		}
		curT = default;
		default = 0.9f * curT - 0.001f * (prevTemp * prevTemp) + Grad - sin(prevQ);
		prevTemp = curT;
		First = false;
		return default;
	}
	float prevTemp;
	bool First;
};

int main()
{
	using namespace std;
	ofstream fout;
	fout.open("Grow.txt", ios_base::out);
	assert(fout.is_open());
	LineModel A(25);
	NotLineModel B(25);
	float* GrowA = A.GetModel(10, 21);
	float* GrowB = B.GetModel(10, 21);
	cout << "Linear model:" << endl;
	for (int i = 0; i < 21; i++)
	{
		cout << i << " : ";
		cout << GrowA[i] << endl;
		fout << i << " : ";
		fout << GrowA[i] << endl;
	}
	fout << endl;
	cout << endl << "NotLinear model:" << endl;
	for (int i = 0; i < 21; i++)
	{
		cout << i << " : ";
		cout << GrowB[i] << endl;
		fout << i << " : ";
		fout << GrowB[i] << endl;
	}
	fout.close();
	system("pause");
	return 0;
}
