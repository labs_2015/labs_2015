#pragma once
#include <iostream>
using namespace std;
class first
{
public:
	first();
	~first();
	double getY0();
	///@param y0 - initial input temperature
	void setY0(double y0);
	///@brief virtual method
	virtual void show() = 0;

protected:
	double y[10] = {};
	double y0 = 0;


};