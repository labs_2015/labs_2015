///@file mm2123.cpp
///@author Valuevich
///@date 01.11.2015

#include "stdafx.h"
#include "first.h"
#include "line.h"
#include "notline.h"
#include "PID_REG.h"
#include <fstream>

///@mainpage Result
///@image html result.png
///@brief creating the model of PID regulator
///@brief Dispaly output expected temperature and input heat, create 2 files (Line.txt and Not_line.txt) witch the calculations of PID regulator
int main()
{ 
	double u;
	double wt;
	cout << "Enter start temperature u(t): ";
	cin >> u;
	cout << "Enter expected temperature wt: ";
	cin >> wt;
	line q;
	notline w;
	q.setY0(0);
	q.setY_U(u);
	w.setY_U(u);
	PID_REG s;
	s.setParametrs(0.5,0.02, 0.85,0.5,wt);
	s.findq012();
	double y = 0;
	y=q.findY_U(0,0);
	u = s.foundU(y);
	int i = 1;
	cout.setf(std::ios_base::fixed, std::ios_base::floatfield);
	cout.precision(5);
	ofstream rep("Line.txt");
	ofstream rep1("No_line.txt");

	while (y!=wt) {
		rep << (i - 1)*0.5 << "\t"<< y << "\t" << u << endl;
		y = q.findY_U(y, u);
		u = s.foundU(y);
		i++;
		
	}
	
	if (y == wt)
		cout << "\nLINE: Finded expected temperature( " << wt << " ) when\ninput heat U = " << u << "\ntime is: " <<(double) i*0.5<< endl;
	rep.close();
	PID_REG se;
	se.setParametrs(0.5, 0.021, 0.85, 0.5, wt);
	se.findq012();
	y = w.findY_U(0, 0);
	u = se.foundU(y);
	i = 1;

	while (y!=wt) {
		rep1 << (i - 1)*0.5<<"\t" << y << "\t" << u <<endl;
		y = w.findY_U(y, u);
		u = se.foundU(y);
		i++;
	}
	
	if (y == wt)
		cout << "\nNOTLINE: Finded expected temperature( " << wt << " ) when\ninput heat U = " << u << "\ntime is: " << i*0.5 << endl;
	rep1.close();
	system("pause");
	return 0;
}
