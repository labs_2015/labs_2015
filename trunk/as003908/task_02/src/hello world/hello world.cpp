/**
@file
@brief Doxygen introduction: "Hello world!"
@author Gutnikov Vladislav Sergeevich
*/

#include "stdafx.h"
#include <iostream>
#include <string>

int _tmain(int argc, wchar_t** argv) {
	/**
		Print "Hello world!" message 
		using wide string into console output stream
	*/
	std::wcout << L"Hello world!" << std::endl;
	return 0;
}