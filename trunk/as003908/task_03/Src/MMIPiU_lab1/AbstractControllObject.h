#pragma once
#ifndef __ABSTRACT_CONTROLL_OBJECT_H__
#define __ABSTRACT_CONTROLL_OBJECT_H__
/**
	@brief Abstract class for controll object

	Defines default functinality for our math model class
*/


class AbstractControllObject
{
public:
	AbstractControllObject(float startTemperature = 25.0);
	/**
		recalcs temperature according quantity of heat
		Increases current temperature accordingly to the given quantity of heat
		@param[in] quantityOfHeat defines the quantity of heat, given to our object
		@return new temperature of the model
	*/
	inline virtual float recalcTemp(float quantityOfHeat) = 0;

protected:
	float currentTemperature;
};

#endif

