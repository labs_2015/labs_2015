#include "stdafx.h"
#include "LinearControllObject.h"


LinearControllObject::LinearControllObject(float CurrentT) : AbstractControllObject(CurrentT) {}

float LinearControllObject::recalcTemp(float QuantityOFHeat) {
	currentTemperature = currentTemperature * 0.988f + QuantityOFHeat * 0.232f;
	return currentTemperature;
}
