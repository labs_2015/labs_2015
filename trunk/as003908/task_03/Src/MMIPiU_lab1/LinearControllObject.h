#pragma once
#ifndef __LINEAR_CONTROLL_OBJECT_H__
#define __LINEAR_CONTROLL_OBJECT_H__

#include "AbstractControllObject.h"

/**
	@brief class inherited from AbstractControllObject
	Provides a linear model of our object
*/
class LinearControllObject : public AbstractControllObject {
public:
	LinearControllObject(float CurrentT = 20.0);
	float recalcTemp(float QuantityOFHeat);
};

#endif