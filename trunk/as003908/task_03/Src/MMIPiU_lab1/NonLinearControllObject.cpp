#include "stdafx.h"
#include "NonLinearControllObject.h"
#include <cmath>


NonLinearControllObject::NonLinearControllObject(float currentT) : AbstractControllObject(currentT) {
	prevTemp = 0;
}


float NonLinearControllObject::recalcTemp(float quantityOfHeat)
{
	float curT = currentTemperature;

	currentTemperature = 0.9f * currentTemperature - 0.001f * (prevTemp * prevTemp) + quantityOfHeat - sin(quantityOfHeat);
	prevTemp = curT;

	return currentTemperature;
}

