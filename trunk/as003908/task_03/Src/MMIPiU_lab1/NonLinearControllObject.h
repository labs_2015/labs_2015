#pragma once
#ifndef __NON_LINEAR_CONTROLL_OBJECT_H__
#define __NON_LINEAR_CONTROLL_OBJECT_H__

#include "AbstractControllObject.h"

/**
	@brief class inherited from AbstractControllObject
	Provides a non linear model of our object
*/
class NonLinearControllObject : public AbstractControllObject
{
public:
	NonLinearControllObject(float currentT = 20.0);

	float recalcTemp(float quantityOfHeat);

private:
	float prevTemp;///< Temperature of the previous iteration(required for nonlinear model)

};

#endif;