#include "stdafx.h"
#include "LinearControllObject.h"


LinearControllObject::LinearControllObject(float requiredT) : PIDController(requiredT) {}

float LinearControllObject::recalcTemp() {
	correct();
	currentTemperature = currentTemperature * 0.988f + correctingValue * 0.232f;
	return currentTemperature;
}
