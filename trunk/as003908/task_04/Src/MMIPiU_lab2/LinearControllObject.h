#pragma once
#ifndef __LINEAR_CONTROLL_OBJECT_H__
#define __LINEAR_CONTROLL_OBJECT_H__

#include "PIDController.h"

/**
	@brief class inherited from PIDController
	Provides a linear model of our object
*/
class LinearControllObject : public PIDController {
public:
	LinearControllObject(float requiredT = 40.0);
	float recalcTemp();
};

#endif