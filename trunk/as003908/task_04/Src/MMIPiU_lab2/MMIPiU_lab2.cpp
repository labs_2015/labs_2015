/**
	@file
	@brief lab2 entry point file
	@author Gutnikov Vladislav Sergeevich
*/

//@cond
#include "stdafx.h"
#include <iostream>
//@endcond

#include "LinearControllObject.h"
#include "NonLinearControllObject.h"

using namespace std;
//@cond
#define __DEBUG_MODE__ 1
//@endcond


int main() {

#define MODEL_RECALC_TIMES 20


	LinearControllObject A;
	NonLinearControllObject B(40.0f);


	cout << "Linear model:" << endl << "time\t|\ttemp" << endl;
	for (short i = 1; i <= MODEL_RECALC_TIMES; i++) cout << i << "\t|\t" << A.recalcTemp() << endl;

	cout << endl << "Non liner model:" << endl << "time\t|\ttemp" << endl;
	for (short i = 1; i <= MODEL_RECALC_TIMES; i++) cout << i << "\t|\t" << B.recalcTemp() << endl;

#if __DEBUG_MODE__ == 1
	system("pause");
#endif

    return 0;

#undef MODEL_RECALC_TIMES
}

