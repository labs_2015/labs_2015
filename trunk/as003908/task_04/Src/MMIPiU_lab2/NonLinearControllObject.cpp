#include "stdafx.h"
#include "NonLinearControllObject.h"
#include <cmath>


NonLinearControllObject::NonLinearControllObject(float requiredT) : PIDController(requiredT) {
	prevTemp = 0;
}


float NonLinearControllObject::recalcTemp() {
	correct();
	float curT = currentTemperature;

	currentTemperature = 0.9f * currentTemperature - 0.001f * (prevTemp * prevTemp) + correctingValue - sin(correctingValue);
	prevTemp = curT;

	return currentTemperature;
}

