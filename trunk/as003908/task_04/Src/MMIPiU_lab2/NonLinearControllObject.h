#pragma once
#ifndef __NON_LINEAR_CONTROLL_OBJECT_H__
#define __NON_LINEAR_CONTROLL_OBJECT_H__

#include "PIDController.h"

/**
	@brief class inherited from AbstractControllObject
	Provides a non linear model of our object
*/
class NonLinearControllObject : public PIDController {
public:
	NonLinearControllObject(float requiredT = 40.0);

	float recalcTemp();

private:
	float prevTemp;///< Temperature of the previous iteration(required for nonlinear model)

};

#endif;