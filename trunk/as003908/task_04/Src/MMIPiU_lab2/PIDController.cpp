#include "stdafx.h"
#include "PIDController.h"


PIDController::PIDController(float requiredT) {
	currentTemperature = 0;
	requiredTemperature = requiredT;
	correctingValue = 0;
}

void PIDController::correct() {
	float q0 = k*(1 + (Td / T0)),
		q1 = -k *(1 + 2 * (Td / T0) - (T0 / T)),
		q2 = k * (Td / T0);

	e2 = e1;
	e1 = e0;
	e0 = requiredTemperature - currentTemperature;

	correctingValue += (q0 * e0 + q1 * e1 + q2 * e2);
}