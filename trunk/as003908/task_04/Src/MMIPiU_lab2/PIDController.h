#pragma once
#ifndef __PID_CONTROLLER_H__
#define __PID_CONTROLLER_H__
/**
	@brief Abstract class for controll object
	Defines default functinality for our math model class
*/


class PIDController {
private:
	const float T0 = 0.5f, //* time step
	Td = 0.02f, //* differential const
	T = 0.5f, //* integral const
	k = 0.5f; //* const transmit coeff

	float e0 = 0, e1 = 0, e2 = 0; //* difference between current output value y(t) -- currentTemperature(t), and requiredValue w(t) -- requiredValue(t).

	float requiredTemperature;

public:
	PIDController(float requiredT);

	/**
		recalcs temperature
		@return new temperature of the model
	*/
	inline virtual float recalcTemp() = 0;

protected:
	//*	Coputes current correcting value for current temperature
	void correct(void);

	//* output temperature
	float currentTemperature;
	///����������� �����������
	float correctingValue;
};

#endif

