///@mainpage  Hello World on controller
///@file task_05.c

#include "i7188.h"

///@brief  The program will display "HELLO WORLd." on controller LED screen
int main() {
        while(1) {
            Show5DigitLedSeg(1,55);
            Show5DigitLedSeg(2,79);
            Show5DigitLedSeg(3,14);
            Show5DigitLedSeg(4,14);
            Show5DigitLedSeg(5,126);
            DelayMs(1000);
            Show5DigitLedSeg(1,30);
            Show5DigitLedSeg(2,60);
            Show5DigitLedSeg(3,126);
            Show5DigitLedSeg(4,118);
            Show5DigitLedSeg(5,14);
            DelayMs(1000);
            Show5DigitLedSeg(1,189);
            Show5DigitLedSeg(2,0);
            Show5DigitLedSeg(3,0);
            Show5DigitLedSeg(4,0);
            Show5DigitLedSeg(5,0);
            DelayMs(1000);
	}
}
