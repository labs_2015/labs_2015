/// @author Dmitruk Michael

#include "stdafx.h"
#include <iostream>
#include <vector>
#include <math.h>

using namespace std;

/// @brief Function for the linear analysis of simulating object
vector<double> Linear(int Period, double OutputTemperature, double outWarmth)
{
	vector<double> linearResults;
	linearResults.push_back(OutputTemperature);

	for (int period = 1; period < Period; period++)
	{
		linearResults.push_back(0.988 * linearResults.back() + 0.232 * outWarmth);
	}

	return linearResults;
}

/// @brief Function for the non-linear analysis of simulating object
vector <double> NonLinear(int Period, double OutputTemperature, double outWarmth)
{
	vector<double> nonLinearResults;
	nonLinearResults.push_back(OutputTemperature);
	nonLinearResults.push_back(OutputTemperature);

	for (int period = 2; period < Period; period++)
	{
		nonLinearResults.push_back(0.9 * nonLinearResults[period - 1] - 0.001 * pow(nonLinearResults[period - 2], 2.0) + outWarmth + sin(outWarmth));
	}

	return nonLinearResults;
}

/// @brief The main function on project
int main()
{
	int Period = 0;
	double outTemperature = 0.0, outWarmth = 0.0;
	cout << "Input period: ";
	cin >> Period;
	cout << "Ouput temp: ";
	cin >> outTemperature;
	cout << "Ouput warmth: ";
	cin >> outWarmth;

	vector<double> linearModel = Linear(Period, outTemperature, outWarmth);
	vector<double> nonlinearModel = NonLinear(Period, outTemperature, outWarmth);

	cout << " #" << "\t" << "Linear" << "\t\t" << "Non-linear" << endl;

	for (int i = 0; i < linearModel.size(); i++)
		cout << i + 1 << "\t" << linearModel[i] << "\t\t" << nonlinearModel[i] << endl;
	system("pause");
	return 0;
}