///@file
///@author ������� ������
///@date 25.11.2015
#include"stdafx.h"
#include <iostream>

using namespace std;

class Abstract
{
public:
	///@brief ���������� ���������� � �������� ����������� �������
	virtual double getTemp(double y, double u) = 0;
};

class Object1 : public Abstract
{
public:


	double getTemp(double y, double u)
	{
		return 0.988*y + 0.232*u;
	}
};


class Object2 : public Abstract
{
protected:
	double yL = 20;

public:

	///@brief ���������� �������� �������� ����������� �������
	double getY1()
	{
		return yL;
	}

	///@brief ������������� �������� �������� ����������� �������
	void setY1(double y)
	{
		this->yL = y;
	}

	double getTemp(double y, double u)
	{
		double result = 0.9*y - 0.001*yL*yL + u + sin(u);
		yL = y;
		return result;
	}
};





class PID
{
private:
	double T0, Td, T, k, q0, q1, q2, e1 = 0, e2 = 0, e3 = 0, u = 0;
public:
	///@brief ������������� �������� ��� T0
	void SetT0(double t)
	{
		T0 = t;
	}
	double getT0()
	{
		return T0;
	}
	///@brief ������������� �������� ��� Td
	void SetTd(double t)
	{
		Td = t;
	}
	double getTd()
	{
		return Td;
	}
	///@brief ������������� �������� ��� T
	void SetT(double t)
	{
		T = t;
	}
	double getT()
	{
		return T;
	}
	///@brief ������������� �������� ��� �
	void SetK(double K)
	{
		k = K;
	}
	double getK()
	{
		return k;
	}
	///@brief ������������ �������� q
	void CalcQ()
	{
		q0 = k * (1 + (Td / T0));
		q1 = -k *(1 + 2 * (Td / T0) - (T0 / T));
		q2 = k * (Td / T0);
	}

	double Calc(double y, double w)
	{
		e3 = e2;
		e2 = e1;
		e1 = w - y;
		u += (q0 * e1 + q1 * e2 + q2 * e3);
		return u;
	}

};

int main()
{
	Object1 o1;
	Object2 o2;
	PID pid;
	pid.SetK(0.5);
	pid.SetT(0.5);
	pid.SetT0(0.2);
	pid.SetTd(0.01);
	pid.CalcQ();
	o2.setY1(0);

	double w = 50, y = o1.getTemp(0, 0), u = pid.Calc(y, w);

	cout.width(8);
	cout << "y" << "  |  " << "u" << endl;
	cout << "-------------------" << endl;

	for (int i = 0; i < 15; i++)
	{
		y = o1.getTemp(y, u);
		u = pid.Calc(y, w);
		cout.width(8);
		cout << y << "  |  " << u << endl;
	}

	cout << endl << endl;


	y = o2.getTemp(0, 0);
	u = pid.Calc(y, w);

	cout.width(8);
	cout << "y" << "  |  " << "u" << endl;
	cout << "-------------------" << endl;

	for (int i = 0; i < 15; i++)
	{
		y = o2.getTemp(y, u);
		u = pid.Calc(y, w);
		cout.width(8);
		cout << y << "  |  " << u << endl;
	}

	system("Pause");
}