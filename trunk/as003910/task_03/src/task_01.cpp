///@file
///@Author Dobrolinsky Maxim Olegovich
///@25.09.2015

#include "stdafx.h"
#include <math.h>
#include <iostream>
///@mainpage Result
///@image html Capture.png
///@brief creating the model
using namespace std;

///@brief set input temperature
int setU()
{
	return 250;
}

class Abstr
{
public:
	///@brief create method show
	virtual void show() = 0;

private:
	double y = 0;
};

class First : public Abstr
{
public:
	double y = 0;
	void show()
	{
		cout << "Linear model" << endl;
		cout << "u = 250" << endl;
		cout << "-- y --" << endl;
		cout << y << endl;
		for (int i = 1; i < 10; i++)
		{
			///@brief calculating linear temperature
			cout << (y = 0.988*y + 0.232*setU()) << endl;
		}
	}
};

class Second : public First
{
public:
	double y1 = 20;
	double y0 = 0;
	void show()
	{
		double nextY = 0, prevY = y1;
		cout << "Non-linear model" << endl;
		cout << "u = 250" << endl;
		cout << "-- y --" << endl;
		cout << y0 << endl;
		cout << y1 << endl;
		for (int i = 2; i < 10; i++)
		{
			y = nextY;
			///@brief calculating not-linear temperature
			cout << (nextY = 0.9*nextY - 0.001 * (prevY * prevY) + setU() + sin(setU())) << endl;
			prevY = y;

		}
	}

};




int main()
{
	///@brief create object
	First obj;
	///@brief call methon show
	obj.show();
	cout << endl;
	Second obj2;
	obj2.show();
	system("pause");
	return 0;
}

