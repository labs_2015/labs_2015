#include "stdafx.h"
#include "BaseClass.h"
#include <math.h>

BaseClass::BaseClass()
{
}


BaseClass::~BaseClass()
{
}
///@brief calculate basic model
double BaseClass::calcq0(double k, double td, double t0)
{
	double result = 0;
	result = k*(1 + td / t0);
	return result;
}
///@brief calculate basic model
double BaseClass::calcq1(double k, double td, double t0, double ti)
{
	double result = 0;
	result = -k*(1 + 2 * td / t0 - t0 / ti);;
	return result;
}
///@brief calculate basic model
double BaseClass::calcq2(double k, double td, double t0)
{
	double result = 0;
	result = k*td / t0;
	return result;
}