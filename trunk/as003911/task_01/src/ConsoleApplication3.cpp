// ConsoleApplication3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>

using namespace std;

int main()
{
	/** 
	@output massage "Hello World"
   */
	cout << "Hello World" << endl;
	/**
	@pause
	*/
	system("pause");
    return 0;
}

