/**
@mainpage console application for display messege "Hello World"
@file
@author Dubina
@date 01,11,2015
*/
#include "stdafx.h"
#include <iostream>

using namespace std; 
	
/**
@brief defined an entry dot in this program
*/
	
	int main()
{
	/**
	@output massage "Hello World"
	*/	
	cout << "Hello World" << endl;
	/**
	@pause
	*/
	system("pause");
	return 0;
}