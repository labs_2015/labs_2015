/**
@file
@author Eugene Dubina
@date 14.11.15
*/
#include "stdafx.h"
#include <math.h>
#include <iostream>

using namespace std;
/**
@brief We define class "Abstractniy"
*/
class Abstractniy
{
protected:
	double y0;
public:
	Abstractniy()
	{
		y0 = 0;
	}
	double getY0()
	{
		return y0;
	}

	void setY0(double y0)
	{
		this->y0 = y0;
	}
	virtual void VyvodTemperature(int(*u)(int)) = 0;
};
/**
@brief heirdon class LineModel from Abstractniy
*/
class LineModel : public Abstractniy
{
public:
	/**
	@brief show table
	*/
	void VyvodTemperature(int(*ut)(int))
	{
		cout.width(8);
		cout << "y" << " | " << "u" << "   | " << "t" << endl;
		cout << "--------------------" << endl;
		cout.width(8);
		cout << y0 << " | " << ut(0) << " | " << "0" << endl;
		double y = y0;
		for (int i = 1; i <= 15; i++)
		{
			cout.width(8);
			cout << (y = 0.988 * y + 0.232 * ut(i - 1)) << " | " << ut(i - 1) << " | " << i << endl;
		}
	}
};
/**
@brief heirdon class NotLineModel from Abstractniy
*/
class NotLineModel : public Abstractniy
{
protected:
	double y1;
public:
	NotLineModel()
	{
		y1 = 20;
	}

	double getY1()
	{
		return y1;
	}

	void setY1(double y1)
	{
		this->y1 = y1;
	}
	/**
	@brief vyvodim tablic
	*/
	void VyvodTemperature(int(*ut)(int))
	{
		cout.width(8);
		cout << "y" << " | " << "u" << "   | " << "t" << endl;
		cout << "--------------------" << endl;
		cout.width(8);
		cout << y0 << " | " << ut(0) << " | " << "0" << endl;
		cout.width(8);
		double yPrev = y0, yNext = y1;
		for (int i = 1; i <= 15; i++)
		{
			double y = yNext;
			cout.width(8);
			cout << (yNext = 0.9 * yNext - 0.001 * yPrev * yPrev + ut(i - 1) + sin(static_cast<double>(ut(i - 2))))
				<< " | " << ut(i - 1) << " | " << i << endl;
			yPrev = y;
		}
	}
};

int u(int t)
{
	return 150;
}
/**
@brief We define an entry point in the program
@mainpage Result
@image html LineModel.png
\details zavisimoct'  lineynay t.k. nety elementov c nelineynoy zavisimostyu
@image html NotLineModel.png
\details zavisimost' nelineynay t.k. est' elementy, y kotoryh nelineynaya zavisimost' .
@brief creating the mode
*/
int main()
{
	LineModel o1;
	NotLineModel o2;
	o1.VyvodTemperature(&u);
	cout << endl << endl;
	o2.VyvodTemperature(&u);
	system("pause");
}