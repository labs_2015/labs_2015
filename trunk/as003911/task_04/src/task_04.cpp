/**
@mainpage console application wich displays control object
@file
@author Eugene Dubina
@date 28.11.15
*/
#include  "stdafx.h"
#include <math.h>
#include <iostream>

using namespace std;
/**
@brief We define class "Abstractniy"
*/
class Abstraktniy {
public:
	float Y[20], ut = 5;
	double T0 = 0.5,
		Td = 0.02,
		T = 0.5,
		k = 0.5,
		q0,
		q1,
		q2,
		e1 = 0,
		e2 = 0,
		e3 = 0,
		u = 0,
		w = 40,
		y = 0;
	/**
	@brief calculations
	*/
	void pid()
	{
		q0 = k*(1 + (Td / T0));
		q1 = -k *(1 + 2 * (Td / T0) - (T0 / T));
		q2 = k * (Td / T0);
		e3 = e2;
		e2 = e1;
		e1 = w - y;
		u += (q0 * e1 + q1 * e2 + q2 * e3);
	}
protected:
	virtual void output() = 0;
};

/**
@brief heirdon class LineModel from Abstractniy
*/
class LineModel : public Abstraktniy
{
public:
	/**
	@brief create and output first table which showing LineModel
	*/
	void output()
	{
		cout.width(8);
		cout << "Y" << " / " << "U\n";
		double  y1 = 0;
		for (int i = 0; i <= 15; i++)
		{
			pid();
			y1 = 0.988*y + 0.232*u;
			y = y1;
			cout.width(8);
			cout << y1 << " / " << u << endl;
		}
	}
};
/**
@brief heirdon class NotLineModel from Abstractniy
*/
class NotLineModel : public Abstraktniy
{
public:
	/**
	@brief create and output second table which showing NotLineModel
	*/
	void output()
	{
		double y2 = 0, y1 = 0;
		cout.width(8);
		cout << "Y" << " / " << "U\n";
		for (int i = 1; i <= 15; i++)
		{
			pid();
			y2 = 0.9*y1 - 0.001*y * y + u + sin(u);
			y1 = y2;
			y = y1;
			cout.width(8);
			cout << y2 << " / " << u << endl;
		}
	}
};


/**
@brief We define an entry point in the program
@mainpage Result
@image html LineModel.png
@image html NotLineModel.png
@brief creating the mode
*/
int main()

{
	LineModel O1;
	NotLineModel O2;
	O1.output();
	O2.output();
	system("pause");
	return 0;
};