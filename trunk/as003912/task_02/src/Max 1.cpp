/**
@mainpage  A console application that displays the "Hello World!"
@file
@author Maxim Zdanevich
*/
#include "stdafx.h"
#include <iostream>
using namespace std;
/**
@brief define an entry point in the program
*/
int main()
{
	/**
	@brief output message "HelloWorld"
	*/
	cout << "Hello world!" << endl;
/**
@brief waiting for something
*/
	system("pause");
	return 0;
}