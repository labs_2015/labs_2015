///@file
///@author ������ ��������
///@30.10.2015
#include "stdafx.h"
#include <iostream>
#include <math.h>
using namespace std;

int u = 10;
/**@class abstr
����������� �����
*/
class abstr
{
public:
	virtual void show() = 0;
protected:
	double y[10];
};
/**@class Linear
��������� ����� �� ��������(������������)
*/
class Linear : public abstr
{
public:
	void show()
	{
		cout << "\tLinear model" << endl << endl;
		cout.width(10);
		cout << "u = 10" << endl << endl << "\t" << "y" << "\t" << "t" << endl << endl;
		y[0] = 0;
		for (int t = 0; t < 11; t++)
		{
			y[t + 1] = 0.988*y[t] + 0.232*u;
			cout.width(10);
			cout << y[t] << "\t" << t << endl << endl;
		}
	}
};
/**@class Nonlinear
��������� ����� �� ��������(������������)
*/
class Nonlinear : public abstr
{
public:
	void show()
	{
		cout << "\tNonlinear model" << endl << endl;
		cout.width(10);
		cout << "u = 10" << endl << endl << "\t" << "y" << "\t" << "t" << endl << endl;
		y[0] = 0;
		y[1] = 0;
		for (int t = 1; t < 11; t++)
		{
			y[t + 1] = 0.9*y[t] - 0.001*y[t - 1] * y[t - 1] + u + sin(u);
			cout.width(10);
			cout << y[t] << "\t" << t << endl << endl;
		}
	}
};
/**@mainpage ������
@image html nelin.png
\details ����������� �������������� �� ������� �������� ���������� �.�. ������������ ��������, ������� ���������� �����������
@image html lin.png
\details ����������� �������������� �� ������� �������� �������� �.�. ��� ��������� ������� ���������� �����������
*/
int main()
{
	Linear lin;
	Nonlinear nonlin;
	lin.show();
	cout << endl;
	nonlin.show();
	system("pause");
	return 0;
};