///@file
///@author �������� ������
///@25.11.2015
#include <iostream>
#include <math.h>
using namespace std;
/**@class Abstr
����������� �����
*/
class Abstr {
public:
	///@param ��������� ���-�����������
	double T0 = 0.5, Td = 0.02, T = 0.5, k = 0.5, q0, q1, q2, e1 = 0, e2 = 0, e3 = 0, u = 0, w = 40, y = 0;

	void PID()
	{
		q0 = k*(1 + (Td / T0));
		q1 = -k *(1 + 2 * (Td / T0) - (T0 / T));
		q2 = k * (Td / T0);
		e3 = e2;
		e2 = e1;
		e1 = w - y;
		u += (q0 * e1 + q1 * e2 + q2 * e3);
	}
protected:
	virtual void show() = 0;
};
/**@class Linear
��������� ����� �� ��������(������������)
*/
class Linear : public Abstr
{
public:
	void show()
	{

		cout << "Linear\n";
		cout.width(8);
		cout << "y" << "\t" << "u\n";
		double  y1 = 0;
		for (int i = 0; i <= 20; i++)
		{
			PID();
			y1 = 0.988*y + 0.232*u;
			y = y1;
			cout.width(8);
			cout << y1 << "\t" << u << endl;
		}
	}
};
/**@class NonLinear
��������� ����� �� ��������(������������)
*/
class NonLinear : public Abstr
{
public:
	void show()
	{
		double y2 = 0, y1 = 0;

		cout << "\NonLinear\n";
		cout.width(8);
		cout << "y" << "\t" << "u\n";
		for (int i = 1; i <= 20; i++)
		{
			PID();
			y2 = 0.9*y1 - 0.001*y * y + u + sin(u);
			y1 = y2;
			y = y1;
			cout.width(8);
			cout << y2 << "\t" << u << endl;
		}
	}
};
/**@mainpage ���-����������
@image html linmod.png
@image html nonlinmod.png
*/
int main()
{
	Linear q;
	NonLinear w;
	q.show();
	w.show();
	system("pause");
	return 0;
};
