/*! \mainpage Project "HELLO WORLD!!!!"
* ���������, �����������  �����"HELLO WORLD!!!!".
*\file hello.cpp
*\brief �������� ������� ������� ������� � ������� ������� "HELLO WORLD!!!!".
*/
#include <iostream>
/*!
\brief ������� �� ������� ����������� "HELLO WORLD!!!!".
\param argc
\return 0-�������� ����������
*/
#include <string>
using namespace std;

int main()//! ������� main() - �������, ������� ���������� ���������� ����� ������� ���������. � ��� ���������������� ������.
{
	/*!
	���������� ������ ��������� �� ��������:
	\code
	string str = "HELLO WORLD!!!!";
	\endcode
	*/
	string str = "HELLO WORLD!!!!";
	/**
	����� ������ �������� �� �����:
	\code
	cout � str � endl;
	\endcode
	*/
	cout << str << endl;
	system("pause");
	return 0;
}