/*! \mainpage Project "������������ ������ 2"
* ���������, ������������ ���-���������.
* ����������� ������������� ���: ��� ������ � ������������.
*  \image html lin.png
* * <b><center> ��� �������� ������ </b></center>
*  \image html nonlin.png
* <b><center>  ��� ���������� ������ </b></center>
* \file main.cpp
* \brief ���� � �������� �����, � ������� �������������� �������� �������� ������� linmod � nonlinmod, � ���������� ������ ShowResult() ��� ����.
*/

#include "Model.h"
#include "linmod.h""
#include "nonlinmod.h"		
#include <iostream>
using namespace std;

//!�������, � ������� �������� �������� �������� ������� linmod � nonlinmod, � ����� ���������� ������ ShowResult() ��� ����.
int main()
{
	setlocale(0, "");
	linmod *Objectlinmod = new linmod;
	Objectlinmod->ShowResult();
	nonlinmod *Objectnonlinmod = new nonlinmod;
	Objectnonlinmod->ShowResult();
	system("pause");
	return 0;
}

