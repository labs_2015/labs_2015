/*! \file LinearModel.h
* \brief ������������ ����, ���������� � ���� ��������� ������� ShowResult(), �������� ������ LinearModel, ����� ������������ � �����������.
*/
#ifndef _LINEARMODEL_H
#define _LINEARMODEL_H
#include <math.h>
#include <iostream>
#include "Model.h"

/*!����� ��� ���������� �������� ������, �������������� �� ������ Model.
* \brief ����� - ������� ������ Model, ����������� ������� ������ �������.
*/
class LinearModel : public Model
{
public:
	LinearModel();
	~LinearModel();
	void LinearModel::ShowResult();
};
#endif 