/*! \file NonLinearModel.h
* \brief ������������ ����, ���������� � ���� ��������� ������� ShowResult(), �������� ������ NonLinearModel, � ����� ������������ � �����������.
*/
#ifndef _NONLINEARMODEL_H
#define _NONLINEARMODEL_H
#include <math.h>
#include <iostream>
#include "Model.h"

/*!����� ��� ���������� ���������� ������, �������������� �� ������ Model.
* \brief ����� - ������� ������ Model, ����������� ��������� ������ �������.
*/
class NonLinearModel : public Model
{
public:
	NonLinearModel();
	~NonLinearModel();
	void NonLinearModel::ShowResult();
};

#endif

