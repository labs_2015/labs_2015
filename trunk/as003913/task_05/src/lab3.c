/**
@mainpage  The program will appear on the seven-segment display a scrolling "Hello World!"
@file
@author Evgeny Kislow
@date 12.12.15
*/
#include "i7188.h"

int main()
{
	while(1)///Ticker
	{
		Show5DigitLedSeg(1, 55);	///function for display 'H'
		Show5DigitLedSeg(2, 79);	///E
		Show5DigitLedSeg(3, 14);	///L
		Show5DigitLedSeg(4, 14);	///L
		Show5DigitLedSeg(5, 126);	///O
		DelayMs(750);///pause 750 milliseconds
		Show5DigitLedSeg(1, 79);
		Show5DigitLedSeg(2, 14);
		Show5DigitLedSeg(3, 14);
		Show5DigitLedSeg(4, 126);
		Show5DigitLedSeg(5, 0); ///_
		DelayMs(750);
		Show5DigitLedSeg(1, 14);	
		Show5DigitLedSeg(2, 14);
		Show5DigitLedSeg(3, 126);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 30);	///|_-
		DelayMs(750);
		Show5DigitLedSeg(1, 14);	
		Show5DigitLedSeg(2, 126);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 30);
		Show5DigitLedSeg(5, 60);	///-_|
		DelayMs(750);
		Show5DigitLedSeg(1, 126);	
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 30);
		Show5DigitLedSeg(4, 60);
		Show5DigitLedSeg(5, 126);
		DelayMs(750);
		Show5DigitLedSeg(1, 0);	
		Show5DigitLedSeg(2, 30);
		Show5DigitLedSeg(3, 60);
		Show5DigitLedSeg(4, 126);
		Show5DigitLedSeg(5, 70);	///r
		DelayMs(750);
		Show5DigitLedSeg(1, 30);	
		Show5DigitLedSeg(2, 60);
		Show5DigitLedSeg(3, 126);
		Show5DigitLedSeg(4, 70);
		Show5DigitLedSeg(5, 14);
		DelayMs(750);
		Show5DigitLedSeg(1, 60);	
		Show5DigitLedSeg(2, 126);
		Show5DigitLedSeg(3, 70);
		Show5DigitLedSeg(4, 14);
		Show5DigitLedSeg(5, 189);	///d.
		DelayMs(750);
		Show5DigitLedSeg(1, 126);	
		Show5DigitLedSeg(2, 70);
		Show5DigitLedSeg(3, 14);
		Show5DigitLedSeg(4, 189);
		Show5DigitLedSeg(5, 0);
		DelayMs(750);
		Show5DigitLedSeg(1, 70);	
		Show5DigitLedSeg(2, 14);
		Show5DigitLedSeg(3, 189);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 103);	///P
		DelayMs(750);
		Show5DigitLedSeg(1, 14);	
		Show5DigitLedSeg(2, 189);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 103);
		Show5DigitLedSeg(5, 126);
		DelayMs(750);
		Show5DigitLedSeg(1, 189);	
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 103);
		Show5DigitLedSeg(4, 126);
		Show5DigitLedSeg(5, 102);	///M(1)
		DelayMs(750);
		Show5DigitLedSeg(1, 0);	
		Show5DigitLedSeg(2, 103);
		Show5DigitLedSeg(3, 126);
		Show5DigitLedSeg(4, 102);
		Show5DigitLedSeg(5, 114);	///M(2)
		DelayMs(750);
		Show5DigitLedSeg(1, 103);	
		Show5DigitLedSeg(2, 126);
		Show5DigitLedSeg(3, 102);
		Show5DigitLedSeg(4, 114);
		Show5DigitLedSeg(5, 246);	///A.
		DelayMs(750);
		Show5DigitLedSeg(1, 126);	
		Show5DigitLedSeg(2, 102);
		Show5DigitLedSeg(3, 114);
		Show5DigitLedSeg(4, 246);
		Show5DigitLedSeg(5, 0);
		DelayMs(750);
		Show5DigitLedSeg(1, 102);	
		Show5DigitLedSeg(2, 114);
		Show5DigitLedSeg(3, 246);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 55);
		DelayMs(750);
		Show5DigitLedSeg(1, 114);	
		Show5DigitLedSeg(2, 246);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 55);
		Show5DigitLedSeg(5, 79);
		DelayMs(750);
		Show5DigitLedSeg(1, 246);	
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 55);
		Show5DigitLedSeg(4, 79);
		Show5DigitLedSeg(5, 14);
		DelayMs(750);
		Show5DigitLedSeg(1, 0);	
		Show5DigitLedSeg(2, 55);
		Show5DigitLedSeg(3, 79);
		Show5DigitLedSeg(4, 14);
		Show5DigitLedSeg(5, 14);
		DelayMs(750);
	}
}