/**
@mainpage console application for display messege "Hello World"
@file
@author Anton Kononovich
@date 29.10.15
*/
#include <iostream>
using namespace std;
/**
@brief defined an entry dot in this program
*/
int main()
{
	/**
	@brief output message "Hello world"
	*/
	cout << "Hello world" << endl;
	/**
	@brief detained the console screen
	*/
	system("pause");
	return 0;
}
