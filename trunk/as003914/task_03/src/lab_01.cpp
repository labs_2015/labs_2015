/**
@mainpage console application wich displays control object
@file
@author Anton Kononovich
@date 10.11.15
*/
#include <math.h>
#include <iostream>

using namespace std;
/**
@brief defined parent class
*/
class Tabl
{
protected:
	double y0;
public:
	Tabl()
	{
		y0 = 0;
	}
	double getY0()
	{
		return y0;
	}

	void setY0(double y0)
	{
		this->y0 = y0;
	}
	virtual void TempShowing(int(*u)(int)) = 0;
};
/**
@brief defined 1st inherited class from Tabl
*/
class Tabl_1 : public Tabl
{
public:
	void TempShowing(int(*ut)(int))
	{
		cout.width(8);
		cout << "y" << " \\ " << "u" << " \\ " << "t" << endl;
		cout.width(8);
		cout << y0 << " \\ " << ut(0) << " \\ " << "0" << endl;
		double y = y0;
		for (int i = 1; i <= 10; i++)
		{
			cout.width(8);
			/**
			@brief perform calculations of linear model of temp
			*/
			cout << (y = 0.988 * y + 0.232 * ut(i - 1)) << " \\ " << ut(i - 1) << " \\ " << i << endl;
		}
	}
};
/**
@brief defined 2nd inherited class from Tabl
*/
class Tabl_2 : public Tabl
{
protected:
	double y1;
public:
	Object2()
	{
		y1 = 20;
	}

	double getY1()
	{
		return y1;
	}

	void setY1(double y1)
	{
		this->y1 = y1;
	}

	void TempShowing(int(*ut)(int))
	{
		cout.width(8);
		cout << "y" << " \\ " << "u" << " \\ " << "t" << endl;
		cout.width(8);
		cout << y0 << " \\ " << ut(0) << " \\ " << "0" << endl;
		cout.width(8);
		double yLast = y0, yNext = y1;
		for (int i = 1; i <= 10; i++)
		{
			double y = yNext;
			cout.width(8);
			/**
			@brief perform calculations of nonlinear model of temp
			*/
			cout << (yNext = 0.9 * yNext - 0.001 * yLast * yLast + ut(i - 1) + sin(static_cast<double>(ut(i - 2))))<< " \\ " << ut(i - 1) << " \\ " << i << endl;
			yLast = y;
		}
	}
};

int u(int t)
{
	return 88;
}
/**
@brief cerated class object and method of showing
*/
int main()
{
	Tabl_1 Z1;
	Tabl_2 Z2;
	Z1.TempShowing(&u);
	cout << endl;
	Z2.TempShowing(&u);
	system("pause");
}

