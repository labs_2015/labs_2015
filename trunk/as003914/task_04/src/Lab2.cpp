/**
@file
@author Anton Kononovich
@date 10.11.15
*/
#include <math.h>
#include <iostream>

using namespace std;
/**
@class defined abstract class
*/
class A_class {
public:
	float Y[20], ut = 5;
	double T0 = 0.5, Td = 0.02, T = 0.5, k = 0.5,
		   q0, q1, q2,
		   e1 = 0, e2 = 0, e3 = 0,
		   u = 0, w = 40, y = 0;
	/**
	@brief needful computations
	*/
	void pid()
	{
		q0 = k*(1 + (Td / T0));
		q1 = -k *(1 + 2 * (Td / T0) - (T0 / T));
		q2 = k * (Td / T0);
		e3 = e2;
		e2 = e1;
		e1 = w - y; 
		u += (q0 * e1 + q1 * e2 + q2 * e3);
	}
protected:
	virtual void output() = 0;
};
/**
@class created 1st table which showing liner model
*/
class lin_mod : public A_class
{
public:
	void output()
	{
		cout.width(8);
		cout << "Y" << " / " << "U\n";
		double  y1 = 0;
		for (int i = 0; i <= 20; i++)
		{
			pid();
			y1 = 0.988*y + 0.232*u;
			y = y1;
			cout.width(8);
			cout << y1 << " / " << u << endl;
		}
	}
};
/**
@class created 2st table which showing liner model
*/
class nelin_mod : public A_class
{
public:
	void output()
	{
		double y2 = 0, y1 = 0;
		cout.width(8);
		cout << "Y" << " / " << "U\n";
		for (int i = 1; i <= 20; i++)
		{
			pid();
			y2 = 0.9*y1 - 0.001*y * y + u + sin(u);
			y1 = y2;
			y = y1;
			cout.width(8);
			cout << y2 << " / " << u << endl;
		}
	}
};
/**
@mainpage console application wich displays control object
@image html lin.png
*/
int main()
{
	lin_mod a;
	nelin_mod b;
	a.output();
	b.output();
	system("pause");
	return 0;
};
