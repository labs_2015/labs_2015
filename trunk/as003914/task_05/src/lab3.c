/**
@mainpage console application wich displays Hello World on the LED
@file
@author Anton Kononovich
@date 15.12.15
*/
#include "i7188.h"
/**
@brief application displays "HELLO WORLd" by some of the seven segments on LED
*/
int main()
{
   while(1)
   {
       Show5DigitLedSeg(1,55);
       Show5DigitLedSeg(2,79);
       Show5DigitLedSeg(3,14);
       Show5DigitLedSeg(4,14);
       Show5DigitLedSeg(5,126);
       DelayMs(3000);
       Show5DigitLedSeg(1,30);
       Show5DigitLedSeg(2,60);
       Show5DigitLedSeg(3,126);
       Show5DigitLedSeg(4,118);
       Show5DigitLedSeg(5,14);
       DelayMs(3000);
   }
}
