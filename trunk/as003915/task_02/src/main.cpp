/**
@mainpage  A console application that displays the "Hello World!"
@file
@author Roman Kreidzich
@date 10.11.15
*/
#include <iostream>
/**
@brief We define an entry point in the program
*/
int main()
{
	/**
	@brief Display message "Hello,world"
	*/
	std::cout << "Hello World" << std::endl;
	///@brief We set the display console
	system("pause");
	return 0;
}