#include <iostream>
#include <cmath>

using namespace std;

class TemperatureChange
{
public:
	double Y[20];
	double Ut = 10;
protected:
	virtual void show() = 0;
};

class LinearTemperatureChange : public TemperatureChange
{
public:
	void show()
	{
		cout << "Linear change:" << endl;
		cout <<"Y         | T" << endl;
		Y[0] = 0;
		cout.width(10);
		cout << left <<Y[0] << "| 1" << endl;
		for (int i = 1; i < 20; i++)
		{
			cout.width(10);
			Y[i] = 0.988*Y[i - 1] + 0.232*Ut;
			cout<<left << Y[i]<< "| "<<i + 1 << endl;

		}
	}
};

class NonlinearTemperatureChange : public TemperatureChange
{
public:
	void show()
	{
		cout << "Nonlinear change:" << endl;
		cout << "Y         | T" << endl;
		Y[0] = 0;
		cout.width(10);
		cout << left << Y[0] << "| 1" << endl;
		Y[1] = 0.9*Y[0] + Ut + sin(Ut);
		cout.width(10);
		cout << left << Y[1] << "| 2" << endl;
		for (int i = 2; i < 20; i++)
		{
			Y[i] = 0.9*Y[i - 1] - 0.001*pow(Y[i - 2],2) + Ut + sin(Ut);
			cout.width(10);
			cout << left << Y[i] << "|"<<i + 1 << endl;
		}
	}
};

int main()
{
	LinearTemperatureChange line;
	NonlinearTemperatureChange NoLine;
	line.show();
	NoLine.show();
	system("pause");
	return 0;
}