/*!
	@file
	@brief Realisation of LinearControllObject class
*/
#include "stdafx.h"
#include "LinearControllObject.h"


LinearControllObject::LinearControllObject(float CurrentT) : AbstractControllObject(CurrentT)
{

}
/*!
	Overloads incrTemp function
	@code
	float LinearControllObject::incrTemp(float QuantityOFHeat) {

	return CurrentTemperature = CurrentTemperature*0.988 + QuantityOFHeat*0.232;
	}
	@encode
*/
float LinearControllObject::incrTemp(float QuantityOFHeat) {

	return CurrentTemperature = CurrentTemperature*0.988 + QuantityOFHeat*0.232;
}
