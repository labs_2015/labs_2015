#pragma once
/**
@mainpage ������
@image html Linear.png
@image html Nonlinear.png
�� �������� ����� ��� �� ���������� ������.
���������� ������ ������������ ������� ��� ��������.
*/
class GeneralModel
{
public:
	GeneralModel();
	///@brief ����������� ����� ��� ������ �������� �������
	virtual double Model(double yt, double ut) = 0;
	~GeneralModel();
};


