///@author Kuzavka Dima
///@date 17.12.15

#include "stdafx.h"
#include "i7188.h"

///@mainpage  Hello World
///@file task_05.c

///@brief Show on the dispaly of the  controller "HELLO WORLd."
int main()
{
	while (1)
	{
		Show5DigitLedSeg(1, 55);
		Show5DigitLedSeg(2, 79);
		Show5DigitLedSeg(3, 14);
		Show5DigitLedSeg(4, 14);
		Show5DigitLedSeg(5, 126);
		DelayMs(1000);
		Show5DigitLedSeg(1, 30);
		Show5DigitLedSeg(2, 60);
		Show5DigitLedSeg(3, 126);
		Show5DigitLedSeg(4, 118);
		Show5DigitLedSeg(5, 14);
		DelayMs(1000);
		Show5DigitLedSeg(1, 189);
		DelayMs(1000);
	}
	return 0;
}