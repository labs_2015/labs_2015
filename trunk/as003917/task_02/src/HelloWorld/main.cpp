/**
@mainpage This program was displayed a "Hello World!"
@file
@author Litskevich Martsin
@date 05.12.2015
*/
#include <iostream>
using namespace std;
/**
@brief Entry point is here
*/
int main()
{
	/**
	@brief Display message "hello world"
	*/
	cout << "Hello World!" << endl;
	///@brief We detain the console screen
	system("pause");
	return 0;
}

