/*! \file LinModel.cpp
* \brief Linear model
*/
#include "LinModel.h"
#include "Models.h"
#include "stdafx.h"

//! Modeling pin with linear model.
void LinModel::ShowResult()
{
	for (int i = 1; i <= 30; i++)
	{
		PidController();
		y1 = 0.988*y + 0.232*u;
		y = y1;
		std::cout << i << "  |  " << y1 << "  |  " << u << std::endl;
	}
}
LinModel::LinModel()
{
	std::cout << "\nPID-regulator linear models:" << std::endl;
	std::cout << "           Y" << "  |  " << "      U" << "  |  " << std::endl;
}

LinModel::~LinModel() {}