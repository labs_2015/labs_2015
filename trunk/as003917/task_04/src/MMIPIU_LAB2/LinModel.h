/*! \file LinearModel.h
* \brief Linear model class
*/
#ifndef _LINMODEL_H
#define _LINMODEL_H
#include <math.h>
#include <iostream>
#include "Models.h"

/*!Class for realising linear model
* \brief Realiseing linear model
*/
class LinModel : public Models
{
public:
	LinModel();
	~LinModel();
	void LinModel::ShowResult();
};
#endif 