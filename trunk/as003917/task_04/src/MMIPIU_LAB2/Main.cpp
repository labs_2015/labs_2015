/*! \mainpage Project "Labor �2"
*  \image html lin.png
* * <b><center> For linear model </b></center>
*  \image html nlin.png
* <b><center>  For non linear model </b></center>
* \author Litskevich Martsin
* \file main.cpp
*/

#include "Models.h"
#include "LinModel.h"
#include "NLinModel.h"		
#include <iostream>
#include "stdafx.h"
using namespace std;

//!How to create models
int main()
{
	LinModel *ObjectLinModel = new LinModel;
	ObjectLinModel->ShowResult();
	NLinModel *ObjectNLinModel = new NLinModel;
	ObjectNLinModel->ShowResult();
	system("pause");
	return 0;
}

