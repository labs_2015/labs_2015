/*! \file Models.h
* \brief Header of Models
*/
#ifndef _MODELS_H
#define _MODELS_H

/*!Abstract class model
* \brief Model class
*/
class Models
{
public:

	double Ti = 0.32;

	double Td = 0.12;

	double K = 0.63;

	double T0 = 0.5;

	double e1 = 0;

	double e2 = 0;

	double e3 = 0;

	double q0 = 0;

	double q1 = 0;

	double q2 = 0;

	double u = 0;

	double w = 60;

	double y = 0;

	double y1 = 0;

	double y2 = 0;
	//! Modeling pid
	void Models::PidController();
	//! Show modeling results
	virtual void ShowResult() = 0;
	Models();
	~Models();
};

#endif