/*! \file NLinModel.cpp
* \brief Nlin model class
*/
#include "NLinModel.h"
#include "Models.h"
#include "stdafx.h"

//!Modeling pin with non linear model.
void NLinModel::ShowResult()
{
	for (int i = 1; i <= 30; i++)
	{
		PidController();
		y2 = 0.9*y1 - 0.001*y * y + u + sin(u);
		y1 = y2;
		y = y1;
		std::cout << i << "  |  " << y2 << "  |  " << u << std::endl;
	}
}

NLinModel::NLinModel()
{
	std::cout << "\nPID-regulator non linear model:" << std::endl;
	std::cout << "           Y" << "  |  " << "      U" << "  |  " << std::endl;
}

NLinModel::~NLinModel() {}