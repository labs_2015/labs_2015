/*! \file NLinModel.h
* \brief NLinModel class
*/
#ifndef _NONLINEARMODEL_H
#define _NONLINEARMODEL_H
#include <math.h>
#include <iostream>
#include "Models.h"

/*!Class for realising non linear model
* \brief Realiseing non linear model
*/
class NLinModel : public Models
{
public:
	NLinModel();
	~NLinModel();
	void NLinModel::ShowResult();
};

#endif

