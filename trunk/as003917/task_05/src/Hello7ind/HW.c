/**
Add a comment to this line
@mainpage  Seven-segment display
@file
@author Litskevich Martsin
@date 15.12.15
*/
#include "..\INCLUDE\i7188.h"

int main()
{
		while(1)
	{
		Show5DigitLedSeg(1, 55);	///H
		Show5DigitLedSeg(2, 79);	///E
		Show5DigitLedSeg(3, 14);	///L
		Show5DigitLedSeg(4, 14);	///L
		Show5DigitLedSeg(5, 126);	///O
		DelayMs(750);
		Show5DigitLedSeg(1, 79);        ///E
		Show5DigitLedSeg(2, 14);        ///L
		Show5DigitLedSeg(3, 14);        ///L
		Show5DigitLedSeg(4, 126);       ///O
		Show5DigitLedSeg(5, 0);         ///_
		DelayMs(750);
		Show5DigitLedSeg(1, 14);        ///L
		Show5DigitLedSeg(2, 14);        ///L
		Show5DigitLedSeg(3, 126);       ///O
		Show5DigitLedSeg(4, 0);         ///_
		Show5DigitLedSeg(5, 30);        ///W1
		DelayMs(750);
		Show5DigitLedSeg(1, 14);        ///L
		Show5DigitLedSeg(2, 126);       ///O
		Show5DigitLedSeg(3, 0);         ///_
		Show5DigitLedSeg(4, 30);        ///W1
		Show5DigitLedSeg(5, 60);        ///W2
		DelayMs(750);
		Show5DigitLedSeg(1, 126);       ///O
		Show5DigitLedSeg(2, 0);         ///_
		Show5DigitLedSeg(3, 30);        ///W1
		Show5DigitLedSeg(4, 60);        ///W2
		Show5DigitLedSeg(5, 126);       ///O
		DelayMs(750);
		Show5DigitLedSeg(1, 0);	        ///_
		Show5DigitLedSeg(2, 30);        ///W1
		Show5DigitLedSeg(3, 60);        ///W2
		Show5DigitLedSeg(4, 126);       ///0
		Show5DigitLedSeg(5, 70);	///r
		DelayMs(750);
		Show5DigitLedSeg(1, 30);        ///W1
		Show5DigitLedSeg(2, 60);        ///W2
		Show5DigitLedSeg(3, 126);       ///O
		Show5DigitLedSeg(4, 70);        ///r
		Show5DigitLedSeg(5, 14);        ///L
		DelayMs(750);
		Show5DigitLedSeg(1, 60);        ///W2
		Show5DigitLedSeg(2, 126);       ///O
		Show5DigitLedSeg(3, 70);        ///r
		Show5DigitLedSeg(4, 14);        ///L
		Show5DigitLedSeg(5, 189);	///d
		DelayMs(750);
		Show5DigitLedSeg(1, 126);       ///O
		Show5DigitLedSeg(2, 70);        ///r
		Show5DigitLedSeg(3, 14);        ///L
		Show5DigitLedSeg(4, 189);       ///d
		Show5DigitLedSeg(5, 0);         ///_
		DelayMs(750);
		Show5DigitLedSeg(1, 70);        ///r
		Show5DigitLedSeg(2, 14);        ///L
		Show5DigitLedSeg(3, 189);       ///d
		Show5DigitLedSeg(4, 0);         ///_
		Show5DigitLedSeg(5, 0);	        ///_
		DelayMs(750);
	}
 }