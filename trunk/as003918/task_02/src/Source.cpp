/**
@mainpage  A console application that displays the "Hello World!"
@file
@author Sergej Ayovich
*/

#include <iostream>
using namespace std;
/**
@brief We define an entry point in the program
*/
int main()
{
	/**
	@output message "HelloWorld"
	*/
	cout << "Hello World!" << endl;
	system("pause");
	return 0;
}