///@file
///@Author Sergej Ayovich

#include <math.h>
#include <iostream>
using namespace std;

///@brief set temperature
int setU()
{
	return 120;
}

class BaseClass
{
public:
	virtual void show() = 0;

private:
	double y = 0;
};
/**@class LinearClass
��������� ����� �� ��������(������������)
*/
class LinearClass : public BaseClass
{
public:
	double y = 0;
	void show()
	{
		cout << "Linear model" << endl;
		cout << "y\t" << "i" << endl;
		cout << y <<   "\t" << 0 << endl;
		for (int i = 1; i < 10; i++)
		{
			///@brief output linear temperature

			cout << (y = 0.988*y + 0.232*setU()) << "\t" << i << endl; 
		}
	}
};
/**@class NotLinearClass
��������� ����� �� ��������(������������)
*/
class NotLinearClass : public LinearClass
{
public:
	double y1 = 20;
	double y0 = 0;
	void show()
	{
		double nextY = 0, prevY = y1;
		cout << "Not Linear model" << endl;
		cout << "y\t" << "i" << endl;
		cout << y0 <<  "\t0" << endl;
		cout << y1 <<  "\t1" << endl;
		for (int i = 2; i < 10; i++)
		{
			y = nextY;
			cout << (nextY = 0.9*nextY - 0.001 * (prevY * prevY) + setU() + sin(setU())) << "\t" << i << endl;
			prevY = y;

		}
	}

};
/**@mainpage ������
@image html NotLinear.PNG
\details ����������� �������������� �� ������� �������� ���������� �.�. ������������ ��������, ������� ���������� �����������
@image html Linear.PNG
\details ����������� �������������� �� ������� �������� �������� �.�. ��� ��������� ������� ���������� �����������
*/
int main()
{
	///@brief create object
	LinearClass obj;
	obj.show();
	cout << endl;
	NotLinearClass obj2;
	obj2.show();
	system("pause");
	return 0;
}