/**
@image html lin.png
*/

#pragma once
#include "MainModel.h"
///@brief �������� �����, ����������� �������� ������
class LinearModel :
	public MainModel
{
private:
	double yt;
	double ut;
public:
	LinearModel();
	///@brief ������ ���� � ����� ������� ��������������
	///@param yt - �������� ����������� 
	///@param ut - �����, ����������� � �������
	LinearModel(double yt, double ut);
	///@brief ������� �� ������� ������� �������� ����������� �� ������ ��������(�������� ������)
	///@param t - ���������� ��������
	void PrintModel(int t);
	~LinearModel();
};
