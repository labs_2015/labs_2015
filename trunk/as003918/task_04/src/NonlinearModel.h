/**
@image html nonlin.png
*/

#pragma once
#include "MainModel.h"
///@brief �������� �����, ����������� ���������� ������
class NonlinearModel :
	public MainModel
{
private:
	double yt;
	double ut;
public:
	NonlinearModel();
	///@brief ������ ���� � ����� ������� ��������������
	///@param y0 - �������� ����������� 
	///@param ut - �����, ����������� � �������
	NonlinearModel(double y0, double ut);
	///@brief ������� �� ������� ������� �������� ����������� �� ������ ��������(���������� ������)
	///@param t - ���������� ��������
	void PrintModel(int t);
	~NonlinearModel();
};


