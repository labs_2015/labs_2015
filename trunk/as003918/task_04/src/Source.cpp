///@file
///@author Ayovich	Sergei
///@mainpage
///@image html lin.png
///������� �������� �������� �.�. ��� ��������� ������� ���������� ����������� ����� � ������.
///@image html nonlin.png
///������� �������� ���������� �.�. ������������ ��������, ������� ���������� ����������� ����� � ������. 

#include<iostream>
#include"MainModel.h"
#include"LinearModel.h"
#include"NonlinearModel.h"
#include<iostream>

using namespace std;
/**
@brief ������� �� ������� �������� �����������
*/
void main()
{
	LinearModel linMod(5, 50);
	NonlinearModel nonlinMod(5, 100);

	linMod.PrintModel(15);
	cout << endl;
	nonlinMod.PrintModel(15);
	cout << endl;
	system("PAUSE");
}