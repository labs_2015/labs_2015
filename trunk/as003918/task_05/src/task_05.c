
///@mainpage  Hello World
///@file task_05.c
///@author Ayovich Sergei Viktorovitch


#include "i7188.h"

///@brief  The program will display on the seven-segment LED the string "HELLO WORLd.CEPE��."
int main()
{
        while(1)
	{
            Show5DigitLedSeg(1,55);
            Show5DigitLedSeg(2,79);
            Show5DigitLedSeg(3,14);
            Show5DigitLedSeg(4,14);
            Show5DigitLedSeg(5,126);
            DelayMs(1000);
            Show5DigitLedSeg(1,30);
            Show5DigitLedSeg(2,60);
            Show5DigitLedSeg(3,126);
            Show5DigitLedSeg(4,118);
            Show5DigitLedSeg(5,14);
            DelayMs(1000);
            Show5DigitLedSeg(1,189);
            Show5DigitLedSeg(2,0);
            Show5DigitLedSeg(3,0);
            Show5DigitLedSeg(4,0);
            Show5DigitLedSeg(5,0);
            DelayMs(1000);
            Show5DigitLedSeg(1,78);
            Show5DigitLedSeg(2,79);
            Show5DigitLedSeg(3,103);
            Show5DigitLedSeg(4,79);
            Show5DigitLedSeg(5,70);
            DelayMs(1000);
            Show5DigitLedSeg(1,247);
            DelayMs(1000);
	}
}
