#include <iostream>
#include <math.h>

using namespace std;

class Abstract {
public:
	float Y[20], ut = 5;
	double T0 = 0.5, Td = 0.02, T = 0.5, k = 0.5,
		   q0, q1, q2,
		   e1 = 0, e2 = 0, e3 = 0, u = 0,
		   w = 40, y = 0;
	/**
	calculations
	*/
	void pid()
	{
		q0 = k * (1 + (Td / T0));
		q1 = -k * (1 + 2 * (Td / T0) - (T0 / T));
		q2 = k * (Td / T0);
		e3 = e2;
		e2 = e1;
		e1 = w - y;
		u += (q0 * e1 + q1 * e2 + q2 * e3);
	}
protected:
	virtual void show() = 0;
};

class lineModel : public Abstract
{
public:
	/**
	show first table with line model
	*/
	void show()
	{
		cout << "\nline model\n" << endl;
		cout.width(8);
		cout << "Y" << "  |  " << "T\n----------------------\n";
		double  yL = 0;
		for (int i = 0; i <= 20; i++)
		{
			pid();
			yL = 0.988 * y + 0.232 * u;
			y = yL;
			cout.width(8);
			cout << yL << "  |  " << u << endl;
		}
	}
};

class unlineModel : public Abstract
{
public:
	/**
	show unline model
	*/
	void show()
	{
		double yLK = 0, yL = 0;
		cout << "\nunline model\n" << endl;
		cout.width(8);
		cout << "Y" << "  |  " << "T\n----------------------\n";
		for (int i = 1; i <= 20; i++)
		{
			pid();
			yLK = 0.9 * yL - 0.001 * y * y + u + sin(u);
			yL = yLK;
			y = yL;
			cout.width(8);
			cout << yLK << "  |  " << u << endl;
		}
	}
};
/**
start program
*/
int main()
{
	lineModel lm;
	unlineModel ulm;
	lm.show();
	ulm.show();
	system("pause");
	return 0;
};
