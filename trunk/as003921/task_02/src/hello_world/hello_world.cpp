/** \file
\brief �������� ��������� Hello_world.

\mainpage 
"������������ � ��������� hello world."
*/

#include "stdafx.h"
#include <iostream>

using namespace std;

/**
\brief ���������, ��������� �� ������ ������� "Hello, world".
\return 0 - ��������� ������� ���������.
!0 - ��������� ����������� � �������.
*/
int main()
{
	cout << "Hello, world!" << endl;
	return 0;
}