#pragma once
class Abstract
{
public:
	///@brief Returns information about the output temperature of the object
	virtual double getTemperature(double y, double u) = 0;
	
	Abstract();
	~Abstract();
};

