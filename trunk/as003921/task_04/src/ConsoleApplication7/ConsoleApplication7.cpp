/** \file
\brief Function main project task_04
\author Savinets Vladimir

\mainpage
\image html model.png
\brief It can be seen that the non-linear model of linear regulated effectively
*/

#include "stdafx.h"
#include <iostream>
#include "Abstract.h"
#include "BasicOBJ.h"
#include "Const.h"
#include "PID.h"

using namespace std;
/// The operation created models
int main()
{
	Const const1;
	BasicOBJ obj;
	PID pid;
	pid.SetK(0.6);
	pid.SetT(0.6);
	pid.SetT0(0.2);
	pid.SetTd(0.02);
	pid.CalcQ();
	obj.setY1(0);
	double w = 50, y = const1.getTemperature(0, 0), u = pid.Calc(y, w);


	for (int i = 0; i < 50; i++)
	{
		y = const1.getTemperature(y, u);
		u = pid.Calc(y, w);
		cout << y << "    " << u << endl;
	}

	cout << endl << endl << endl;


	y = obj.getTemperature(0, 0);
	u = pid.Calc(y, w);


	for (int i = 0; i < 50; i++)
	{
		y = obj.getTemperature(y, u);
		u = pid.Calc(y, w);
		cout.width(5);
		cout << y << "    " << u << endl;
	}

    return 0;
}

