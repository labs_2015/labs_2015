#pragma once
#include "Abstract.h"
class Const :
	public Abstract
{
public:
	double getTemperature(double y, double u)
	{
		return 0.988*y + 0.232*u;
	}
	Const();
	~Const();
};

