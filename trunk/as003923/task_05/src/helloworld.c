/**
@mainpage  Seven-segment display
@file
@author Ilya Khursin
@date 13.12.15
*/
#include "i7188.h"

int main()
{
	while(1) 
	{
		Show5DigitLedSeg(1, 55);	
		Show5DigitLedSeg(2, 79);	
		Show5DigitLedSeg(3, 14);	
		Show5DigitLedSeg(4, 14);	
		Show5DigitLedSeg(5, 126);	
		DelayMs(1000);
		Show5DigitLedSeg(1, 79);
		Show5DigitLedSeg(2, 14);
		Show5DigitLedSeg(3, 14);
		Show5DigitLedSeg(4, 126);
		Show5DigitLedSeg(5, 0);    
		DelayMs(1000);
		Show5DigitLedSeg(1, 14);
		Show5DigitLedSeg(2, 14);
		Show5DigitLedSeg(3, 126);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 55);    
		DelayMs(1000);
		Show5DigitLedSeg(1, 14);	
		Show5DigitLedSeg(2, 126);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 55);
		Show5DigitLedSeg(5, 62);	   
		DelayMs(1000);
		Show5DigitLedSeg(1, 126);	
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 55);
		Show5DigitLedSeg(4, 62);
		Show5DigitLedSeg(5, 5);  
		DelayMs(1000);
		Show5DigitLedSeg(1, 0);	
		Show5DigitLedSeg(2, 55);
		Show5DigitLedSeg(3, 62);
		Show5DigitLedSeg(4, 5);
		Show5DigitLedSeg(5, 91); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 55);	
		Show5DigitLedSeg(2, 62);
		Show5DigitLedSeg(3, 5);
		Show5DigitLedSeg(4, 91);
		Show5DigitLedSeg(5, 6); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 62);	
		Show5DigitLedSeg(2, 5);
		Show5DigitLedSeg(3, 91);
		Show5DigitLedSeg(4, 6);
		Show5DigitLedSeg(5, 21); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 5);	
		Show5DigitLedSeg(2, 91);
		Show5DigitLedSeg(3, 6);
		Show5DigitLedSeg(4, 21);
		Show5DigitLedSeg(5, 0); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 91);	
		Show5DigitLedSeg(2, 6);
		Show5DigitLedSeg(3, 21);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 6);	
		Show5DigitLedSeg(2, 21);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 21);	
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0); 
		DelayMs(1000);
		Show5DigitLedSeg(1, 0);	
		Show5DigitLedSeg(2, 0);
		Show5DigitLedSeg(3, 0);
		Show5DigitLedSeg(4, 0);
		Show5DigitLedSeg(5, 0);  
	}
}