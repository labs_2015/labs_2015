/**
@mainpage  This console application can display the "Hello World!"
@file
@author Masha Eikhort
@date 22.11.15
*/
#include <iostream>
using namespace std;
/**
@brief We find an entry point in the program
*/
int main()
{
	/**
	@brief Display message of this program "Hello,world"
	*/
	cout << "Hello, world." << endl;
	///@brief We hold the console screen
	system("pause");
	return 0;
}
