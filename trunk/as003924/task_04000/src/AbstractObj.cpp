#include "stdafx.h"
#include "AbstractObj.h"

AbstractObj::AbstractObj(float CurrentT)
{
	CurrentTemperature = CurrentT;
}

AbstractObj::~AbstractObj(void)
{
}

float* AbstractObj::GetModel(float QuantityOFHeat, int time)
{
	float *Table = new float[time];

	Table[0] = CurrentTemperature;

	for (int i = 1; i < time; i++) {
		Table[i] = incrTemp(QuantityOFHeat);
	}

	return Table;
}

void AbstractObj::OneStep(float QuantityOfHeat)
{
	incrTemp(QuantityOfHeat);
}

float AbstractObj::getTemp()
{
	return CurrentTemperature;
}
