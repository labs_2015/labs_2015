/*!
	@file
	@brief task_04 main function
	@author Eichort Maria AS - 39
*/
#include "stdafx.h"
#include "LinearControllObject.h"
#include "NonLinearObj.h"
#include "Regulator.h"
#include <iostream>
#include <Windows.h>
#include <ctime>

/*!
	@mainpage Result of the regulation
	@image html Result.png
*/

using namespace std;

/*!
	@brief Main function.

	Works with two objects (Linear and non linear model).
	Regulates temperature of those objects to the given value.
*/
int main()
{
	srand(time(0));
	LinearControllObject LinearModel(rand() % 16 + 5);
	NonLinearObj NonlinearModel(rand() % 16 + 5);

	float requairedTemp = 25;
	float E, Q = 0, max = 0;
	int iterations = 1;

	float q1 = 0.58;
	float q2 = 0.08;
	float q3 = -0.6337;

	cout << " Current temperatur is:  " << LinearModel.getTemp() << endl << "Enter required temperature: ";
	cin >> requairedTemp;

	Regulator forLinear(&LinearModel, requairedTemp, "LinearResults.txt", q1, q2, q3);
	forLinear.Regulate();

	cout << "\n\nNonlinear model's current temperatur is:  " << NonlinearModel.getTemp() << endl << "Enter required temperature: ";
	cin >> requairedTemp;

	q1 = 0.28;
	q2 = 0.016;
	q3 = -0.25;

	Regulator forNonLinear(&NonlinearModel, requairedTemp, "NonLinearResults.txt", q1, q2, q3);
	forNonLinear.Regulate();

	cout << endl;

	system("pause");
	return 0;
}

