
#include "stdafx.h"
#include "LinearObj.h"

LinearObj::LinearObj(float CurrentT) : AbstractObj(CurrentT)
{

}
/*!
	@brief Overloads incrTemp function

	Function realisation:
	@code
	float LinearObj::incrTemp(float QuantityOFHeat) {

		return CurrentTemperature = CurrentTemperature*0.988 + QuantityOFHeat*0.232;
	}
	@encode
*/

float LinearObj::incrTemp(float QuantityOFHeat) {

	return CurrentTemperature = CurrentTemperature*0.988F + QuantityOFHeat*0.232F;
}
