#pragma once
#include "AbstractObj.h"

/*!
	@brief Class inherited from AbstractObj

	Linear model of the object
*/
class LinearObj : public AbstractObj
{
public:
	LinearObj(float CurrentT = 25.0);

private:
	/*!
		@brief Overloaded function of AbstractObj, that realises linear model.
	*/
	float incrTemp(float QuantityOFHeat);

};

