
#include "stdafx.h"
#include "NonLinearObj.h"
#include <cmath>


NonLinearObj::NonLinearObj(float CurrentT) : AbstractObj(CurrentT)
{
	prevTemp = 0;
	prevQ = 0;
}

NonLinearObj::~NonLinearObj()
{
}
float NonLinearObj::incrTemp(float QuantityOFHeat)
{
	float curT = CurrentTemperature;

	CurrentTemperature = 0.9 * curT - 0.001 * (prevTemp * prevTemp) + QuantityOFHeat - sin(prevQ);

	prevTemp = curT;
	prevQ = QuantityOFHeat;

	return CurrentTemperature;
}
