#pragma once
#include "AbstractObj.h"

/*!
	@brief class inherited from AbstractObj.

	Provides a non linear model of our object
*/
class NonLinearObj : public AbstractObj
{
public:
	NonLinearObj(float CurrentT = 25.0);
	~NonLinearObj();

private:
	/*!
		@brief Overloaded function of AbstractObj, that realises non linear model.

		@param QuantityOFHeat heat passed to our object
	*/
	float incrTemp(float QuantityOFHeat);
	
	float prevTemp;///< Temperature of the previous iteration(required for nonlinear model)
	float prevQ;///< Supportive variable, whych helps to define if it is first iteration

};

