#pragma once
#include "AbstractObj.h"
#include <string>

/*!
@brief Class for Regulator controller definition

Defines functinality for our temperature controller
*/
class Regulator
{
public:
	Regulator();
	Regulator(AbstractObj *_Object, float requiredTemp);
	Regulator(AbstractObj *_Object, float requiredTemp, std::string _fileName, float _q1, float _q2, float _q3);
	~Regulator();

	/*!
		Function that counts and sends required quantity of heat to our object.
	*/
	float countQ();
	/*!
		Function that counts average value of E for the last four iterations.
	*/
	float average();
	/*!
		Function just sets the coeffitients that required for regulation.
		It tells when to stop the regulation.
	*/
	void setCoeffs(float _q1, float _q2, float _q3);
	void setObject(AbstractObj *_Object);
	/*!
		Regulates our object while average() doesn't equalls required value (0.001).
	*/
	void Regulate();

private:
	AbstractObj *Object;

	float requiredTemp;
	float E;/*!<Difference between the required temperature and current temperature */
	float prev_E;
	float dobl_prev_E;
	float tripl_prev_E;
	float q1, q2, q3;

	float QuantityOfHeat;

	float max;

	std::string fileName;

	int number;

	int iterations;

};