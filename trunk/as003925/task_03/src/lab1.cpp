///@file
///@author ���� �������
///@27.10.2015
#include <iostream>
#include <math.h>
using namespace std;
/**@class AbstactClass
����������� �����
*/
class AbstactClass {
public:
	float Y[20];
	float ut = 10;
protected:
	///@brief ����������� �����, ������� ������� ���������� � �������� �����������
	virtual void vivod() = 0;
};
/**@class LineinayaModel
��������� ����� �� ��������(������������)
*/class LineinayaModel : public AbstactClass
{
public:
	void vivod()
	{
		cout << "LineinayaModel\n\n";
		cout.width(8);
		cout << "Y" << "  |  " << "T\n";
		Y[0] = 0;
		for (int i = 0; i <= 20; i++)
		{
			Y[i + 1] = 0.988*Y[i] + 0.232*ut;
			cout.width(8);
			cout << Y[i] << "  |  " << i << endl;
		}
	}
};
/**@class NeLineinayaModel 
��������� ����� �� ��������(������������)
*/
class NeLineinayaModel : public AbstactClass
{
public:
	void vivod()
	{
		Y[0] = 0;
		Y[1] = 0;
		cout << "\nNeLineinayaModel\n\n";
		cout.width(8);
		cout << "Y" << "  |  " << "T\n";
		for (int i = 1; i <= 20; i++)
		{
			Y[i + 1] = 0.9*Y[i] - 0.001*Y[i - 1] * Y[i - 1] + ut + sin(ut);
			cout.width(8);
			cout << Y[i] << "  |  " << i << endl;
		}
	}
};
/**@mainpage ������
@image html nelineyanaya.png
\details ����������� �������������� �� ������� �������� ���������� �.�. ������������ ��������, ������� ���������� ����������� 
@image html lineyanaya.png
\details ����������� �������������� �� ������� �������� �������� �.�. ��� ��������� ������� ���������� ����������� 
*/
int main(int argc, char* argv[])
{
	LineinayaModel a;
	NeLineinayaModel b;
	a.vivod();
	b.vivod();
	system("pause");
	return 0;
};
