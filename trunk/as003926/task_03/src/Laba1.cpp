///@file
///@author Andronov Oleg
///@31,10,2015

#include "stdafx.h"
#include <iostream>
#include <math.h>
#include <fstream>
#include <cassert>


class Enjoy
{
///@brief  set deffault temperature
public:
	Enjoy(float temperature = 25.0)
	{
		deffault = temperature;
	}
	///@brief  set number temperature
	virtual float* GetModel(float Grad, int s4et)
	{
		float *Table = new float[s4et];
		Table[0] = deffault;
		for (int i = 1; i < s4et; i++)
		{
			Table[i] = incrTemp(Grad);
		}
		return Table;
	}

public:
	virtual float incrTemp(float Grad) = 0;
	float deffault;
};

class LinearModel : public Enjoy
{
///@brief  set change of temperature
public:
	LinearModel(float temperature1 = 25.0) : Enjoy(temperature1) {

	}
///@brief get the temperature in the time
private:
	float incrTemp(float Grad) {

		return deffault = deffault*0.988 + Grad*0.232;
	}
};

class NonLinearModel : public Enjoy
{
///@brief  set the input temperature in time moment 1
public:
	NonLinearModel::NonLinearModel(float temperature2 = 25.0) : Enjoy(temperature2)
	{
		First = true;
		prevTemp = deffault;
	}
///@brief  set change of temperature
private:
	float incrTemp(float Grad)
	{
		float curT, prevQ;
		if (!First)
		{
			prevQ = Grad;
		}
		else
		{
			prevTemp = 0.0f;
			prevQ = 0.0f;
		}
		curT = deffault;
		deffault = 0.9f * curT - 0.001f * (prevTemp * prevTemp) + Grad - sin(prevQ);
		prevTemp = curT;
		First = false;
		return deffault;
	}
	float prevTemp;
	bool First;
};

///@mainpage Result
///@image html screen.png
///@brief creating the model
int main()
{
	std::ofstream fout;
	fout.open("Grow.txt", std::ios_base::out);
	assert(fout.is_open());
	LinearModel A(25);
	NonLinearModel B(25);
	float* GrowA = A.GetModel(10, 21);
	float* GrowB = B.GetModel(10, 21);
	std::cout << "Linear model:" << std::endl;
	for (int i = 0; i < 21; i++)
	{
		std::cout << i << " : ";
		std::cout << GrowA[i] << std::endl;
		fout << i << " : ";
		fout << GrowA[i] << std::endl;
	}
	fout << std::endl;
	std::cout << std::endl << "NonLinear model:" << std::endl;
	for (int i = 0; i < 21; i++)
	{
		std::cout << i << " : ";
		std::cout << GrowB[i] << std::endl;
		fout << i << " : ";
		fout << GrowB[i] << std::endl;
	}
	fout.close();
	system("pause");
	return 0;
}

