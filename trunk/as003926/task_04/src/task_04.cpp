///@file task_04.cpp
///@author Andronov Oleg
///@date 05.11.2015

#include "stdafx.h"
#include <iostream>
#include <math.h>
#include <fstream>

using namespace std;
class abstract {
	virtual void show() = 0;
};
class split : public abstract {
	///@brief  set start temperature 
protected:
	double y[10] = {};
	double y0 = 0;
public:
	void split::setY0(double y0) {

		this->y0 = y0;

	};
	///@brief  return start temperature 
	double split::getY0() {

		return y0;

	};
	split::~split()
	{
	}
};

class line: public split {
	///@brief  set change of temperature

public:
		  void line::setY_U(double U) {
		this->y[0] = y0;
		for (int i = 0; i < 10; i++)
			this->y[i + 1] = this->y[i] * 0.988 + U*0.232;

	}

	///@brief  return result of output temperature 
	double line::findY_U(double y, double U) {
		return y*0.988 + U*0.232;

	}

	///@brief get the temperature in the time
	double line::getY_U(int i) {
		if (i >= 0 && i<10)
			return y[i];
		else return -1;

	};

	///@brief show the temperature dependence on time
	void show() {
		cout << "Y" << "\t\t" << "T" << endl;
		for (int i = 0; i < 10; i++) {
			cout << y[i] << "\t\t" << i << endl;

		}

	}

	line::~line()
	{
	}
};

class notline: public split {
	///@brief  set the input temperature in time moment 1
	
	
public:
	double y1 = 0;
	void notline::setY1(double y1) {
		this->y1 = y1;
	}
	///@brief  set change of temperature
	void notline::setY_U(double U) {
		this->y[0] = y0;
		this->y[1] = y1;
		for (int i = 1; i < 10; i++)
			this->y[i + 1] = this->y[i] * 0.9 - 0.001*pow(this->y[i - 1], 2) + U + sin(U);

	}
	///@brief  return result of output temperature 
	double notline::findY_U(double y, double U) {
		double temp = 0;
		temp = y * 0.9 - 0.001*pow(y1, 2) + U + sin(U);
		y1 = y;
		return temp;

	}
	///@brief show the temperature dependence on time
	void show() {
		cout << "Y" << "\t\t" << "T" << endl;
		for (int i = 0; i < 10; i++) {
			cout << y[i] << "\t\t" << i << endl;

		}

	}
	notline::~notline()
	{
	}
};

class pid {
	///@brief set parameters: step on the time (T0), const diff (Td), const integr (T), transfer coeff (K), expected temperature (wt)
public:
	double T0, Td, T, K, wt, q0 = 0, q1 = 0, q2 = 0, e3 = 0, e2 = 0, e1 = 0, deltaU = 0;
	void pid::setParametrs(double T0, double TD, double T, double K, double wt)
	{
		this->T0 = T0;
		this->Td = TD;
		this->T = T;
		this->K = K;
		this->wt = wt;
	}
	///@brief calculate q0, q1, q2
	void pid::findq012() {
		q0 = K * (1 + (Td / T0));
		q1 = -K *(1 + 2 * (Td / T0) - (T0 / T));
		q2 = K * (Td / T0);
	}
	///@brief return deltaU
	double pid::foundU(double y)
	{
		e3 = e2;
		e2 = e1;
		e1 = wt - y;
		deltaU += (q0 * e1 + q1 * e2 + q2 * e3);
		return deltaU;
	}
	pid::~pid()
	{
	}
};

///@mainpage Result
///@image html result.jpg
///@brief creating the model of PID regulator
int main()
{
	double u;
	double wt;
	cout << "Enter start temperature u(t): " << endl;
	cin >> u;
	cout << "Enter expected temperature wt: " << endl;
	cin >> wt;
	line q;
	notline w;
	q.setY0(0);
	q.setY_U(u);
	w.setY_U(u);
	pid one;
	one.setParametrs(0.5, 0.02, 0.85, 0.5, wt);
	one.findq012();
	double y = 0;
	y = q.findY_U(0, 0);
	u = one.foundU(y);
	int i = 1;

	while (y != wt) {

		y = q.findY_U(y, u);
		u = one.foundU(y);
		i++;

	}

	if (y == wt)
		cout << "\nLINE: Finded  temperature( " << wt << " ) when\ninput heat U = " << u 
		<< "\ntime is: " << (double)i*0.5 << endl;

	pid two;
	two.setParametrs(0.5, 0.02, 0.85, 0.5, wt);
	two.findq012();
	y = w.findY_U(0, 0);
	u = two.foundU(y);
	i = 1;

	while (y != wt) {
		y = w.findY_U(y, u);
		u = two.foundU(y);
		i++;
	}
	if (y == wt)
		cout << "\nNOTLINE: Finded temperature( " << wt << " ) when\ninput heat U = " << u 
		<< "\ntime is: " << i*0.5 << endl;
	
	system("pause");
	return 0;
}